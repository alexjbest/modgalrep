modular_group_type(G) = G[1];
modular_group_level(G) = G[2];
modular_group_representatives(G) = G[3];
modular_group_degree(G) = length(G[3]);
modular_group_cusps(G) = G[4];
modular_group_num_cusps(G) = length(G[4]);

install (modular_group_contains_minus1, "lG", , "libmodular.so");
install (modular_group_modular_forms_degree, "GL", , "libmodular.so");
install (modular_group_cusp_forms_degree, "GL", , "libmodular.so");
install (modular_group_genus, "lG", , "libmodular.so");

install (Gamma_0, "L", , "libmodular.so");
install (Gamma_1, "L", , "libmodular.so");
install (pmGamma_1, "L", , "libmodular.so");
