#ifndef MODULAR_FORMS
#define MODULAR_FORMS

GEN modular_forms_q_expansion_basis(GEN group, unsigned long weight, GEN p, unsigned long prec);
GEN modular_forms_as_hecke_module(GEN group, unsigned long weight, GEN p, unsigned long prec);

#endif
