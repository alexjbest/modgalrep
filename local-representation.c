#include <pari/pari.h>

#include "local-representation.h"
#include "modular-groups.h"
#include "modular-symbols.h"
#include "torsion-subscheme.h"

static GEN
eigenvalue(GEN T, GEN subspace) {
  T = inverseimage(subspace, gmul(T, subspace));
  return gcoeff(T, 1, 1);
}

/*
  Return the points of the F_l-vector space scheme over F_p realising
  the mod l representation attached to the given cusp form.
*/
GEN
local_representation(GEN M, GEN subspace, unsigned long p,
		     unsigned long max_degree, unsigned long tries) {
  long l, weight;
  GEN T, a_p, eps_p, chi, Gamma;

  if (lg(subspace) != 2)
    pari_err(e_MISC, "Hecke algebra quotient should be one-dimensional");

  /* Determine the congruence subgroup to use.  */
  Gamma = modular_symbols_group(M);
  weight = modular_symbols_weight(M);
  l = modular_symbols_characteristic(M);
  if (weight != 2)
    Gamma = Gamma_1(l * modular_group_level(Gamma));

  /* Compute a_p and epsilon(p) for our form.  */
  T = modular_symbols_hecke_operator(M, p);
  a_p = eigenvalue(T, subspace);
  T = modular_symbols_diamond_operator(M, p);
  eps_p = eigenvalue(T, subspace);

  /*
    Compute  chi = x^2 - a_p x + epsilon(p) p^(k - 1),
    which is the characteristic polynomial of  Frob_p  on
    the representation space that we want to compute.
  */
  chi = gmul(mkpoln(3, gen_1, gneg(a_p),
		    gmul(eps_p, powuu(p, weight - 1))),
	     gmodulss(1, l));

  /* TODO: use diamond operators to cut down the space if needed.  */
  return torsion_subscheme(Gamma, l, p, chi, max_degree, tries);
}
