/***

    Modular curves

***/

#include <pari/pari.h>

#include "curve.h"
#include "cusp-forms.h"
#include "modular-curve.h"
#include "modular-forms.h"
#include "modular-groups.h"
#include "jacobian.h"

#include <stdlib.h>  /* getenv */


/*
  Return the characteristic polynomial of the Frobenius (the one
  with complex roots of absolute value \sqrt p) on the l-adic
  Tate module of the modular curve over \F_p associated to the
  given modular group.
  Here l is any prime number different from p.  We assume
  that p does not divide the level of the group.
*/
GEN
modular_curve_characteristic_polynomial_frobenius(GEN group, unsigned long p) {
  GEN S, T_p, diam_p, x;
  unsigned long dim, level = modular_group_level (group);
  pari_sp av = avma;
  if (!uisprime (p) || level % p == 0)
    pari_err (e_MISC, "modular_curve_frobenius: p must be a prime number not dividing the level");
  S = cusp_forms (group, 2, NULL);
  dim = cusp_forms_dimension (S);
  /* Compute  T_p  and  <p>  on the space of cusp forms.  */
  T_p = cusp_forms_hecke_operator (S, p);
  diam_p = cusp_forms_diamond_operator (S, p);
  x = pol_x(0);
  /*
    The characteristic polynomial is
    det (x^2 * matid (dim) - x * T_p + p * diam_p).
  */
  return gerepileupto(av, det(gadd(gmul(x, gsub(gmul(x, matid(dim)), T_p)),
				   gmulsg(p, diam_p))));
}

/*
  Return the numerator of the zeta function of the modular curve
  over  \F_p  associated to the given modular group.  This is the
  reciprocal polynomial of the characteristic polynomial of the
  Frobenius endomorphism.
*/
GEN
modular_curve_numerator_zeta_function(GEN group, unsigned long p) {
  pari_sp av = avma;
  return gerepileupto(av, polrecip (modular_curve_characteristic_polynomial_frobenius (group, p)));
}

/*
  Return the zeta function of the modular curve over  \F_p
  associated to the given modular group.
*/
GEN
modular_curve_zeta_function(GEN group, unsigned long p) {
  pari_sp av = avma;
  /* numerator / ((1 - x)(1 - px)) */
  GEN N = gdiv(modular_curve_numerator_zeta_function(group, p),
	       gmul (mkpoln(2, gen_m1, gen_1),
		     mkpoln(2, stoi(-p), gen_1)));
  return gerepileupto(av, N);
}
    
/*
  Return the number of points of the modular curve over  \F_{p^k}
  associated to the given group
*/
GEN
modular_curve_count_points(GEN group, unsigned long p, unsigned long k) {
  /* Compute the numerator of the zeta function
     of the curve over  \F_{p^k}.  */
  GEN f, n;
  pari_sp av = avma;
  long y = fetch_var();
  f = polresultant0(modular_curve_numerator_zeta_function (group, p),
		    gsub(gpowgs(pol_x(0), k), pol_x(y)), 0, 0);
  n = gadd(polcoeff0(f, 1, y),
	   gaddgs(gpowgs(stoi(p), k), 1));
  delete_var();
  return gerepileupto(av, n);
}

/*
  Return the number of points of the Jacobian of the modular curve
  over  \F_{p^k}  associated to the given group.  We use that this
  number is equal to the value of the characteristic polynomial
  of the Frobenius in 1.
*/
GEN
modular_curve_jacobian_count_points(GEN group, unsigned long p,
				    unsigned long k) {
  pari_sp av = avma;
  GEN n = polresultant0 (modular_curve_numerator_zeta_function (group, p),
			 gsubgs(gpowgs(pol_x(0), k), 1), 0, 0);
  return gerepileupto(av, n);
}

/*
  Compute data associated to the modular curve.  Modular forms are
  computed to sufficient precision to determine the spaces of global
  sections of  {\cal L}^max_power.
*/
GEN
modular_curve(GEN group, unsigned long p, unsigned long max_power) {
  unsigned long degree, dim, genus = modular_group_genus (group), weight,
    prec, r, i, s0, s1;
  GEN result, forms, V, M, ind, numzeta;
  char *cache_file;

  /*
    Choose WEIGHT such that the line bundle  {\cal L}  of cusp forms
    of weight WEIGHT has degree  >= 2g + 1.
  */
  for(weight = 2;
      (degree = gtos(modular_group_modular_forms_degree (group, weight)))
	< 2*genus + 1;
      weight++);
  r = degree - (2*genus + 1);

  /*
    Compute the dimension of  \Gamma(X,{\cal L})  using
    Riemann's theorem (note that the degree is greater
    than 2g - 2).
  */
  dim = 1 - genus + degree;

  /*
    We need to know \Gamma(X,{\cal L}^4) to find random points
    on the curve.
  */
  if (max_power < 4)
    max_power = 4;
  prec = max_power * degree + 1;

  if ((cache_file = getenv("MODFORMS")) != NULL) {
    printf("modular_curve: loading forms from %s\n", cache_file);
    forms = gp_read_file(cache_file);
    forms = rowslice(forms, 1, prec);
    forms = gmul(forms, gmodulss(1, p));
  } else if (1) {
    /* as long as we don't need the diamond operators...  */
    pari_printf ("modular_curve: computing q-expansions of modular forms"
		 " of weight %li to precision O(q^%li)\n", weight, prec);
    forms = modular_forms_q_expansion_basis (group, weight, stoi(p), prec);
  } else {
    /* also compute the Hecke operators */
    pari_printf ("modular_curve: computing q-expansions and Hecke algebra"
		 " of modular forms of weight %li to precision O(q^%li)",
		 weight, prec);
    result = modular_forms_as_hecke_module (group, weight, stoi(p), prec);
    forms = gel (result, 1);
    /* Hecke algebra: diam = result[2] and T = result[3] */
  }

  if (lg(forms) - 1 != dim)
    pari_err (e_MISC, "modular_curve: incorrect dimension of space of modular forms of weight %li (dimension %li instead of %li)",
	      weight, lg(forms) - 1, dim);

  printf("computing the subspace of forms vanishing to order %li\n", r);
  M = cgetg(prec - r + 1, t_MAT);
  for(i = 1; i <= prec - r; i++)
    gel(M, i) = vec_ei(prec, r + i);
  V = intersect(forms, M);
  V = ZM_to_Flm(lift(V), p);
  if(lg(V) - 1 != genus + 2)
    pari_err(e_MISC, "unexpected dimension");

  /* Extract the small subspace we are interested in.  */
  ind = gel(Flm_indexrank(V, p), 1);
  if(lg(ind) != lg(V))
    pari_err(e_MISC, "dimensions do not agree");
  if(ind[1] != r + 1)
    pari_err(e_MISC, "strange order of vanishing");
  pari_printf("using pivots %Ps\n", ind);
  s0 = ind[1]; s1 = s0 + max_power*(2*genus + 1);
  V = matslice0(V, s0, s1, 1, lg(V) - 1);

  numzeta = modular_curve_numerator_zeta_function (group, p);

  return curve_construct (mkvec2(group, stoi(weight)), p, NULL,
			  MULTIPLY_POWER_SERIES,
			  V, max_power, numzeta);
}

GEN
modular_curve_distinguished_point(GEN X) {
  GEN V = curve_V(X, 1);
  /* Return the first row of V.  */
  return matslice0(V, 1, 1, 1, lg(V) - 1);
}
