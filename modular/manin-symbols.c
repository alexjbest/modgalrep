/***

    Manin symbols

***/

#include <pari/pari.h>
#include "modular-groups.h"
#include "manin-symbols.h"

/*
  - The set of Manin symbols for Gamma_0(n) is in bijection with
  the Cartesian product of  { X^j Y^{k-2-j} | 0 <= j <= k - 2 }
  and the set

    Gamma_0(n)\PSL_2(Z) = P^1(Z/nZ) =
      { (c, d) in (Z/nZ)^2 | <c, d> = Z/nZ } / (Z/nZ)^\times.

  We compute representatives  (c, d) in Z^2  with  d  either  0
  or a positive divisor of  n, and  c >= 0  as small as possible.

  - The set of Manin symbols for Gamma_1(n) is in bijection with
  the Cartesian product of  { X^j Y^{k-2-j} | 0 <= j <= k - 2 }
  and the set

    Gamma_1(n)\PSL_2(Z) =
      { (c, d) in (Z/nZ)^2 | <c, d> = Z/nZ } / {\pm1}.

  We compute representatives  (c, d) in Z^2  with  0 <= c <= n/2
  and  d >= 0  as small as possible.

  - We represent Manin symbols as [c, d] if the weight is 2,
  and as [j, c, d] if the weight is >= 3, where  j  corresponds
  to  X^j Y^{k-2-j}.
*/


/*
  Return the list of Manin symbols for the given group and weight.
*/
GEN
manin_symbols(GEN group, long weight) {
  GEN sym_weight_2, syms, s;
  long i, j;
  pari_sp av = avma;

  sym_weight_2 = modular_group_representatives(group);
  if(weight == 2)
    return sym_weight_2;
  syms = cgetg((lg(sym_weight_2) - 1) * (weight - 1) + 1, t_VEC);
  for(i = 0; i < lg(sym_weight_2) - 1; i++) {
    s = gel(sym_weight_2, i + 1);
    for(j = 0; j <= weight - 2; j++)
      gel(syms, (weight - 1) * i + j + 1) = mkvecsmall3(j, s[1], s[2]);
  }
  return gerepileupto(av, syms);
}

long
manin_symbol_index(GEN group, long weight, GEN sym) {
  long j, index;
  GEN rep;

  if(weight == 2) {
    return modular_group_repr_index(group, sym);
  } else {
    j = sym[1];
    rep = mkvecsmall2(sym[2], sym[3]);
    index = modular_group_repr_index(group, rep);
    cgiv(rep);
    return (weight - 1) * (index - 1) + j + 1;
  }
}


/*
  The quotient by the two-term relations is specified as follows:

  We give a vector  [s_1, ..., s_m]  with each  s_i  the index of a
  Manin generator; the classes of these modulo the two-term relations
  form a basis for the quotient.  Furthermore, we give a vector
    [[c_1, i_1], ..., [c_1, i_n]],
  where  1 <= i_j <= m  for all j, such the image of the j-th Manin
  generator in the quotient is the class  c_j s_{i_j}.

  If  p != 0,  reduce everything modulo  p.
*/
GEN
manin_symbols_basis(GEN group, long weight, GEN syms, GEN p) {
  long nsym, count, c, d, i, i2, i3, i4, j, k, l, r;
  GEN sym, sym2, sym3, sym4, splitting_Q1_Q2, map_Q1_Q2, rel, w,
    indices, rel_suppl, zero_one,
    splitting_Q2_M, map_Q2_M, splitting_Q1_M, map_Q1_M;
  pari_sp av = avma;

  nsym = lg(syms) - 1;

  /*
    First we compute the quotient by the 2-term relations.  These are
    the relations

      [j, [c, d]] = (-1)^k [j, [-c, -d]]
      [j, [c, d]] + (-1)^j [k - j - 2, [d, -c]] = 0
      [j, [c, d]] + (-1)^(k - 2 - j) [k - 2 - j, [-d, c]] = 0
  */

  /* We allocate a bigger splitting_Q1_Q2 than we need and truncate later.  */
  splitting_Q1_Q2 = cgetg(nsym + 1, t_VECSMALL);
  map_Q1_Q2 = cgetg(nsym + 1, t_VEC);
  for(i = 1; i <= nsym; i++)
    gel(map_Q1_Q2, i) = NULL;

  count = 0;
  for(i = 1; i <= nsym; i++) {
    if(gel(map_Q1_Q2, i) == NULL) {
      sym = gel(syms, i);
      j = (weight == 2) ? 0 : sym[1];
      /*
	sym = [j, [c, d]],      sym2 = [k - 2 - j, [d, -c]],
	sym3 = [j, [-c, -d]],   sym4 = [k - 2 - j, [-d, c]]

	relations:

	sym = (-1)^k sym3
	sym + (-1)^j sym2 = 0
	sym + (-1)^(k - 2 - j) sym4 = 0
      */
      sym2 = (weight == 2) ? mkvecsmall2(sym[2], -sym[1])
	                   : mkvecsmall3(weight - 2 - j, sym[3], -sym[2]);
      sym3 = (weight == 2) ? mkvecsmall2(-sym[1], -sym[2])
	                   : mkvecsmall3(j, -sym[2], -sym[3]);
      sym4 = (weight == 2) ? mkvecsmall2(-sym[2], sym[1])
	                   : mkvecsmall3(weight - 2 - j, -sym[3], sym[2]);
      i2 = manin_symbol_index(group, weight, sym2);
      i3 = manin_symbol_index(group, weight, sym3);
      i4 = manin_symbol_index(group, weight, sym4);
      if((j % 2 == 0 && i == i2) || (weight % 2 == 1 && i == i3)
	 || ((weight - 2 - j) % 2 == 0 && i == i4)) {
	/* All of the above symbols map to zero.  */
	gel(map_Q1_Q2, i) = gen_0;
	gel(map_Q1_Q2, i2) = gen_0;
	gel(map_Q1_Q2, i3) = gen_0;
	gel(map_Q1_Q2, i4) = gen_0;
      } else {
	/*
	  Each of the above symbols maps to (1 or -1) times the first
	  one.
	*/
	count++;
	splitting_Q1_Q2[count] = i;
	gel(map_Q1_Q2, i) = mkvecsmall2(1, count);
	gel(map_Q1_Q2, i2) = mkvecsmall2(((j % 2 == 0) ? -1 : 1), count);
	gel(map_Q1_Q2, i3) = mkvecsmall2(((weight % 2 == 0) ? 1 : -1), count);
	gel(map_Q1_Q2, i4) = mkvecsmall2((((weight - 2 - j) % 2 == 0) ? -1 : 1), count);
      }
    }
  }
  fixlg(splitting_Q1_Q2, count + 1);

  /* Next we compute the 3-term relations on the basis of 2-term
     relations we just determined.  */
  rel = zero_Flm_copy(count, nsym);
  for(i = 1; i <= nsym; i++) {
    /*
      Compute the relation s + s\tau + s\tau^2 = 0 for the
      i-th Manin symbol  [j, [c, d]].
    */
    sym = gel(syms, i);
    j = (weight == 2) ? 0 : sym[1];
    c = (weight == 2) ? sym[1] : sym[2];
    d = (weight == 2) ? sym[2] : sym[3];
    /* gel(map_Q1_Q2, i) is either gen_0 or a mkvecsmall2().  */
    if(gel(map_Q1_Q2, i) != gen_0)
      coeff(rel, mael(map_Q1_Q2, i, 2), i) = mael(map_Q1_Q2, i, 1);
    for(l = 0; l <= weight - 2 - j; l++) {
      sym2 = (weight == 2) ? mkvecsmall2(d, -c - d)
	                   : mkvecsmall3(l, d, -c - d);
      /* Express sym2 on the Manin generators...  */
      i2 = manin_symbol_index(group, weight, sym2);
      /* ... and then on the basis of the 2-term quotient.  */
      w = gel(map_Q1_Q2, i2);
      if(w != gen_0)
	coeff(rel, w[2], i) += w[1] * itou(binomialuu(weight - 2 - j, l))
	  * (((weight - 2 - l) % 2 == 0) ? 1 : -1);
    }
    for(l = weight - 2 - j; l <= weight - 2; l++) {
      sym2 = (weight == 2) ? mkvecsmall2(-c - d, c)
                           : mkvecsmall3(l, -c - d, c);
      i2 = manin_symbol_index(group, weight, sym2);
      w = gel(map_Q1_Q2, i2);
      if(w != gen_0)
	coeff(rel, w[2], i) += w[1] * itou(binomialuu(j, weight - 2 - l))
	  * ((l % 2 == 0) ? 1 : -1);
    }
  }
  rel = zm_to_ZM(rel);

  if(gequal0(p)) {
    /*
      We only perform one iteration of mathnf.

      The PARI documentation says that mathnf should be called with
      flag = 0 for reasonably small matrices and enough memory.

      In fact, we should use hnfspec/hnfadd (not documented yet),
      which would presumably be better because our matrices are
      sparse.
    */
    rel = mathnf0(rel, 0);
    /*
      Alternative with multiple iterations:

      int stop;
      do {
        stop = 1;
        rel = mathnf0(rel, 0);
        for(i = 1; i < lg(rel); i++) {
	  GEN denom = ggcd0(gel(rel, i), NULL);
	  if(!gequal1(denom) & !gequalm1(denom)) {
	    stop = 0;
	    gel(rel, i) = gdiv(gel(rel, i), denom);
	  }
        }
      } while(!stop);
    */
  } else {
    /*
      If the weight is 2, the cokernel of the relation matrix is
      torsion-free [REF: Gabor's thesis?], so we don't have to apply
      matrixqz.
    */
    if (weight > 2)
      rel = matrixqz0(image(rel), p);
    rel = image(gmodulo(rel, p));
  }
  
  /*
    Now compute the quotient.  In general there does not exist a
    Z-basis for the quotient which consists of images of standard
    generators.  Therefore we compute it over the rationals or the
    field F_p.
  */
  r = lg(rel) - 1;
  indices = gel(indexrank(rel), 1);
  if(lg(indices) - 1 != r)
    pari_err(e_BUG, "manin_symbols_basis: inconsistent dimensions");
  /* Construct the complement of the vector of indices.  */
  splitting_Q2_M = cgetg(count - r + 1, t_VECSMALL);
  for(i = 1, j = 1, k = 1; i <= count; i++, j++) {
    for( ; i < indices[j] && i <= count; i++, k++)
      splitting_Q2_M[k] = i;
  }
  rel_suppl = (r == 0) ? matid(count) : suppl(rel);
  zero_one = shallowconcat(zeromat(count - r, r), matid(count - r));
  /*
    Solve the linear system
    map_Q2_M * rel_suppl = zero_one.
  */
  map_Q2_M = gtrans(gauss(gtrans(rel_suppl), gtrans(zero_one)));

  /*
    Now we have a vector of indices of the basis of Q2 which form a
    basis for M, together with the map Q2 -> M.  We lift each of the
    basis vectors to a Manin symbol representing it, and we compose
    the maps Q1 -> Q2 -> modular symbols.
  */
  splitting_Q1_M = cgetg(lg(splitting_Q2_M), t_VECSMALL);
  for(i = 1; i < lg(splitting_Q2_M); i++)
    splitting_Q1_M[i] = splitting_Q1_Q2[splitting_Q2_M[i]];
  map_Q1_M = cgetg(nsym + 1, t_MAT);
  for(j = 1; j <= nsym; j++) {
    w = gel(map_Q1_Q2, j);
    gel(map_Q1_M, j) = (w == gen_0) ? zerocol(lg(splitting_Q2_M) - 1)
                                    : gmulsg(w[1], gel(map_Q2_M, w[2]));
  }
  if(!gequal0(p))
    map_Q1_M = gmodulo(map_Q1_M, p);
  return gerepilecopy(av, mkvec2(splitting_Q1_M, map_Q1_M));
}


/***

    Boundary symbols

***/

/*
  Given integers c, d, n with gcd(c, d, n) = 1, find integers a, b
  such that a*d - b*c == 1 (mod n).
*/
static GEN
matrix_lift_mod_n(long c, long d, long n) {
  long g, p, q, r, s;
  GEN result;

  g = cbezout(c, d, &p, &q);
  if(cbezout(g, n, &r, &s) != 1)
    pari_err(e_MISC, "matrix_lift_mod_n: entries not coprime");
  result = cgetg(3, t_MAT);
  gel(result, 1) = mkvecsmall2(smodss(q*r, n), c);
  gel(result, 2) = mkvecsmall2(smodss(-p*r, n), d);
  return result;
}

static GEN
boundary_symbol_to_basis_pmGamma_1(long n, long weight, GEN cusps, GEN sym) {
  long a, b, i, sgn;
  GEN cusp;

  a = smodss(sym[1], n);
  b = smodss(sym[2], n);
  if(2*b > n) {
    a = -a;
    b = n - b;
    sgn = -1;
  } else {
    sgn = 1;
  }
  a = smodss(a, cgcd(b, n));
  if(b == 0 && 2 * a > n) {
    a = n - a;
    sgn = -sgn;
  }
  if(2 * b == n && 2 * a > b) {
    a = b - a;
    sgn = -sgn;
  }
  cusp = mkvecsmall2(a, b);
  for(i = 1; i < lg(cusps); i++) {
    if(gequal(gel(cusps, i), cusp)) {
      cgiv(cusp);
      return mkvecsmall2((weight % 2 == 0) ? 1 : sgn, i);
    }
  }
  pari_err(e_MISC, "boundary_symbol_to_basis_Gamma_1: cusp not found");
  return NULL;
}

/*
  Return a vector [c, i], such that the image of  sym  in B_k(\Gamma_1(n))
  equals  c \times(i-th cusp).
*/
GEN
boundary_symbol_to_basis(GEN group, long weight, GEN sym) {
  long group_type = modular_group_type(group);

  if(group_type == GAMMA_0 || group_type == GAMMA_1) {
    return mkvecsmall2(1, modular_group_cusp_index(group, sym));
  } else if(group_type == pmGAMMA_1) {
    return boundary_symbol_to_basis_pmGamma_1(modular_group_level(group), weight,
					      modular_group_cusps(group), sym);
  } else {
    pari_err(e_IMPL, "boundary_symbol_to_basis for groups other than Gamma_0 and Gamma_1");
    return NULL;
  }
}

/*
  Apply the boundary map to the Manin symbol sym.
  Return the answer as a vector with respect to the standard basis
  of the space B_k(\Gamma).
*/
GEN
boundary_map_on_manin_symbol(GEN group, long weight, GEN sym) {
  long n = modular_group_level(group), c, d, j;
  GEN g, v, w;

  if(weight == 2) {
    j = 0;
    c = smodss(sym[1], n);
    d = smodss(sym[2], n);
  } else {
    j = sym[1];
    c = smodss(sym[2], n);
    d = smodss(sym[3], n);
  }
  g = matrix_lift_mod_n(c, d, n);
  v = zerocol(modular_group_num_cusps(group));
  if(j == weight - 2) {
    w = boundary_symbol_to_basis(group, weight, mkvecsmall2(coeff(g, 1, 1), coeff(g, 2, 1)));
    gel(v, w[2]) = stoi(w[1]);
  }
  if(j == 0) {
    w = boundary_symbol_to_basis(group, weight, mkvecsmall2(coeff(g, 1, 2), coeff(g, 2, 2)));
    gel(v, w[2]) = subii(gel(v, w[2]), stoi(w[1]));
  }
  return v;
}
