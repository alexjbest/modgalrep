z4; w4; z3; w3; z2; w2; z1; w1; z0; w0; z; t;  \\ variable ordering

install(eltreltoabs, "GG");
install(expi, "lG");
install(nf_rnfeq, "mGG");
install(roots_to_pol, "Gn");

/*
  Given a (possibly recursive) object  f  that contains the
  variable  x  but has degree 0 in  x, eliminate  x  from  f.
*/
descend_to_base_field(f, x) =
{
   if(type(f) == "t_POL" || type(f) == "t_VEC",
      apply(a -> descend_to_base_field(a, x), f),
      my(g = liftpol(f));
      if(poldegree(g, x) <= 0,
	 polcoeff(g, 0, x),
	 error("coefficients of f do not lie in the base field")));
}

filename(s, p) = concat([s, "_", Str(p), ".gp"]);

polynomials(p) =
{
   my(V = readvec(filename("values", p)));
   liftall(if(#V == 3, [roots_to_pol(concat(Vec(V[1])), 'x)],
	      [roots_to_pol(concat(Vec(V[1])), 'x),
	       roots_to_pol(concat(Vec(V[3])), 'y)]));
}

/* G  is given as a list of elements.  */
action_orbits(X, G, action) =
{
   my(X = Set(X), Q = [], Gx);
   while(X != [],
	 Gx = Set([action(g, X[1]) | g <- G]);
	 Q = concat(Q, [Gx]);
	 X = setminus(X, Gx));
   Q;
}

/*
  Given a prime number  l  and a subgroup  H  of  F_l^\times,
  return the set of orbits for  H  acting on  F_l^2 \ {(0, 0)}.
*/
scalar_multiplication_subgroup_orbits(l, H) =
{
   my(V = concat(vector(l, i, vector(l, j, Mod([i, j], l)))),
      action = (g, x) -> g * x);
   action_orbits(setminus(Set(V), [Mod([0, 0], l)]), H, action);
}

/* Return the set of orbits for  F_l^\times  acting on  F_l^2 \ {(0, 0)}.  */
scalar_multiplication_orbits(l) =
{
   scalar_multiplication_subgroup_orbits(l, vector(l - 1, i, Mod(i, l)));
}

values_by_scalar_multiplication_subgroup_orbit(V, H) =
{
   my(l = matsize(V)[1]);
   [[V[liftint(a[1]) + 1, liftint(a[2]) + 1] | a <- orbit]
    | orbit <- scalar_multiplication_subgroup_orbits(l, H)];
}

values_by_scalar_multiplication_orbit(V) =
{
   my(l = matsize(V)[1]);
   values_by_scalar_multiplication_subgroup_orbit(V, vector(l - 1, i, Mod(i, l)));
}

scalar_multiplication_traces(V) = apply(vecsum, values_by_scalar_multiplication_orbit(V));

projective_polynomial(p) =
{
   V = readvec(filename("values", p))[1];
   liftall(roots_to_pol(scalar_multiplication_traces(V), 't));
}

projectivisation(p) =
{
   V = readvec(filename("values", p))[1];
   x = varhigher("x", variable(V));
   l = matsize(V)[1];
   T = scalar_multiplication_traces(V);
   W = matrix(l, l, i, j,
	      if(j == 1, if(i == 1, 0, T[l + 1]),
		 T[(i - 1)/(j - 1) % l + 1]));
   liftall(polinterpolate(concat(Vec(V))[2..l^2],
			  concat(Vec(W))[2..l^2], x));
}

relative_polynomial(p) =
{
   [g, a] = readvec("projective_polynomial_reduced.gp");
   V = readvec(filename("values", p))[1];
   x = variable(V);
   z = varhigher("z", x);
   y = varhigher("y", z);
   T = scalar_multiplication_traces(V);
   R = [roots_to_pol(v, z) | v <- values_by_scalar_multiplication_orbit(V)];
   liftall(subst(polinterpolate(T, R, y), y, a));
}

/* Compute a maximal chain of subgroups of  G.  */
subgroup_chain(G) =
{
   my(r = length(G.cyc),
      d = vector(r, j, 1),
      H = [matid(r)],
      F, p, i, j);
   p = if(G.no == 1, [],
	  F = factorint(G.no);
	  Vecrev(concat(vector(matsize(F)[1], i,
			       vector(F[i, 2], j, F[i, 1])))));
   for(i = 1, #p,
      j = [j | j <- [1..r], (G.cyc[j]/d[j]) % p[i] == 0][1];
      d[j] *= p[i];
      H = concat(H, [matdiagonal(d)]));
   [G, H];
}

subgroup_chain_length() = length(readvec("subgroups.gp")[2]) - 1;

/* H  is given by a matrix in HNF form, determinant = index.  */
subgroup_elements(G, S_H) =
{
   my(r = length(G.cyc),
      M_G = matdiagonal(G.cyc),
      M_H = matsolve(S_H, M_G),
      D, U, V, H, i, j, e, h);
   [U, V, D] = matsnf(M_H, 1);
   H = [];
   forvec(j = [[0, D[i, i] - 1] | i <- [1..r]],
	  e = S_H * matsolve(U, j~);
	  h = prod(i = 1, r, G.gen[i]^(e[i]));
	  H = concat(H, [h]));
   H;
}

compute_intermediate_polynomial(V, orbits_H, var, b) =
{
   my(W = [[vecsum([V[liftint(a[1]) + 1, liftint(a[2]) + 1]
		    | a <- orbit]) | orbit <- orb] | orb <- orbits_H],
      T = [vecsum(v) | v <- W],
      z = varhigher(var, variable(V)),
      R = [roots_to_pol(v, z) | v <- W],
      y = varhigher("y", z));
   liftall(subst(polinterpolate(T, R, y), y, b));
}

/*
  Return defining polynomials for the two fields cut out by the
  k-th subgroup of the selected chain in  (\Z/n\Z)^\times.
*/
intermediate_polynomials(k, p) =
{
   V = readvec(filename("values", p));
   if(k == 0,
      z = varhigher("z0", variable(V[1]));
      return(if(#V == 3,
		[liftall(roots_to_pol(scalar_multiplication_traces(V[1]), z))]
		,
		w = varhigher("w0", variable(V[3]));
		[liftall(roots_to_pol(scalar_multiplication_traces(V[1]), z)),
		 liftall(roots_to_pol(scalar_multiplication_traces(V[3]), w))])));
   [G, subgroups] = readvec("subgroups.gp");
   Hprev = subgroup_elements(G, subgroups[k]);
   H = subgroup_elements(G, subgroups[k + 1]);
   l = matsize(V[1])[1];
   orbits_Hprev = scalar_multiplication_subgroup_orbits(l, Hprev);
   orbits_H = [action_orbits(orbit, H, (g, x) -> g * x)
	       | orbit <- orbits_Hprev];
   if(#V == 3,
      [g, a, b] = readvec(concat(["intermediate_polynomials_reduced_", Str(k - 1), ".gp"]));
      [compute_intermediate_polynomial(V[1], orbits_H, concat("z", Str(k)), b)]
      ,
      [g1, a1, b1, g2, a2, b2] = readvec(concat(["intermediate_polynomials_reduced_", Str(k - 1), ".gp"]));
      [compute_intermediate_polynomial(V[1], orbits_H, concat("z", Str(k)), b1),
       compute_intermediate_polynomial(V[3], orbits_H, concat("w", Str(k)), b2)]);
}

/* Helper function for reduced_bases.  */
red_basis(n, K, alpha) =
{
   matconcat(vectorv(#K, i,
		     matconcat([nfalgtobasis(K[i], a)
				| a <- powers(alpha[i], n - 1)])))^-1;
}

/* Return the changes of basis from the LLL-reduced bases to the power bases.  */
reduced_bases() =
{
   my(k = subgroup_chain_length(),
      c = readvec("origins.gp"),
      f = readvec(concat(["intermediate_polynomials_reduced_", Str(k), ".gp"])),
      alpha = if(#f == 3, [f[3]], [f[3], f[6]]),
      n = poldegree(f[1]) + 1,
      K = readvec(concat(["intermediate_fields_", Str(k), ".gp"])));
   vector(#c, j, red_basis(n, [nfinit(variable(f[j])), K[j]], [c[j], alpha[j]]));
}

addition(p) =
{
   V = readvec(filename("values", p))[1];
   x = variable(V);
   z = varhigher("z", x);
   y = varhigher("y", z);
   l = matsize(V)[1];
   A = matrix(l, l, i0, j0,
	      W = matrix(l, l, i, j,
			 V[(i + i0 - 2) % l + 1, (j + j0 - 2) %l + 1]);
	      polinterpolate(concat(Vec(V)), concat(Vec(W)), z));
   liftall(polinterpolate(concat(Vec(V)), concat(Vec(A)), y));
}

scalar_multiplication(p, r) =
{
   V = readvec(filename("values", p))[1];
   z = varhigher("z", variable(V));
   l = matsize(V)[1];
   Vr = matrix(l, l, i, j,
	       V[(i - 1)*r % l + 1, (j - 1)*r % l + 1]);
   liftall(polinterpolate(concat(Vec(V)), concat(Vec(Vr)), z));
}

scalar_multiplication_relative(p, r) =
{
   [g, a] = readvec("projective_polynomial_reduced.gp");
   V = readvec(filename("values", p))[1];
   z = varhigher("z", variable(V));
   y = varhigher("y", z);
   l = matsize(V)[1];
   W = values_by_scalar_multiplication_orbit(V);
   F = [polinterpolate(v, vector(l - 1, i, v[i*r % l]), z) | v <- W];
   T = scalar_multiplication_traces(V);
   liftall(subst(polinterpolate(T, F, y), y, a));
}

pairing(p) =
{
   my(V = readvec(filename("values", p)),
      values = V[1],
      m = matsize(values)[1],
      v = variable(values),
      g = ffgen(component(values[1, 1], 1), v),
      x, y, z, Z, Y1, Y2, W, Wpol);
   if(v == 0, v = 'x);
   y = varhigher("y", v);
   x = varhigher("x", y);
   if(#V == 3,
      values_dual = values;
      z = V[3];
      Z = [1, z; z^-1, 1],
      values_dual = V[3];
      Z = V[5]);
   Z = apply(z -> powers(z, m - 1), Z);
   values = subst(liftpol(values), v, g) * g^0;
   values_dual = subst(liftpol(values_dual), v, g) * g^0;
   Z = subst(liftpol(Z), v, g) * g^0;
   Y1 = matrix(m, m, i0, j0,
	       Y2 = matrix(m, m, i, j,
			   Z[1,1][(i0-1)*(i-1) % m + 1]
			   * Z[2,1][(j0-1)*(i-1) % m + 1]
			   * Z[1,2][(i0-1)*(j-1) % m + 1]
			   * Z[2,2][(j0-1)*(j-1) % m + 1]);
	       polinterpolate(concat(Vec(values_dual)),
			      concat(Vec(Y2)), y));
   Wpol = polinterpolate(concat(Vec(values)),
			 concat(Vec(Y1)), x);
   W = matrix(m^2, m^2, i, j,
	      polcoeff(polcoeff(Wpol, i - 1, x), j - 1, y));
   /* Return the matrix of the dual pairing (this has smaller height).  */
   print(W~^-1);
}

pairing_red(p) =
{
   my(T = Mod(read(filename("pairing", p)), p),
      P = Mod(readvec("reduced_bases.gp"), p));
   print(liftint(if(#P == 1, P[1]~ * T * P[1],
		    P[1]~ * T * P[2])));
}

/*
  Return the list of primes for which we have computed
  the required data and for which the weight is minimal.
*/
suitable_primes() =
{
   P = readvec("primes.gp");
   W = [if(#(V = readvec(filename("values", p))) == 3,
	   V[2], [V[2], V[4]]) | p <- P];
   w = if(#W[1] == 1, vecmin(W),
	  [vecmin([v[1] | v <- W]), vecmin([v[2] | v <- W])]);
   ind = select(x -> x == w, W, 1);
   return(vecextract(P, ind));
}

/*
  Return the list of suitable primes  p  for which in addition
  the polynomial modulo  p  is square-free.
*/
suitable_primes_poly() =
{
   [p | p <- readvec("suitable_primes.gp"),
    issquarefree(Mod(readvec(filename("poly", p))[1], p))];
}

/*
  Return the list of suitable primes  p  for which in addition
  the projective polynomial modulo  p  is square-free.
*/
suitable_primes_ppoly() =
{
   [p | p <- readvec("suitable_primes.gp"),
    issquarefree(Mod(read(filename("ppoly", p)), p))];
}

/*
  Return the list of suitable primes  p  for which in addition
  both  k-th intermediate polynomials modulo  p  are square-free.
*/
suitable_primes_ipoly(k) =
{
   my(P, f, g, basename = concat("ipoly_", Str(k)));
   if(k == 0,
      P = readvec("suitable_primes.gp");
      return([p | p <- P,
	      vecprod([issquarefree(Mod(g, p)) | g <- readvec(filename(basename, p))])]));
   P = readvec(concat(["suitable_primes_ipoly_red_", Str(k - 1), ".gp"]));
   f = readvec(concat(["intermediate_polynomials_reduced_", Str(k - 1), ".gp"]));
   f = if(#f == 3, [f[1]], [f[1], f[4]]);
   [p | p <- P, if(1,
		   g = readvec(filename(basename, p));
		   vecprod([issquarefree(polresultant(Mod(g[i], p), Mod(f[i], p), variable(f[i])))
			    | i <- [1..#f]]))];
}

/*
  Return the primes  p in suitable_primes_ppoly  for which in addition
  the change of variables to the reduced projective polynomial has no
  p  in the denominator.
*/
suitable_primes_rpoly() =
{
   v = Vec(lift(readvec("projective_polynomial_reduced.gp")[2]));
   [p | p <- readvec("suitable_primes_ppoly.gp"),
    iferr(v % p; 1, e, 0)];
}

/*
  Return the primes  p in suitable_primes_ipoly(k)  for which in addition
  the changes of variables to both  k-th intermediate polynomials have no
  p  in the denominator.
*/
suitable_primes_ipoly_red(k) =
{
   my(v = liftpol(readvec(concat(["intermediate_polynomials_reduced_", Str(k), ".gp"]))));
   v = if(#v == 3, Vec(v[3]), concat(Vec(v[3]), Vec(v[6])));
   [p | p <- readvec(concat(["suitable_primes_ipoly_", Str(k), ".gp"])),
    iferr(v % p; 1, e, 0)];
}

/*
  Return the list of suitable primes  p  for which both the polynomial
  and the dual polynomial modulo  p  are square-free.
*/
test(f, p) = { issquarefree(Mod(f[1], p)) &&
		(#f == 1 || issquarefree(Mod(f[2], p))); }

suitable_primes_dual() =
{
   [p | p <- readvec("suitable_primes.gp"),
    test(readvec(filename("poly", p)), p)];
}

/*
  Return the primes  p  in suitable_primes_dual for which both
  changes of basis to the LLL-reduced integral bases have no  p
  in the denominator.
*/
suitable_primes_pairing() =
{
   my(P = readvec(concat(["suitable_primes_dual.gp"])),
      M = readvec("reduced_bases.gp"));
   [p | p <- P, iferr(M % p; 1, e, 0)];
}

bit_precision() =
{
   P = readvec("suitable_primes.gp");
   N = prod(i = 1, #P, P[i]);
   expi(N);
}

matheight(x) =
{
   vecmax([vecmax([log(max(abs(numerator(z)), denominator(z))) | z <- y])
	   | y <- x]);
}

polheight(f) =
{
   if(type(f) == "t_POL",
      vecmax(apply(polheight, Vec(f))),
      log(max(abs(numerator(f)), denominator(f))));
}

height_polynomial() = polheight(read("polynomial.gp"));

height_projective_polynomial() = polheight(read("projective_polynomial.gp"));

height_relative_polynomial() = polheight(read("relative_polynomial.gp"));

height_addition() = polheight(read("addition.gp"));

height_scalar_mul() = polheight(read("scalar_mul.gp"));

info() =
{
   rnfeq = read("relative_equation.gp");
   K = read("projective_field.gp");
   polabs = rnfeq[1];
   pol = rnfeq[5];
   r1 = polsturm(polabs);
   r2 = (poldegree(polabs) - r1) / 2;
   print("[K:Q] = ", poldegree(K.pol));
   print("[L:K] = ", poldegree(pol));
   print("disc(Z_K/Z) = ", K.disc);
   iferr(L = read("relative_field.gp");
	 D_abs = (-1)^r2 * abs(K.disc)^poldegree(pol) * idealnorm(K, L.disc[1]);
	 print("disc(Z_L/Z_K) = ", L.disc[1]);
	 print("factorisation of |disc(Z_L/Z)| = ", factor(D_abs)),
	 err,
	 /* do nothing */);
   print("sign(projective field) = ", K.sign);
   print("sign(absolute field) = ", [r1, r2]);
   print("projective polynomial = ", K.pol);
   print("relative polynomial = ", pol);
   print("absolute polynomial = ", polabs);
   print("h(projective_polynomial) = ", polheight(read("projective_polynomial.gp")));
   print("h(relative_polynomial) = ", polheight(lift(read("relative_polynomial.gp"))));
   print("h(projective_polynomial_reduced) = ", polheight(K.pol));
   print("h(relative_polynomial_reduced) = ", polheight(pol));
   print("h(absolute_polynomial) = ", polheight(polabs));
   iferr(F = readvec("absolute_polynomial_reduced.gp")[1];
	 print("h(absolute_polynomial_reduced) = ", polheight(F)),
	 err,
	 /* do nothing */);
   iferr(print("h(pairing) = ", matheight(read("pairing.gp"))),
	 err,
	 /* do nothing */);
   iferr(print("h(addition) = ", polheight(read("addition.gp"))),
	 err,
	 /* do nothing */);
   print("h(scalar_mul_rel) = ", polheight(liftall(read("scalar_mul_rel.gp"))));
   print("h(scalar_mul_rel_reduced) = ", polheight(liftall(read("scalar_mul_rel_reduced.gp"))));
   print("h(scalar_mul_absolute) = ", polheight(lift(read("scalar_mul_absolute.gp"))));
}

/*
  Like bestappr(f), but try in addition to replace the
  polynomial  F(x)  by the shifted polynomial  F(x - a),
  where  a  is an integer such that the coefficient of
  x^(deg(F) - 1) is small.
*/
bestappr_shift(F) =
{
   my(G = bestappr(F), a, d, x);
   if(G != [], return(G));
   d = poldegree(F);
   a = bestappr(polcoeff(F, d - 1));
   if(a == [], return([]));
   a = round(a/d);
   x = variable(F);
   G = bestappr(subst(F, x, x - a));
   if(G == [], return([]));
   G = subst(G, x, x + a);
   G;
}

chinese_from_files(prefix, P) =
{
   my(x, y, z, N, p, u, v, d);
   if(P == [], return([]));
   N = P[1];
   x = read(filename(prefix, P[1]));

   for(i = 2, #P,
      p = P[i];
      y = read(filename(prefix, p));
      [u, v, d] = gcdext(N, p);
      if(d != 1, error("gcd is not 1"));
      z = x + u*N*(y - x);
      \\ now u*N + v*p = 1 and z = y + v*p*(x - y),
      \\ so z == x (mod N) and z == y (mod p)
      N *= p;
      x = z % N);
   Mod(x, N);
}

/*
  Reconstruct a polynomial  P  whose splitting field is
  the field of definition of the Galois representation.
*/
reconstruct_polynomial() =
{
   my(P = readvec("suitable_primes.gp"),
      Fp = [Mod(readvec(filename("poly", p))[1], p) | p <- P],
      F = bestappr_shift(chinese(Fp)));
   if(F == [],
      error("cannot reconstruct polynomial"));
   F;
}

reconstruct_polynomials() =
{
   my(P = readvec("suitable_primes.gp"),
      fp = [Mod(readvec(filename("poly", p)), p) | p <- P],
      f = bestappr(chinese(fp)));
   if(f == [],
      error("cannot reconstruct polynomials"));
   f;
}

reconstruct_projective_polynomial() =
{
   P = readvec("suitable_primes.gp");
   Fp = [Mod(read(filename("ppoly", p)), p) | p <- P];
   F = bestappr_shift(chinese(Fp));
   if(F == [],
      error("cannot reconstruct projective polynomial"));
   F;
}

reconstruct_relative_polynomial() =
{
   P = readvec("suitable_primes_rpoly.gp");
   Gp = [Mod(read(filename("rpoly", p)), p) | p <- P];
   G = bestappr(chinese(Gp));
   if(G == [],
      error("cannot reconstruct relative polynomial"));
   Mod(G, readvec("projective_polynomial_reduced.gp")[1]);
}

reconstruct_intermediate_polynomials(k) =
{
   if(k == 0,
      P = readvec("suitable_primes.gp");
      Fp = [Mod(readvec(filename("ipoly_0", p)), p) | p <- P];
      F = bestappr(chinese(Fp));
      if(F == [],
	 error("cannot reconstruct projective polynomials"));
      return(F));
   P = readvec(concat(["suitable_primes_ipoly_red_", Str(k - 1), ".gp"]));
   Gp = [Mod(readvec(filename(concat("ipoly_", Str(k)), p)), p) | p <- P];
   G = bestappr(chinese(Gp));
   if(G == [],
      error("cannot reconstruct relative polynomials"));
   f = readvec(concat(["intermediate_polynomials_reduced_", Str(k - 1), ".gp"]));
   f = if(#f == 3, [f[1]], [f[1], f[4]]);
   [Mod(G[i], f[i]) | i <- [1..#f]];
}

reconstruct_origin() =
{
   P = readvec("suitable_primes.gp");
   L = [lift(readvec(filename("values", p))[1][1,1]) | p <- P];
   bestappr(chinese(L));
}

reconstruct_origins() =
{
   my(P = readvec("suitable_primes.gp"),
      L = [if(1,
	      V = readvec(filename("values", p));
	      V = if(#V == 3, [V[1]], [V[1], V[3]]);
	      [liftpol(v[1, 1]) | v <- V])
	   | p <- P]);
   L = bestappr(chinese(L));
   if(L == [],
      error("cannot reconstruct origins"));
   L;
}

reconstruct_addition() =
{
   P = readvec("suitable_primes_poly.gp");
   L = [Mod(read(filename("addition", p)), p) | p <- P];
   alpha0 = chinese(L);
   alpha = bestappr(alpha0);
   if(alpha == [],
      error("cannot reconstruct addition"));
   f = read("polynomial.gp");
   x = variable(f);
   [y, z] = variables(alpha);
   if(substvec(alpha, [y, z], [z, y]) != alpha,
      error("addition is not commutative"));
   \\ The check below is very slow for  l >= 7.
   if(subst(f, x, Mod(Mod(alpha, subst(f, x, y)), subst(f, x, z))) != 0,
      error("alpha does not operate on the roots of f"));
   alpha;
}

reconstruct_scalar_mul() =
{
   P = readvec("suitable_primes_poly.gp");
   L = [Mod(read(filename("scalar_mul", p)), p) | p <- P];
   mu0 = chinese(L);
   mu = bestappr_shift(mu0);
   if(mu == [],
      error("cannot reconstruct scalar multiplication"));
   f = read("polynomial.gp");
   x = variable(f);
   z = variable(mu);
   if(subst(f, x, Mod(mu, subst(f, x, z))) != 0,
      error("mu does not operate on the roots of f"));
   mu;
}

reconstruct_projectivisation() =
{
   P = readvec("suitable_primes_poly.gp");
   L = [Mod(read(filename("projectivisation", p)), p) | p <- P];
   iota0 = chinese(L);
   iota = bestappr_shift(iota0);
   if(iota == [],
      error("cannot reconstruct projectivisation"));
   iota = Mod(iota, read("polynomial_star.gp"));
   g = read("projective_polynomial.gp");
   t = variable(g);
   if(subst(g, t, iota) != 0,
      error("projectivisation inconsistent"));
   iota;
}

reconstruct_pairing() =
{
   my(P = readvec("suitable_primes_dual.gp"),
      W = chinese_from_files("pairing", P));
   W = bestappr(W);
   if(W == [],
      error("cannot reconstruct pairing matrix"));
   print(W);
}

reconstruct_reduced_pairing() =
{
   my(P = readvec("suitable_primes_pairing.gp"),
      W = chinese_from_files("pairing_red", P));
   W = bestappr(W);
   if(W == [],
      error("cannot reconstruct reduced pairing matrix"));
   print(W);
}

reduced_dual_pair() =
{
   my(k = subgroup_chain_length(),
      K = readvec(concat(["intermediate_fields_", Str(k), ".gp"])),
      T = read("reduced_pairing.gp"));
   if(#K == 1, [[variable(K[1]), K[1].pol], [[1], K[1].zk], T],
      [[variable(K[1]), K[1].pol], [[1], K[1].zk],
       [variable(K[2]), K[2].pol], [[1], K[2].zk], T]);
}

projectivisation_to_reduced() =
{
   iota = read("projectivisation.gp");
   [g, a] = readvec("projective_polynomial_reduced.gp");
   t = variable(g);
   kappa = subst(lift(modreverse(a)), t, iota);
   if(subst(g, t, kappa) != 0,
      error("projectivisation_to_reduced inconsistent"));
   kappa;
}

relative_polynomial_reduced() =
{
   G = read("relative_polynomial.gp");
   K = read("projective_field.gp");
   rnfpolredbest(K, G, 1);
}

relative_equation() =
{
   g = readvec("relative_polynomial_reduced.gp")[1];
   K = read("projective_field.gp");
   nf_rnfeq(K, g);
}

/*
  Find a simpler relative equation for the quadratic extension of K
  defined by f, assuming this extension is ramified at most at the
  prime ideals in P.
*/
rnfpolred_quadratic(K, f, P) =
{
   my(x, D, Dp, D0, A, A_red, s, D_red, g, root);
   if(poldegree(f) != 2,
      error("not a quadratic polynomial in rnfpolred_quadratic"));
   x = variable(f);
   D = poldisc(f);
   \\ product of the prime ideals dividing D to an odd power
   Dp = idealfactorback(K, [p | p <- P, idealval(K, D, p) % 2 == 1]);
   D0 = idealdiv(K, D, Dp);
   if(!idealispower(K, D0, 2, &A),
      error("ideal not a square"));
   \\ A = a * A_red with a in K^* and A_red integral and ``small''
   [A_red, s] = idealred(K, [A, 1]);
   D_red = nfeltmul(K, nfeltpow(K, s, -2), D);
   g = x^2 - nfbasistoalg(K, D_red);
   root = Mod((-polcoeff(f, 1) + nfbasistoalg(K, s)*x)
	      / (2 * polcoeff(f, 2)), g);
   [g, root];
}

/* As rnfpolredbest(K, f, 3), but with a special case for quadratic extensions.  */
rnfpolredbest_special(K, f) =
{
   my(P, g, root, h, a, b);
   if(poldegree(f) != 2,
      return(rnfpolredbest(K, f, 3)));
   P = concat([idealfactor(K, p)[,1]~
	       | p <- read("ramified_primes.gp")]);
   [g, root] = rnfpolred_quadratic(K, f, P);
   [h, a, b] = rnfpolredbest(K, g, 3);
   [h, a, substvec(liftpol(liftpol(root)), [variable(K.pol), variable(g)], [a, b])];
}

intermediate_polynomials_reduced(k) =
{
   my(K, f = readvec(concat(["intermediate_polynomials_", k, ".gp"])), v);
   if(k == 0,
      concat([if(1, v = polredbest(f[i], 1); [v[1], 0, v[2]]) | i <- [1..#f]])
      ,
      K = readvec(concat(["intermediate_fields_", k - 1, ".gp"]));
      concat([rnfpolredbest_special(K[i], f[i]) | i <- [1..#f]]));
}

intermediate_fields(k) =
{
   my(F = readvec(concat(["intermediate_polynomials_reduced_", k, ".gp"])),
      P = read("ramified_primes.gp"));
   F = if(#F == 3, [F[1]], [F[1], F[4]]);
   [nfinit([f, P]) | f <- F];
}

reconstruct_scalar_mul_rel() =
{
   f = read("relative_polynomial.gp");
   g = readvec("projective_polynomial_reduced.gp")[1];
   P = readvec("suitable_primes_strict.gp");
   L = [Mod(read(filename("scalar_mul_rel", p)), p) | p <- P];
   mu0 = chinese(L);
   mu = bestappr(mu0);
   if(mu == [],
      error("cannot reconstruct scalar multiplication"));
   mu = Mod(Mod(mu, g), f);
   z = variable(f);
   if(subst(f, z, mu) != 0,
      error("mu does not operate on the roots of f"));
   mu;
}

scalar_mul_rel_reduced() =
{
   [f, a] = readvec("relative_polynomial_reduced.gp");
   [z, t] = variables(f);
   mu = read("scalar_mul_rel.gp");
   mu = Mod(subst(lift(modreverse(a)), z, subst(lift(mu), z, a)), f);
   if(subst(f, z, mu) != 0,
      error("mu does not operate on the roots of f"));
   mu;
}

scalar_mul_absolute() =
{
   rnfeq = read("relative_equation.gp");
   f = rnfeq[1];
   mu = read("scalar_mul_rel_reduced.gp");
   \\ K = \Q(alpha),  L = K(beta),
   \\ L = \Q(beta + k alpha)
   \\ mu(beta + k alpha) = mu(beta) + k alpha
   mu = Mod(eltreltoabs(rnfeq, liftpol(mu)) + rnfeq[3] * rnfeq[2], f);
   z = variable(f);
   if(subst(f, z, mu) != 0,
      error("mu does not operate on the roots of f"));
   mu;
}
