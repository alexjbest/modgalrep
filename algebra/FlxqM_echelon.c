#include <pari/pari.h>
#include <pari/paripriv.h>  /* pswap */

#include "FlxqM_echelon.h"
#include "FlxqM_solve_triangular.h"

static const ulong FlxqM_ECHELON_LIMIT = 5;

static GEN
indexcompl(GEN v, long n) {
  long i, j = 1, k = 1, m = lg(v) - 1;
  GEN w = cgetg(n - m + 1, t_VECSMALL);
  for (i = 1; i <= n; i++) {
    if (j <= m && v[j] == i)
      j++;
    else
      w[k++] = i;
  }
  return w;
}

static ulong
FlxqM_echelon_gauss(GEN A, GEN *R, GEN *C, GEN T, ulong p) {
  long i, j, k, m = nbrows(A), n = lg(A) - 1, pr, pc;
  pari_sp av;
  GEN u, v;

  *R = cgetg(m + 1, t_VECSMALL);
  av = avma;
  for (j = 1, pr = 0; j <= n; j++) {
    for (pr++, pc = 0; pr <= m; pr++) {
      for (k = j; k <= n; k++) {
        v = Flx_rem(gcoeff(A, pr, k), T, p);
        gcoeff(A, pr, k) = v;
        if (!pc && lgpol(v) > 0)
          pc = k;
      }
      if (pc)
        break;
    }
    if (!pc)
      break;
    (*R)[j] = pr;
    if (pc != j)
      /* swap(gel(A, j), gel(A, pc)) breaks when using -fstrict-aliasing */
      pswap(((GEN **)A)[j], ((GEN **)A)[pc]);
    u = Flxq_inv(gcoeff(A, pr, j), T, p);
    for (i = pr + 1; i <= m; i++) {
      v = Flxq_mul(gcoeff(A, i, j), u, T, p);
      gcoeff(A, i, j) = v;
      for (k = j + 1; k <= n; k++) {
        gcoeff(A, i, k) = Flx_sub(gcoeff(A, i, k),
                                  Flx_mul(gcoeff(A, pr, k), v, p), p);
      }
    }
    if (gc_needed(av, 2))
      A = gerepilecopy(av, A);
  }
  setlg(*R, j);
  *C = vecslice(A, 1, j - 1);
  return j - 1;
}

/* column echelon form */
ulong
FlxqM_echelon(GEN A, GEN *R, GEN *C, GEN T, ulong p) {
  long i, j, j1, j2, m = nbrows(A), n = lg(A) - 1, n1, r, r1, r2;
  GEN A1, A2, R1, R1c, C1, R2, C2;
  GEN A12, A22, B2, C11, C21, M12;
  pari_sp av = avma;

  if (m < FlxqM_ECHELON_LIMIT || n < FlxqM_ECHELON_LIMIT)
    return FlxqM_echelon_gauss(shallowcopy(A), R, C, T, p);

  n1 = (n + 1)/2;
  A1 = vecslice(A, 1, n1);
  A2 = vecslice(A, n1 + 1, n);
  r1 = FlxqM_echelon(A1, &R1, &C1, T, p);
  if (!r1)
    return FlxqM_echelon(A2, R, C, T, p);
  else if (r1 == m) {
    *R = R1;
    *C = C1;
    return r1;
  }
  R1c = indexcompl(R1, m);
  C11 = rowpermute(C1, R1);
  C21 = rowpermute(C1, R1c);
  if (R1[r1] != r1) {
    GEN zero = zero_Flx(get_Flx_var(T));
    for (j = 1; j <= r1; j++)
      for (i = 1; i <= R1[j] - j; i++)
        gcoeff(C21, i, j) = zero;
  }
  A12 = rowpermute(A2, R1);
  A22 = rowpermute(A2, R1c);
  M12 = FlxqM_rsolve_lower_unit(C11, A12, T, p);
  B2 = FlxM_sub(A22, FlxqM_mul(C21, M12, T, p), p);
  r2 = FlxqM_echelon(B2, &R2, &C2, T, p);
  if (!r2) {
    *R = R1;
    *C = C1;
    r = r1;
  } else {
    R2 = perm_mul(R1c, R2);
    C2 = rowpermute(vconcat(zero_FlxM(r1, r2, get_Flx_var(T)), C2),
                    perm_inv(vecsmall_concat(R1, R1c)));
    r = r1 + r2;
    *R = cgetg(r + 1, t_VECSMALL);
    *C = cgetg(r + 1, t_MAT);
    for (j = j1 = j2 = 1; j <= r; j++) {
      if (j2 > r2 || (j1 <= r1 && R1[j1] < R2[j2])) {
        gel(*C, j) = gel(C1, j1);
        (*R)[j] = R1[j1++];
      } else {
        gel(*C, j) = gel(C2, j2);
        (*R)[j] = R2[j2++];
      }
    }
  }
  if (gc_needed(av, 1))
    gerepileall(av, 2, R, C);
  return r;
}
