#ifndef CURVES
#define CURVES

#include <math.h>  /* log */

#define MULTIPLY_VALUES 0
#define MULTIPLY_POWER_SERIES 1

GEN multiply_sections (GEN s, GEN t,
		       unsigned long multiplication_method,
		       unsigned long p, GEN T);
GEN multiply_section_subspace (GEN s, GEN W,
			       unsigned long multiplication_method, 
			       unsigned long p, GEN T);
GEN multiply_subsets (GEN W1, GEN W2,
		      unsigned long multiplication_method,
		      unsigned long p, GEN T);
GEN multiply_subspaces (GEN W1, GEN W2,
			unsigned long multiplication_method,
			unsigned long p, GEN T);
GEN multiply_subspaces_dim (GEN W1, GEN W2, unsigned long dimension,
			    unsigned long multiplication_method,
			    unsigned long p, GEN T);
GEN divide_subspaces (GEN V, GEN Z, GEN W,
		      unsigned long multiplication_method,
		      unsigned long p, GEN T);

#define log_ffsize(p, T) (T ? degpol (T) * log (p) : log (p))


/*** Curves ***/

/*
  A curve is represented as
  [info, p, T, multiplication_method,
   [V, V_2, V_3, V_4, ...], [IGS_V, IGS_V_2, ...], num_zeta].
*/
#define curve_base_field_characteristic(X) itos (gel (X, 2))
#define curve_base_field_polynomial(X) (gequal0 (gel (X, 3)) ? NULL : gel (X, 3))
#define curve_multiplication_method(X) itos (gel (X, 4))
#define curve_V(X, i) gmael (X, 5, i)
#define curve_max_V(X) (lg (gel (X, 5)) - 1)
#define curve_degree(X) (lg (curve_V (X, 2)) - lg (curve_V (X, 1)))
#define curve_genus(X) (lg (curve_V (X, 2)) - 2 * lg (curve_V (X, 1)) + 2)
#define curve_IGS_V(X, i) gmael (X, 6, i)
#define curve_max_IGS_V(X) (lg (gel (X, 6)) - 1)
#define curve_pivots_V(X, i) gmael (X, 7, i)
#define curve_numerator_zeta_function(X) gel (X, 8)

/*
  Return the degree of the divisor associated to W_D.
*/
#define curve_divisor_degree(X, W_D, i) (lg(curve_V(X, i)) - lg(W_D))

GEN curve_construct (GEN info, unsigned long p, GEN polynomial,
		     unsigned long multiplication_method, GEN V,
		     unsigned long max_power, GEN num_zeta_function);
GEN curve_base_field_cardinality (GEN X);
unsigned long curve_base_field_degree (GEN X);
GEN curve_count_points (GEN X);
GEN curve_multiply_sections (GEN X, GEN s, GEN t);
GEN curve_multiply_section_subspace (GEN X, GEN s, GEN V);
GEN curve_multiply_subspaces (GEN X, GEN V, GEN W);
GEN curve_multiply_subspaces_dim(GEN X, GEN V, GEN W, unsigned long dim);
GEN curve_divide_subspaces (GEN X, GEN Z, unsigned long i_Z,
			    GEN W, unsigned long i_W);

GEN numzeta_base_change (GEN z, int n);
GEN curve_base_change (GEN X, GEN ext);

#endif
