/***

    Modular symbols

***/

/*
  This file implements modular symbols of weight >= 2 for Gamma_0(n)
  and Gamma_1(n).  Forms with given character are not yet supported.
*/

#include <pari/pari.h>
#include "modular-groups.h"
#include "manin-symbols.h"
#include "modular-symbols.h"

/*
  Compute the space of modular symbols for the given group and weight
  over the prime field of characteristic  p, where  p  is a prime
  number or zero.
*/
GEN
modular_symbols(GEN group, long weight, GEN p) {
  pari_sp av = avma;
  GEN syms = manin_symbols(group, weight);
  GEN result = manin_symbols_basis(group, weight, syms, p ? p : gen_0);
  GEN basis = gel(result, 1);
  GEN syms_to_basis = gel(result, 2);
  long dim = lg(basis) - 1;
  GEN boundary_map = cgetg(dim + 1, t_MAT), cuspidal_subspace;
  long j;

  for(j = 1; j <= dim; j++)
    gel(boundary_map, j) = boundary_map_on_manin_symbol(group, weight,
							gel(syms, basis[j]));
  if(!p || gequal0(p)) {
    cuspidal_subspace = keri(boundary_map);
  } else {
    boundary_map = gmodulo(boundary_map, p);
    cuspidal_subspace = gmodulo(ker(boundary_map), p);
  }
  return gerepilecopy(av, mkvecn(7, group, stoi(weight), syms, basis,
				 syms_to_basis, boundary_map,
				 cuspidal_subspace));
}

ulong
modular_symbols_characteristic(GEN M) {
  GEN x = gcoeff(modular_symbols_manin_symbols_to_basis(M), 1, 1);
  return (typ(x) == t_INTMOD) ? itou(gel(x, 1)) : 0;
}

/*
  Return the matrix of the diamond operator <d> with respect to
  the distinguished basis of the space M of modular symbols.
*/
GEN
modular_symbols_diamond_operator(GEN M, long d) {
  pari_sp av = avma;
  GEN group = modular_symbols_group(M);
  long weight = modular_symbols_weight(M);
  if(cgcd(d, modular_group_level(group)) != 1)
    pari_err(e_MISC, "modular_symbols_diamond_operator: diamond operator not defined");
  GEN syms = modular_symbols_manin_symbols(M);
  GEN basis = modular_symbols_basis(M);
  GEN sym_to_basis = modular_symbols_manin_symbols_to_basis(M);
  long dim = lg(basis) - 1;
  GEN T = cgetg(dim + 1, t_MAT), s, t;
  long i, j;

  for(j = 1; j <= dim; j++) {
    s = gel(syms, basis[j]);
    t = (weight == 2) ? mkvecsmall2(d * s[1], d * s[2])
                      : mkvecsmall3(s[1], d * s[2], d * s[3]);
    i = manin_symbol_index(group, weight, t);
    gel(T, j) = gel(sym_to_basis, i);
  }
  return gerepilecopy(av, T);
}


/*
  Auxiliary routines for the function modular_symbols_hecke_operator_prime.
*/

/*
  Given integers c, d, n with gcd(c, d, n) = 1, find integers a, b,
  c', d' such that c == c' (mod n), d == d' (mod n), a*d' - b*c' = 1.

  Idea stolen from Sage (code by Justin Walker).
*/
static GEN
manin_symbol_to_SL2Z(long c, long d, long n) {
  long g, p, q, m;
  GEN result;

  c = smodss(c, n);
  d = smodss(d, n);
  g = cbezout(c, d, &p, &q);
  if(g != 1) {
    /* The gcd of c and d is > 1; we modify d.  */
    if(c == 0)
      c = n;
    /* Compute the largest factor of c that is coprime to dn.  */
    m = c;
    while(1) {
      g = cgcd(m, d);
      if(g == 1)
	break;
      m /= g;
    }
    while(1) {
      g = cgcd(m, n);
      if(g == 1)
	break;
      m /= g;
    }
    d += m*n;
  }
  if(cbezout(c, d, &p, &q) != 1)
    pari_err(e_MISC, "manin_symbol_to_SL2Z: entries not coprime");
  result = cgetg(3, t_MAT);
  gel(result, 1) = mkvecsmall2(q, c);
  gel(result, 2) = mkvecsmall2(-p, d);
  return result;
}

static GEN
make_2x2_matrix(long a, long b, long c, long d) {
  GEN m = cgetg(5, t_VECSMALL);
  m[1] = a; m[2] = b;
  m[3] = c; m[4] = d;
  return m;
}

static GEN
multiply_2x2_matrices(GEN g, GEN h) {
  return make_2x2_matrix(g[1]*h[1] + g[2]*h[3], g[1]*h[2] + g[2]*h[4],
			 g[3]*h[1] + g[4]*h[3], g[3]*h[2] + g[4]*h[4]);
}

static GEN
convert_2x2_matrix_to_MAT(GEN m) {
  GEN n = cgetg(3, t_MAT);
  gel(n, 1) = mkcol2(stoi(m[1]), stoi(m[3]));
  gel(n, 2) = mkcol2(stoi(m[2]), stoi(m[4]));
  return n;
}

/*
  Apply the matrix  gamma in GL_2(Q)^+  to the Manin symbol  sym.
*/
GEN
modular_symbols_matrix_on_manin_symbol(GEN M, GEN sym, GEN gamma) {
  GEN group = modular_symbols_group(M);
  long n = modular_group_level(group);
  long weight = modular_symbols_weight(M);
  long dim = modular_symbols_dimension(M);
  GEN sym_to_basis = modular_symbols_manin_symbols_to_basis(M);
  long c, d, l, i, index, s1, s2;
  GEN modsym, g, T, alpha, beta, x, cf, C, s, result;
  pari_sp av = avma;

  if(weight == 2) {
    c = smodss(sym[1], n);
    d = smodss(sym[2], n);
  } else {
    c = smodss(sym[2], n);
    d = smodss(sym[3], n);
    pari_err(e_IMPL, "manin_symbol_apply_matrix for weight > 2");
  }
  modsym = manin_symbol_to_SL2Z(c, d, n);
  modsym = make_2x2_matrix(coeff(modsym, 1, 1), coeff(modsym, 1, 2),
			   coeff(modsym, 2, 1), coeff(modsym, 2, 2));
  modsym = multiply_2x2_matrices(gamma, modsym);
  modsym = convert_2x2_matrix_to_MAT(modsym);
  alpha = gel(modsym, 1);
  beta = gel(modsym, 2);
  g = gcdext0(gel(alpha, 1), gel(alpha, 2));
  T = mkmat2(gdiv(alpha, gel(g, 3)),
	     mkcol2(gneg(gel(g, 2)), gel(g, 1)));
  beta = gdiv(beta, ggcd(gel(beta, 1), gel(beta, 2)));
  x = inverseimage(T, beta);
  result = zerocol(dim);

  /* Check for the case where the path is trivial.  */
  if(gequal0(gel(x, 2)))
    return result;

  /* Do the continued fraction trick.  */
  cf = contfrac0(gdiv(gel(x, 1), gel(x, 2)), NULL, 0);
  l = lg(cf) - 1;
  C = gmul(T, contfracpnqn(cf, l));
  for(i = 0; i < l; i++) {
    s1 = itos(gcoeff(C, 2, i + 1));
    s2 = itos(gcoeff(C, 2, i + 2));
    switch(i % 4) {
    case 0:
      s = mkvecsmall2(s1, s2); break;
    case 1:
      s = mkvecsmall2(s1, -s2); break;
    case 2:
      s = mkvecsmall2(-s1, -s2); break;
    case 3:
      s = mkvecsmall2(-s1, s2); break;
    }
    index = manin_symbol_index(group, 2, s);
    result = gadd(result, gel(sym_to_basis, index));
  }
  return gerepileupto(av, result);
}

struct entry {
  GEN m;
  struct entry *next;
};

static void
list_push(struct entry **list, GEN x) {
  struct entry *new = (struct entry *) pari_malloc(sizeof(struct entry));
  new->m = x;
  new->next = *list;
  *list = new;
}

static GEN
list_pop(struct entry **list) {
  GEN result = (*list)->m;
  struct entry *next = (*list)->next;
  pari_free(*list);
  *list = next;
  return result;
}

GEN
heilbronn_cremona(long p) {
  long q, r, x1, x2, y1, y2, x3, y3, a, b, c;
  struct entry *list = NULL;
  long i, count = 1;
  GEN vec;
  pari_sp ltop = avma;

  list_push(&list, make_2x2_matrix(1, 0, 0, p));
  for(r = 0; r < p; r++) {
    x1 = p; x2 = -r;
    y1 = 0; y2 = 1;
    a = -p; b = r;
    count++;
    list_push(&list, make_2x2_matrix(x1, x2, y1, y2));
    while(b != 0) {
      q = itos(ground(mkfrac(stoi(a), stoi(b))));
      c = a - b * q; a = -b; b = c;
      x3 = q * x2 - x1; x1 = x2; x2 = x3;
      y3 = q * y2 - y1; y1 = y2; y2 = y3;
      count++;
      list_push(&list, make_2x2_matrix(x1, x2, y1, y2));
    }
  }
  vec = cgetg(count + 1, t_VEC);
  i = 0;
  while(list != NULL)
    gel(vec, ++i) = list_pop(&list);
  return gerepilecopy(ltop, vec);
}

static GEN
modular_symbols_heilbronn_on_manin_symbol(GEN M, GEN sym, GEN h) {
  /* h = Vecsmall([a, b, c, d]) */
  long a, b, c, d;
  /* weight 2: sym = Vecsmall([e, f])
     weight > 2: sym = Vecsmall([j, e, f]) */
  long e, f, j;
  long l, m;
  GEN group = modular_symbols_group(M);
  long level = modular_group_level(group);
  long weight = modular_symbols_weight(M);
  long dim = modular_symbols_dimension(M);
  GEN sym_to_basis = modular_symbols_manin_symbols_to_basis(M);
  GEN t;
  GEN result = zerocol(dim);
  long index;
  /* table of binomial coefficients, kept on the heap */
  static GEN binom = NULL;
  static long binom_max = -1;

  if(typ(h) != t_VECSMALL
     || lg(h) != 5)
    pari_err(e_MISC, "wrong type for third argument");
  a = h[1];
  b = h[2];
  c = h[3];
  d = h[4];

  if(weight == 2) {
    if(typ(sym) != t_VECSMALL
	|| lg(sym) != 3)
      pari_err(e_MISC, "wrong type for second argument");
    e = sym[1];
    f = sym[2];
    t = mkvecsmall2(a * e + c * f, b * e + d * f);
    if(cgcd(cgcd(t[1], t[2]), level) != 1) {
      return zerocol(dim);
    }
    index = manin_symbol_index(group, 2, t);
    return gel(sym_to_basis, index);
  }

  /* weight > 2 */
  if(typ(sym) != t_VECSMALL
     || lg(sym) != 4)
    pari_err(e_MISC, "wrong type for second argument");
  j = sym[1];
  e = sym[2];
  f = sym[3];
  t = mkvecsmall3(0, a * e + c * f, b * e + d * f);
  if(cgcd(cgcd(t[2], t[3]), level) != 1) {
    return zerocol(dim);
  }
  index = manin_symbol_index(group, weight, t);

  /* compute more binomial coefficients if necessary */
  if(weight - 2 > binom_max) {
    binom_max = weight - 2;
    if(binom)
      gunclone(binom);
    binom = gclone(matpascal(binom_max));
  }

  result = zerocol(dim);
  for(l = 0; l <= j; l++) {
    GEN abpow = gmul(gpowgs(stoi(a), l),
		     gpowgs(stoi(b), j - l));
    for(m = 0; m <= weight - 2 - j; m++) {
      GEN cdpow = gmul(gpowgs(stoi(c), m),
		       gpowgs(stoi(d), weight - 2 - j - m));
      result = gadd(result,
		    gmul(gmul(gmul(gmul(gcoeff(binom, j + 1, l + 1),
					gcoeff(binom, weight - 1 - j, m + 1)),
				   abpow),
			      cdpow),
			 gel(sym_to_basis, index + l + m)));
    }
  }
  return result;
}

/*
  modular_symbols_hecke_operator_prime

  Return the matrix of the Hecke operator T_p (with p prime)
  with respect to the basis of the space M of modular symbols.
*/
static GEN
modular_symbols_hecke_operator_prime(GEN M, long p)
{
  GEN syms = modular_symbols_manin_symbols(M);
  GEN basis = modular_symbols_basis(M);
  long dim = modular_symbols_dimension(M);
  GEN heilbronn;
  GEN T, T1;
  long j, k;
  pari_sp ltop = avma;
  pari_sp ltop2, stack_limit;

  if(typ(basis) != t_VECSMALL) {
    pari_err(e_MISC, "wrong type");
    return NULL;
  }
  heilbronn = heilbronn_cremona(p);
  T1 = cgetg(dim + 1, t_MAT);
  ltop2 = avma;
  stack_limit = stack_lim(ltop2, 1);
  T = zeromat(dim, dim);
  for(k = 1; k <= lg(heilbronn) - 1; k++) {
    GEN h = gel(heilbronn, k);
    for(j = 1; j <= dim; j++) {
      GEN s = gel(syms, basis[j]);
      gel(T1, j) = modular_symbols_heilbronn_on_manin_symbol(M, s, h);
    }
    T = gadd(T, T1);
    if(avma < stack_limit) {
      if(DEBUGMEM > 1)
	pari_warn(warnmem, "modular_symbols_hecke_operator_prime (p = %li, k = %li/%li)",
		  p, k, lg(heilbronn) - 1);
      T = gerepileupto(ltop2, T);
    }
  }
  return gerepileupto(ltop, T);
}

/*
  Return the matrix of the Hecke operator T_n (with n arbitrary)
  with respect to the distinguished basis of the space M of modular symbols.
*/
GEN
modular_symbols_hecke_operator(GEN M, long n) {
  pari_sp av = avma;
  long level = modular_group_level(modular_symbols_group(M));
  long weight = modular_symbols_weight(M);
  long dim = modular_symbols_dimension(M);
  GEN f = factoru(n);
  GEN T = matid(dim), T_p, diam_p, prev, cur, new;
  long i, j, p, e;

  for(i = 1; i < lg(gel(f, 1)); i++) {
    p = coeff(f, i, 1);
    e = coeff(f, i, 2);
    /* Compute  T_{p^e}.  */
    T_p = modular_symbols_hecke_operator_prime(M, p);
    if(e == 1 || level % p == 0) {
      T = gmul(T, gpowgs(T_p, e));
    } else {
      /* e > 1  and  p  does not divide the level.  */
      diam_p = modular_symbols_diamond_operator(M, p);
      prev = matid(dim);
      cur = T_p;
      for(j = 1; j < e; j++) {
	new = gsub(gmul(T_p, cur),
		   gmul(powuu(p, weight - 1), gmul(diam_p, prev)));
	prev = cur;
	cur = new;
      }
      T = gmul(T, cur);
    }
  }
  return gerepileupto(av, T);
}

/*
  Return the matrix of the star operator with respect to
  the distinguished basis of the space  M  of modular symbols.
*/
GEN
modular_symbols_star_operator(GEN M) {
  pari_sp av = avma;
  GEN group = modular_symbols_group(M);
  long weight = modular_symbols_weight(M);
  GEN syms = modular_symbols_manin_symbols(M);
  GEN basis = modular_symbols_basis(M);
  GEN sym_to_basis = modular_symbols_manin_symbols_to_basis(M);
  long dim = lg(basis) - 1;
  GEN T = cgetg(dim + 1, t_MAT), s, t;
  long i, j, index;

  for(i = 1; i <= dim; i++) {
    s = gel(syms, basis[i]);
    j = (weight == 2) ? 0 : s[1];
    t = (weight == 2) ? mkvecsmall2(-s[1], s[2])
                      : mkvecsmall3(j, -s[2], s[3]);
    index = manin_symbol_index(group, weight, t);
    gel(T, i) = gmulsg(((j % 2 == 0) ? 1 : -1),
		       gel(sym_to_basis, index));
  }
  return gerepilecopy(av, T);
}

/*
  Return the matrix of the Atkin-Lehner operator  w_d  with respect to
  the distinguished basis of the space  M  of modular symbols.
*/
GEN
modular_symbols_atkin_lehner_operator(GEN M, long d) {
  pari_sp av = avma;
  GEN group = modular_symbols_group(M);
  long n = modular_group_level(group);
  GEN syms = modular_symbols_manin_symbols(M);
  GEN basis = modular_symbols_basis(M);
  long p, q, dim = lg(basis) - 1;
  GEN T = cgetg(dim + 1, t_MAT), s, g;
  long i;

  if(n % d != 0 || cbezout(d, n/d, &p, &q) != 1)
    pari_err(e_MISC, "modular_symbols_atkin_lehner_operator: d must satisfy d | n and gcd(d, n/d) = 1");
  g = make_2x2_matrix(d, -q, n, p*d);
  for(i = 1; i <= dim; i++) {
    s = gel(syms, basis[i]);
    gel(T, i) = modular_symbols_matrix_on_manin_symbol(M, s, g);
  }
  return gerepileupto(av, T);
}

/*
  Return the matrix of the restriction of the operator  T
  to the cuspidal subspace of  M.
*/
static GEN
cuspidal_part(GEN M, GEN T) {
  GEN S = modular_symbols_cuspidal_subspace(M);
  return inverseimage(S, gmul(T, S));
}

GEN
modular_symbols_diamond_operator_cuspidal_part(GEN M, long d) {
  return cuspidal_part(M, modular_symbols_diamond_operator(M, d));
}

GEN
modular_symbols_hecke_operator_cuspidal_part(GEN M, long n) {
  return cuspidal_part(M, modular_symbols_hecke_operator(M, n));
}

GEN
modular_symbols_star_operator_cuspidal_part(GEN M) {
  return cuspidal_part(M, modular_symbols_star_operator(M));
}

GEN
modular_symbols_atkin_lehner_operator_cuspidal_part(GEN M, long d) {
  return cuspidal_part(M, modular_symbols_atkin_lehner_operator(M, d));
}


/*
  Return the cuspidal plus subspace of  M,  i.e. the subspace
  on which the star operator acts as the identity.
*/
GEN
modular_symbols_cuspidal_plus_subspace(GEN M) {
  GEN S = modular_symbols_cuspidal_subspace(M);
  return gmul(S, ker(gsub(modular_symbols_star_operator_cuspidal_part(M),
			  matid(lg(S) - 1))));

}
