#include <pari/pari.h>
#include <pari/paripriv.h>  /* t_FF_Flxq */

#include "finite-fields.h"

/*
  Let  F  be a finite field of  p^m  elements, given as F_p[X]/(T)
  with  T  a polynomial of degree  m.  (If  T == NULL,  we define
  deg T = 1.)  Given such a representation, we endow  F  with the
  F_p-basis  (1, x, x^2, ..., x^{m-1}),  where  x = (X mod T).
  Elements of  F  are represented as polynomials in  X  which are
  reduced modulo  T,  or equivalently as vectors on the above basis.

  An extension of finite fields  F'/F  of degree  n  is specified
  as a pair  [p, T, U, r, M],  where U is a polynomial in F_p[X] of
  degree  mn,  r  is a root in  F'  of the polynomial  T  defining
  F,  and  M  is the matrix of the unique  F_p-algebra homomorphism
  F -> F'  gotten by sending  x  to  r.  (If  T == NULL,  there is
  no  x,  but there is only one possible  M,  namely the vector
  [1, 0, 0, ..., 0]~  of length  n.)  The matrix  M  has as its
  columns the coefficient vectors of successive powers of  r.
*/

GEN
Flxq_to_FF(GEN x, ulong p, GEN T) {
  GEN e = cgetg(5, t_FFELT);
  e[1] = t_FF_Flxq;
  gel(e, 2) = vecsmall_copy(x);
  gel(e, 3) = vecsmall_copy(T);
  gel(e, 4) = utoipos(p);
  return e;
}

/*
  Return a lexicographically minimal irreducible polynomial
  of degree  n  over  the field of  p  elements.
*/
GEN
init_Flxq (unsigned long p, long n, long v) {
  pari_sp av = avma;
#if 1
  /*
    TODO: this approach sometimes takes a very long time (for example
    up to 20 minutes with p = 383 or p = 457, and n = 168).
  */
  GEN g = gp_read_str("(p,n)->forvec(a=[[0,p-1]|i<-[1..n]],"
		      "f=Mod(x^n-Pol(a),p);"
		      "if(polisirreducible(f),return(f)))");
  GEN f = liftint(closure_callgen2(g, stoi(p), stoi(n)));
#else
  GEN f = init_Fq(stoi(p), n, v);
#endif
  pari_printf("using irreducible polynomial %Ps\n", f);
  return gerepileupto(av, ZX_to_Flx(f, p));
}

GEN
Flxq_to_vecsmall (GEN x, unsigned long p, GEN T) {
  if (typ (x) != t_VECSMALL)
    pari_err (e_TYPE, "Flxq_to_vecsmall");
  if (T == NULL) {
    /* TODO: does it ever happen that we represent elements
       by polynomials while  T = NULL?  */
    switch (degpol (x)) {
    case -1:
      return mkvecsmall (0);
    case 0:
      return mkvecsmall (x[2]);
    default:
      pari_err (e_MISC, "incompatible lengths");
      return NULL;
    }
  } else {
    int i, n = degpol (T);
    GEN v = cgetg (n + 1, t_VECSMALL);
    if (degpol (x) >= n)
      pari_err(e_MISC, "degree of %Ps too large (should be at most %li)\n",
	       x, n - 1);
    for (i = 1; i < lg(x) - 1; i++)
      v[i] = x[i + 1];
    for ( ; i <= n; i++)
      v[i] = 0;
    return v;
  }
}

GEN
vecsmall_to_Flxq (GEN v, unsigned long p, GEN T, long sv) {
  if (typ (v) != t_VECSMALL)
    pari_err (e_TYPE, "vecsmall_to_Flxq");
  if (T == NULL) {
    if (lg (v) != 2)
      pari_err (e_MISC, "incompatible lengths");
    return Fl_to_Flx (v[1], sv);
  } else {
    int n = lg (v) - 1, i;
    GEN x;

    /* T is of degree >= 1 or the zero polynomial.  */
    if (n != degpol (T))
      pari_err (e_MISC, "incompatible lengths");
    while (n > 0 && v[n] % p == 0)
      n--;
    x = cgetg (n + 2, t_VECSMALL);
    x[1] = sv;
    for (i = 1; i <= n; i++)
      x[i + 1] = v[i] % p;
    return x;
  }
}

/*
  Given a finite field  F = F_p[X]/(T)  and a positive integer  n,
  return an extension  F'/F  of degree  n.
*/
GEN
finite_field_extension (long n, unsigned long p, GEN T) {
  GEN result = cgetg (6, t_VEC);

  if (T == NULL) {
    gel (result, 1) = stoi (p);
    gel (result, 2) = polx_Flx (evalvarn (MAXVARN));
    gel (result, 3) = init_Flxq (p, n, MAXVARN);
    gel (result, 4) = zero_Flx (evalvarn (MAXVARN));
    gel (result, 5) = cgetg (2, t_MAT);
    gmael (result, 5, 1) = const_vecsmall (n, 0);
    mael3 (result, 5, 1, 1) = 1;
    return result;
  } else {
    long m = degpol (T), i;
    long v = varn (T);
    pari_sp av = avma;
    GEN p1 = stoi (p), U, r, M;

    U = init_Flxq (p, m * n, v);
    r = Flx_ffisom(T, U, p);
    /* fill out the linear map as explained above */
    M = cgetg (m + 1, t_MAT);
    for (i = 1; i <= m; i++)
      gel(M, i) = Flxq_to_vecsmall(Flxq_pow(r, stoi(i - 1), U, p), p, U);
    gel (result, 1) = p1;
    gel (result, 2) = T;
    gel (result, 3) = U;
    gel (result, 4) = r;
    gel (result, 5) = M;
    return gerepilecopy(av, result);
  }
}

GEN
finite_field_vector_up (GEN x, GEN extension) {
  unsigned long p = itos (gel (extension, 1));
  GEN T = gel (extension, 2);
  GEN U = gel (extension, 3);
  GEN M = gel (extension, 5);
  long v = varn (U);
  GEN result;
  long i, n;
  pari_sp av;

  switch (typ (x)) {
  case t_VECSMALL:
    result = cgetg (lg (x), t_COL);
    for (i = 1; i < lg (x); i++)
      gel (result, i) = Fl_to_Flx (x[i], evalvarn (v));
    return result;
  case t_VEC:
  case t_COL:
    av = avma;
    result = cgetg_copy(x, &n);
    for (i = 1; i < n; i++) {
      GEN vec_x_i = Flxq_to_vecsmall (gel (x, i), p, T);
      /* Apply the inclusion map to x[i].  */
      vec_x_i = Flm_Flc_mul (M, vec_x_i, p);
      gel (result, i) = vecsmall_to_Flxq (vec_x_i, p, U, evalvarn (v));
    }
    return gerepilecopy (av, result);
  default:
    pari_err (e_TYPE, "finite_field_vector_up");
    return NULL;
  }
}

GEN
finite_field_vector_down (GEN x, GEN extension) {
  unsigned long p = itos (gel (extension, 1));
  GEN T = gel (extension, 2);
  GEN U = gel (extension, 3);
  GEN M = gel (extension, 5);
  long v = varn (T);
  GEN result;
  long i, n;
  pari_sp av;

  switch (typ (x)) {
  case t_VECSMALL:
    return x;
  case t_VEC:
  case t_COL:
    if (degpol (T) < 1)
      pari_err (e_MISC, "bad polynomial for base field");
    av = avma;
    result = cgetg_copy(x, &n);
    for (i = 1; i < n; i++) {
      GEN vec_x_i = Flxq_to_vecsmall (gel (x, i), p, U);
      /* Try to find the inverse of  x[i]  under the inclusion map.  */
      vec_x_i = Flm_invimage(M, mkmat(vec_x_i), p);
      if (vec_x_i == NULL)
	pari_err(e_MISC, "element %Ps not in the base field", gel(x, i));
      gel(result, i) = vecsmall_to_Flxq(gel(vec_x_i, 1), p, T, evalvarn(v));
    }
    return gerepilecopy (av, result);
  default:
    pari_err_TYPE("finite_field_vector_down", x);
    return NULL;
  }
}

GEN
finite_field_vector_to_prime_field (GEN x, unsigned long p) {
  GEN result;
  int i;
  
  if (typ (x) != t_VEC && typ (x) != t_COL)
    pari_err (e_TYPE, "finite_field_vector_to_prime_field");
  result = cgetg (lg (x), t_VECSMALL);
  for (i = 1; i < lg (x); i++)
    switch (degpol (gel (x, i))) {
    case -1:
      result[i] = 0;
      break;
    case 0:
      result[i] = gel (x, i)[2];
      break;
    default:
      pari_err(e_MISC, "element %Ps not in the prime field", gel(x, i));
    }
  return result;
}

GEN
finite_field_matrix_up (GEN x, GEN extension) {
  GEN y = cgetg (lg (x), t_MAT);
  int i;
  for (i = 1; i < lg (x); i++)
    gel (y, i) = finite_field_vector_up (gel (x, i), extension);
  return y;
}

GEN
finite_field_matrix_down (GEN x, GEN extension) {
  GEN y = cgetg (lg (x), t_MAT);
  int i;
  for (i = 1; i < lg (x); i++)
    gel (y, i) = finite_field_vector_down (gel (x, i), extension);
  return y;
}

GEN
finite_field_matrix_to_prime_field (GEN x, unsigned long p) {
  GEN y = cgetg (lg (x), t_MAT);
  int i;
  for (i = 1; i < lg (x); i++)
    gel (y, i) = finite_field_vector_to_prime_field (gel (x, i), p);
  return y;
}
