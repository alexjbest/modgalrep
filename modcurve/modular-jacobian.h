#ifndef MODULAR_JACOBIAN
#define MODULAR_JACOBIAN

GEN modular_curve_jacobian_medium(GEN group, unsigned long p);
GEN modular_curve_jacobian_large(GEN group, unsigned long p);
GEN modular_jacobian_normalised_divisor (GEN X, GEN W_D, GEN W_rO);

#endif
