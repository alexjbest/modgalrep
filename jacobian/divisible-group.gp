\r jacobian

/*
  Return some information related to the l-divisible group  G
  in the  l^\infty-torsion of the Jacobian  J.
*/
jacobian_G(J, l) = {
  local (chi, v, dim, f1, g1, fg, f, g, a, chi_a,
	 m, n, n2, projector, kummer_polynomial);
  if (l == curve_base_field_characteristic (J),
      error ("l must be different from the characteristic of the base field"));
  chi = polrecip (curve_numerator_zeta_function (J));
  v = variable (chi);
  /* Compute the factorisation  chi = f * g  modulo  l.  */
  g1 = chi * Mod (1, l);
  dim = 0;
  while (subst (g1, v, 1) == 0,
	 dim++;
	 g1 /= (v - 1));
  f1 = (v - 1)^dim * Mod (1, l);
  /*
    Compute the order of (x mod f) in (\F_l/(f))^\times.
    This is the least power of  l  which is at least  deg f;
    it is also the degree of the extension  k_a  that we need
    to define the Kummer map.
  */
  a = 1;
  while (a < dim,
	 a *= l);
  /* Compute the characteristic polynomial of the Frobenius
     endomorphism of  J  over  k_a.  */
  chi_a = frobenius_polynomial_base_change (chi, a);
  /* Factor  #J(k_a) = l^n m  with  m  coprime to  l.  */ 
  m = subst (chi_a, variable (chi_a), 1);
  n = 0;
  while (m % l == 0,
	 m /= l;
	 n++);
  n2 = max (n, 2);
  /* Lift the factorisation  chi = f * g.  */
  fg = polhensellift (chi, lift ([f1, g1]), l, n2);
  f = fg[1] + O(l^n2);
  g = fg[2] + O(l^n2);
  /*
    Compute a projector onto  G  as a polynomial in the
    Frobenius endomorphism of  k.
  */
  projector = (g * bezout (f, g)[2]) % chi * Mod (1, l^n);
  /*
    Compute the Kummer map on  G  as a polynomial in the
    Frobenius endomorphism of  k.
  */
  kummer_polynomial = ((v^a - 1) % f) / l * Mod (1, l);
  return ([dim, a, n, m, projector, kummer_polynomial]);
}
