/***

    Cusp forms

***/

#include <pari/pari.h>
#include "modular-groups.h"
#include "modular-symbols.h"
#include "cusp-forms.h"
#include "linear-algebra.h"

GEN
cusp_forms(GEN group, long weight, GEN p) {
  GEN S = cgetg(3, t_VEC);
  GEN M = modular_symbols(group, weight, p ? p : gen_0);
  gel(S, 1) = M;
  gel(S, 2) = modular_symbols_cuspidal_plus_subspace(M);
  return S;
}

static GEN
cusp_forms_operator(GEN S, GEN T) {
  GEN map = cusp_forms_to_modular_symbols(S);
  return inverseimage(map, gmul(T, map));
}

/*
  Return the matrix of the diamond operator  <d>
  on the space  S  of cusp forms.
*/
GEN
cusp_forms_diamond_operator(GEN S, long d) {
  GEN M = cusp_forms_modular_symbols(S);
  return cusp_forms_operator(S, modular_symbols_diamond_operator(M, d));
}

/*
  Return the matrix of the Hecke operator  T_n
  on the space  S  of cusp forms.
*/
GEN
cusp_forms_hecke_operator(GEN S, long n) {
  pari_sp av = avma;
  long level = modular_group_level(cusp_forms_group(S));
  long weight = cusp_forms_weight(S);
  long dim = cusp_forms_dimension(S);
  GEN M = cusp_forms_modular_symbols(S);
  GEN f = factoru(n);
  GEN T = matid(dim), T_p, diam_p, prev, cur, new;
  long i, j, p, e;

  for(i = 1; i < lg(gel(f, 1)); i++) {
    p = coeff(f, i, 1);
    e = coeff(f, i, 2);
    /* Compute  T_{p^e}.  */
    T_p = cusp_forms_operator(S, modular_symbols_hecke_operator(M, p));
    if(e == 1 || level % p == 0) {
      T = gmul(T, gpowgs(T_p, e));
    } else {
      /* e > 1  and  p  does not divide the level.  */
      diam_p = cusp_forms_diamond_operator(S, p);
      prev = matid(dim);
      cur = T_p;
      for(j = 1; j < e; j++) {
	new = gsub(gmul(T_p, cur),
		   gmul(powuu(p, weight - 1), gmul(diam_p, prev)));
	prev = cur;
	cur = new;
      }
      T = gmul(T, cur);
    }
  }
  return gerepileupto(av, T);
}


/*
  Return the matrix of the Atkin-Lehner operator  w_d
  on the space  S  of cusp forms.
*/
GEN
cusp_forms_atkin_lehner_operator(GEN S, long d) {
  GEN M = cusp_forms_modular_symbols(S);
  return cusp_forms_operator(S, modular_symbols_atkin_lehner_operator(M, d));
}


/*
  Compute the Hecke algebra (diamond operators and the first
  few operators  T_p)  on the space  S  of cusp forms.

  Return  [[<0>, <1>, ..., <n - 1>], [T_1, T_2, ..., T_bound]
  with  n  the level.  The algorithm increases the given bound
  if necessary.
*/
GEN
cusp_forms_hecke_algebra(GEN S, long bound) {
  pari_sp av = avma;
  GEN group = cusp_forms_group(S);
  long level = modular_group_level(group);
  long weight = cusp_forms_weight(S);
  long dim = cusp_forms_dimension(S);
  GEN gen = znstar(stoi(level));
  long ngen = lg(gel(gen, 2)) - 1;
  long bound_min;
  GEN exponents, T, diam, d, v, a, g_i, factors;
  long b, i, p, e, v_i;
  forvec_t E;

  bound_min = itos(gceil(modular_group_cusp_forms_degree(group, weight))) + 1;
  if(bound < bound_min)
    bound = bound_min;

  /* Compute diamond operators on generators of (Z/nZ)^\times.  */
  pari_printf("cusp_forms_hecke_algebra: computing diamond operators:");
  diam = cgetg(level + 1, t_VEC);
  for(i = 1; i <= level; i++)
    gel(diam, i) = zeromat(dim, dim);
  for(i = 1; i <= ngen; i++) {
    b = itos(liftint(gmael(gen, 3, i)));
    pari_printf(" %li", b);
    gel(diam, b + 1) = cusp_forms_diamond_operator(S, b);
  }
  /* Compute all diamond operators.  */
  exponents = cgetg(ngen + 1, t_VEC);
  for(i = 1; i <= ngen; i++)
    gel(exponents, i) = mkvec2(gen_0, gsubgs(gmael(gen, 2, i), 1));
  forvec_init(&E, exponents, 0);
  while((v = forvec_next(&E)) != NULL) {
    d = gmodulss(1, level);
    a = matid(dim);
    for(i = 1; i <= ngen; i++) {
      g_i = gmael(gen, 3, i);
      v_i = itos(gel(v, i));
      d = gmul(d, gpowgs(g_i, v_i));
      a = gmul(a, gpowgs(gel(diam, itos(liftint(g_i)) + 1), v_i));
    }
    gel(diam, itos(liftint(d)) + 1) = a;
  }

  /* Compute the Hecke operators recursively.  */
  pari_printf("\ncusp_forms_hecke_algebra: computing operators T_n up to n = %li:",
	      bound);
  T = cgetg(bound + 1, t_VEC);
  if(bound > 0)
    gel(T, 1) = matid(dim);
  for(i = 2; i <= bound; i++) {
    if(uisprime(i)) {
      pari_printf(" %li", i);
      pari_flush();
      gel(T, i) = cusp_forms_hecke_operator(S, i);
    } else {
      /* i  is composite */
      factors = factoru(i);
      p = coeff(factors, 1, 1);
      e = coeff(factors, 1, 2);
      if(lg(gel(factors, 1)) == 2) {
	/* i  is a prime power */
	gel(T, i) = gmul(gel(T, p), gel(T, upowuu(p, e - 1)));
	if(level % p != 0) {  
	  gel(T, i) = gsub(gel(T, i),
			   gmulsg(upowuu(p, weight - 1),
				  gmul(gel(diam, p % level + 1), gel(T, upowuu(p, e - 2)))));
	}
      } else {
	/* i  is not a prime power */
	gel(T, i) = gmul(gel(T, upowuu(p, e)), gel(T, i / upowuu(p, e)));
      }
    }
  }
  pari_printf("\n");
  return gerepilecopy(av, mkvec2(diam, T));
}


/*
  Return a basis for the q-expansions of cusp forms in  S.

  Note: A cusp form vanishes if its first  D + 1  coefficients
  a_1, a_2, ..., a_{D+1}  are zero, where  D  is the degree of
  the line bundle of cusp forms.
*/
GEN
cusp_forms_q_expansion_basis(GEN S, long nterms) {
  GEN group = cusp_forms_group(S);
  long weight = cusp_forms_weight(S);
  long dim = cusp_forms_dimension(S);
  long bound, i, j, k;
  GEN alg, M;
  pari_sp av = avma;

  bound = itos(gceil(modular_group_cusp_forms_degree(group, weight))) + 1;
  if (nterms < bound)
    nterms = bound;
  alg = cusp_forms_hecke_algebra(S, nterms);
  /* Compute the matrix of the q-expansion homomorphism.  */
  M = cgetg(dim*dim + 1, t_MAT);
  for(i = 1; i <= dim; i++) {
    for(j = 1; j <= dim; j++) {
      gel(M, dim*(i - 1) + j) = cgetg(nterms + 1, t_COL);
      for(k = 1; k <= nterms; k++)
	gcoeff(M, k, dim * (i - 1) + j) = gcoeff(gmael(alg, 2, k), i, j);
    }
  }
  return gerepileupto(av, image(M));
}


/*
  Given the Hecke algebra acting on some space of cusp forms,
  compute the matrix of the operator  A  on the q-expansion basis.
*/
static GEN
hecke_operator_on_q_expansion_basis(GEN alg, GEN A, GEN B, GEN L) {
  long dim, nterms, rows, i, j, k, l;
  GEN ATj, Q, T;
  pari_sp ltop = avma, stack_limit = stack_lim(ltop, 1);

  T = gel(alg, 2);
  dim = lg(gel(T, 1)) - 1;
  if(dim == 0)
    return cgetg(1, t_MAT);
  nterms = lg(T) - 1;
  rows = (lg(gel(B, 1)) - 1) / dim;
  Q = zeromatcopy(nterms, dim);
  for(j = 1; j <= nterms; j++) {
    ATj = gmul(A, gel(T, j));
    for(i = 1; i <= dim; i++)
      for(l = 1; l <= rows; l++)
	for(k = 1; k <= dim; k++)
	  gcoeff(Q, j, i) = gadd(gcoeff(Q, j, i),
				 gmul(gcoeff(B,(l - 1) * dim + k, i),
				      gcoeff(ATj, l, k)));
    if(avma < stack_limit) {
      if(DEBUGMEM > 1)
	pari_warn(warnmem, "hecke_operator_on_q_expansion_basis");
      Q = gerepilecopy(ltop, Q);
    }
  }
  return gerepileupto(ltop, inverseimage(L, Q));
}


/*
  Given a modular group, a weight and the Hecke algebra acting on the
  associated space of cusp forms (as matrices with respect to some
  basis), return a basis of q-expansions of cusp forms and the Hecke
  algebra with respect to this basis.
*/
GEN
hecke_algebra_q_expansion_basis(GEN group, long weight, GEN alg) {
  pari_sp av = avma;
  long level = modular_group_level(group);
  GEN gen = znstar(stoi(level));
  long ngen = lg(gel(gen, 2)) - 1;
  GEN exponents;
  long nterms = lg(gel(alg, 2)) - 1;
  long dim = lg(gmael(alg, 2, 1)) - 1;
  GEN B = cgetg(1, t_MAT);
  GEN L = cgetg(1, t_MAT);
  long rows, cols, b, i, j, k, v_i, p, e;
  GEN Bnew, M, d, v, result, diam, T, a, g_i, factors;
  forvec_t E;

  pari_printf("hecke_algebra_q_expansion_basis: computing the q-expansion basis\n");
  for(i = 1; i <= dim; i++) {
    /*
      Add  dim  forms at a time (corresponding to one row of our
      dim x dim  Hecke matrices).
    */
    rows = lg(B) == 1 ? 0 : lg(gel(B, 1)) - 1;
    cols = lg(B) - 1;
    /* Place a dim x dim identity matrix to the lower right of B.  */
    Bnew = zeromatcopy(rows + dim, cols + dim);
    for(j = 1; j <= rows; j++) {
      for(k = 1; k <= cols; k++)
	gcoeff(Bnew, j, k) = gcoeff(B, j, k);
    }
    for(j = 1; j <= dim; j++)
      gcoeff(Bnew, rows + j, cols + j) = gen_1;
    B = Bnew;
    /* M = matrix (nterms, dim, j, k, alg[2][j][i, k]) */
    M = cgetg(dim + 1, t_MAT);
    for(k = 1; k <= dim; k++) {
      gel(M, k) = cgetg(nterms + 1, t_COL);
      for(j = 1; j <= nterms; j++)
	gcoeff(M, j, k) = gcoeff(gmael(alg, 2, j), i, k);
    }
    L = shallowconcat(L, M);
    result = reduced_column_echelon_form (L, B, 0);
    L = gel(result, 2);
    B = gel(result, 3);
    if(itos(gel(result, 1)) == dim)
      break;
  }
  fixlg(L, dim + 1);
  fixlg(B, dim + 1);

  pari_printf("hecke_algebra_q_expansion_basis: expressing diamond operators on the q-expansion basis:");
  diam = cgetg(level + 1, t_VEC);
  for(i = 1; i <= level; i++)
    gel(diam, i) = zeromat(dim, dim);
  for(i = 1; i <= ngen; i++) {
    b = itos(liftint(gmael(gen, 3, i)));
    pari_printf(" %li", b);
    gel(diam, b + 1) = hecke_operator_on_q_expansion_basis(alg, gmael(alg, 1, b + 1), B, L);
  }
  /* Compute all diamond operators.  */
  exponents = cgetg(ngen + 1, t_VEC);
  for(i = 1; i <= ngen; i++)
    gel(exponents, i) = mkvec2(gen_0, gsubgs(gmael(gen, 2, i), 1));
  forvec_init(&E, exponents, 0);
  while((v = forvec_next(&E)) != NULL) {
    d = gen_1;
    a = matid(dim);
    for(i = 1; i <= ngen; i++) {
      g_i = gmael(gen, 3, i);
      v_i = itos(gel(v, i));
      d = gmul(d, gpowgs(g_i, v_i));
      a = gmul(a, gpowgs(gel(diam, itos(liftint(g_i)) + 1), v_i));
    }
    gel(diam, itos(liftint(d)) + 1) = a;
  }

  /* Compute the Hecke operators recursively.  */
  pari_printf("\nhecke_algebra_q_expansion_basis: expressing operators T_n up to n = %li on the q-expansion basis:",
	      nterms);
  T = cgetg(nterms + 1, t_VEC);
  if(nterms > 0)
    gel(T, 1) = matid(dim);
  for(i = 2; i <= nterms; i++) {
    if(uisprime(i)) {
      pari_printf(" %li", i);
      pari_flush();
      gel(T, i) = hecke_operator_on_q_expansion_basis(alg, gmael(alg, 2, i), B, L);
    } else {
      /* i  is composite */
      factors = factoru(i);
      p = coeff(factors, 1, 1);
      e = coeff(factors, 1, 2);
      if(lg(gel(factors, 1)) == 2) {
	/* i  is a prime power */
	gel(T, i) = gmul(gel(T, p), gel(T, upowuu(p, e - 1)));
	if(level % p != 0) {  
	  gel(T, i) = gsub(gel(T, i),
			   gmulsg(upowuu(p, weight - 1),
				  gmul(gel(diam, p % level + 1), gel(T, upowuu(p, e - 2)))));
	}
      } else {
	/* i  is not a prime power */
	gel(T, i) = gmul(gel(T, upowuu(p, e)), gel(T, i / upowuu(p, e)));
      }
    }
  }
  pari_printf("\n");
  return gerepilecopy(av, mkvec3(L, diam, T));
}

GEN
cusp_forms_as_hecke_module(GEN group, long weight, GEN p, long nterms) {
  long bound;
  pari_sp av = avma;
  GEN S, T;

  bound = itos(gceil(modular_group_cusp_forms_degree(group, weight))) + 1;
  if(nterms < bound)
    nterms = bound;
  S = cusp_forms(group, weight, p);
  T = cusp_forms_hecke_algebra(S, nterms);
  return gerepileupto(av, hecke_algebra_q_expansion_basis(group, weight, T));
}
