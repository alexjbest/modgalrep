GEN algsmall_matrix (GEN A, GEN x, unsigned long p, GEN T);
GEN algsmall_mul (GEN A, GEN x, GEN y, unsigned long p, GEN T);
GEN algsmall_local_maximal_ideal (GEN A, unsigned long p, GEN T);
GEN algsmall_primary_decomposition (GEN A, unsigned long p, GEN T);
GEN algsmall_maximal_ideals (GEN A, unsigned long p, GEN T);
GEN symmetric_bilinear_map_to_algsmall (GEN mu, unsigned long p, GEN T);
GEN bilinear_map_module_generator (GEN mu, unsigned long p, GEN T);
GEN bilinear_map_to_algsmall (GEN mu, unsigned long p, GEN T);
