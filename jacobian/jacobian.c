#include <math.h>  /* log */
#include <pari/pari.h>

#include "algsmall.h"
#include "finite-fields.h"
#include "matsmall.h"

#include "addflip-chain.h"
#include "curve.h"
#include "divisor.h"
#include "jacobian.h"


/*** Jacobians ***/

/*
  Khuri-Makdisi's `medium model' of the Jacobian: to compute with the
  Jacobian of a curve X, we use the line bundle
    {\cal L} = {\cal L}_0^{\otimes 2},
  where {\cal L}_0 is the line bundle attached to X.  We need to know
  \Gamma(X,{\cal L}_0^i) for i <= 6.

  For Khuri-Makdisi's `large model' of the Jacobian, we use
    {\cal L} = {\cal L}_0^{\otimes 3},
  and we need to know  \Gamma(X,{\cal L}_0^i)  for  i <= 9.
*/

static GEN
jacobian_V (GEN X) {
  switch (jacobian_type (X)) {
  case JACOBIAN_TYPE_MEDIUM:
    /* For the medium model,  V = \Gamma(X,{\cal L}_0^2).  */
    return jacobian_V_0_2 (X);
  case JACOBIAN_TYPE_LARGE:
    /* For the large model,  V = \Gamma(X,{\cal L}_0^3).  */
    return jacobian_V_0_3 (X);
  default:
    pari_err (e_MISC, "invalid type for the Jacobian");
    return NULL;
  }
}

static GEN
jacobian_IGS_V (GEN X) {
  switch (jacobian_type (X)) {
  case JACOBIAN_TYPE_MEDIUM:
    return curve_IGS_V (X, 2);
  case JACOBIAN_TYPE_LARGE:
    return curve_IGS_V (X, 3);
  default:
    pari_err (e_MISC, "invalid type for the Jacobian");
    return NULL;
  }
}

static int
jacobian_deg_L (GEN X) {
  switch (jacobian_type (X)) {
  case JACOBIAN_TYPE_MEDIUM:
    /* For the medium model,  {\cal L} = {\cal L}_0^2.  */
    return 2 * jacobian_deg_L_0 (X);
  case JACOBIAN_TYPE_LARGE:
    /* For the large model,  {\cal L} = {\cal L}_0^3.  */
    return 3 * jacobian_deg_L_0 (X);
  default:
    pari_err (e_MISC, "invalid type for the Jacobian");
    return 0;
  }
}

static int
jacobian_dim_V_2 (GEN X) {
  switch (jacobian_type (X)) {
  case JACOBIAN_TYPE_MEDIUM:
    return lg (curve_V (X, 4)) - 1;
  case JACOBIAN_TYPE_LARGE:
    return lg (curve_V (X, 6)) - 1;
  default:
    pari_err (e_MISC, "invalid type for the Jacobian");
    return 0;
  }
}

/*
  Given the spaces  V = \Gamma(X,{\cal L})  and
  W_D = \Gamma(X,{\cal L}(-D)),  find an ideal generating
  set (= basepoint-free subspace) for  W_D  (Khuri-Makdisi,
  Proposition/Algorithm 3.7).
*/
GEN
jacobian_deflate (GEN X, GEN W_D) {
  unsigned long p = curve_base_field_characteristic (X);
  GEN T = curve_base_field_polynomial (X);
  GEN V = jacobian_V (X);
  int degree_D = jacobian_divisor_deg (X, W_D);
  int dim_W_2_D = jacobian_dim_V_2 (X) - degree_D;
  const float alpha = 0.9;
  int h = 1 + ceil (log ((jacobian_deg_L (X) - degree_D) / alpha)
		    / log_ffsize (p, T));
  GEN IGS, IGS_V;
  pari_sp av = avma;

  if (lg(W_D) < 4)
    return gcopy(W_D);
  do {
    avma = av;
    IGS = matsmall_random_subspace(W_D, h, 0, p, T);
    IGS_V = curve_multiply_subspaces(X, IGS, V);
  } while (lg (IGS_V) - 1 < dim_W_2_D);
  return gerepileupto(av, IGS);
}

/*
  Given a basepoint-free subspace of  \Gamma(X,{\cal L}(-D)),
  compute the whole space (Khuri-Makdisi, Proposition/Algorithm 3.9).
*/
GEN
jacobian_inflate (GEN X, GEN IGS_D) {
  GEN V = jacobian_V (X);
  pari_sp av = avma;
  int i;
  switch (jacobian_type(X)) {
  case JACOBIAN_TYPE_MEDIUM:
    i = 2;
    break;
  case JACOBIAN_TYPE_LARGE:
    i = 3;
    break;
  default:
    pari_err (e_MISC, "invalid type for the Jacobian");
    return NULL;
  }
  return gerepileupto(av, curve_divide_subspaces
		      (X, curve_multiply_subspaces(X, IGS_D, V), 2 * i,
		       jacobian_IGS_V(X), i));
}

/*
  Given a divisor  D  of degree  d  with  2g - 1 <= d
  <= deg{\cal L} - 2g  and a non-zero section  s  of  the space
  W_D = \Gamma({\cal L}(-D)),  return  \Gamma({\cal L}(-E)),
  where  E  is the effective divisor such that div(s) = D + E
  (Khuri-Makdisi, Proposition/Algorithm 3.10)
*/
GEN
jacobian_flip (GEN X, GEN W_D, GEN s) {
  GEN V = jacobian_V (X);
  pari_sp av = avma;
  int i;
  switch (jacobian_type (X)) {
  case JACOBIAN_TYPE_MEDIUM:
    i = 2;
    break;
  case JACOBIAN_TYPE_LARGE:
    i = 3;
    break;
  default:
    pari_err (e_MISC, "invalid type for the Jacobian");
    return NULL;
  }
  return gerepileupto (av, curve_divide_subspaces
		       (X, curve_multiply_section_subspace(X, s, V), 2*i,
			jacobian_deflate (X, W_D), i));
}

/*
  Given a divisor  D  on  X, return a random divisor
  representing the same point on the Jacobian.
*/
GEN
jacobian_random_presentation(GEN X, GEN W_D) {
  unsigned long p = curve_base_field_characteristic (X);
  GEN T = curve_base_field_polynomial (X);
  GEN s;
  int i;
  pari_sp av = avma;
  for(i = 0; i < 2; i++) {
    s = gel(matsmall_random_subspace(W_D, 1, 1, p, T), 1);
    W_D = jacobian_flip(X, W_D, s);
  }
  return gerepileupto(av, W_D);
}

/*
  Return 1 if the divisor associated to W_D is of the right degree,
  0 if it is not.  Only valid for the medium model.
*/
#define jacobian_divisor_ok(X, W_D) (jacobian_divisor_deg (X, W_D)	\
				     == jacobian_deg_L_0 (X))

/*
  Return 1 if the divisor associated to W_D is small, 0 if it is not.
  Only valid for the large model.
*/
#define jacobian_divisor_small(X, W_D) (jacobian_divisor_deg (X, W_D)	\
					== jacobian_deg_L_0 (X))
/*
  Return 1 if the divisor associated to W_D is large, 0 if it is not.
  Only valid for the large model.
*/
#define jacobian_divisor_large(X, W_D) (jacobian_divisor_deg (X, W_D)	\
					== 2 * jacobian_deg_L_0 (X))

/*
  Construct the Jacobian of  X  using the specified model.

  TODO: truncate sections to the precision we need for the rest
  of the algorithms.
*/
GEN
curve_jacobian (GEN X, unsigned long model) {
  pari_sp av = avma;
  GEN J = shallowconcat (X, mkvec (utoi (model))), extra;
  GEN V_0 = curve_V (X, 1), s0 = gel (V_0, 1);

  switch (model) {
  case JACOBIAN_TYPE_MEDIUM:
    extra = mkvec(curve_multiply_section_subspace(X, s0, V_0));
    break;
  case JACOBIAN_TYPE_LARGE:
    /* We represent zero as a large divisor.  */
    {
      GEN V_0_2 = jacobian_V_0_2 (X),
	W_D0 = curve_multiply_section_subspace(X, s0, V_0_2),
	s0_2 = curve_multiply_sections(X, s0, s0),
	W_2D0 = curve_multiply_section_subspace(X, s0_2, V_0),
	IGS_W_D0 = jacobian_deflate (J, W_D0),
	IGS_W_2D0 = jacobian_deflate (J, W_2D0);

      extra = mkvecn (6,
		      curve_multiply_section_subspace(X, gel(V_0_2, 1), V_0_2),
		      s0,
		      W_D0,
		      IGS_W_D0,
		      W_2D0,
		      IGS_W_2D0);
      break;
    }
  default:
    pari_err (e_MISC, "invalid type for the Jacobian");
    return NULL;
  }
  return gerepileupto (av, gconcat (J, extra));
}

/*
  Given a divisor  D, return 1 if it represents the zero element
  of the Jacobian, 0 if it does not.

  If  s != NULL  and  D  represents  0, return a suitable section
  witnessing this fact in  *s.
*/
int
jacobian_is_zero_cert (GEN X, GEN W_D, GEN *s) {
  pari_sp av = avma;
  GEN V;
  int is_zero;

  switch (jacobian_type (X)) {
  case JACOBIAN_TYPE_MEDIUM:
    if (jacobian_divisor_ok (X, W_D))
      V = curve_divide_subspaces(X, W_D, 2, curve_V(X, 1), 1);
    else {
      pari_err (e_MISC, "divisor of incorrect degree");
      return 0;
    }
    break;
  case JACOBIAN_TYPE_LARGE:
    if (jacobian_divisor_small (X, W_D))
      V = curve_divide_subspaces(X, W_D, 3, curve_V(X, 2), 2);
    else if (jacobian_divisor_large (X, W_D))
      V = curve_divide_subspaces(X, W_D, 3, curve_V(X, 1), 1);
    else {
      pari_err (e_MISC, "divisor of incorrect degree");
      return 0;
    }
    break;
  default:
    pari_err (e_MISC, "invalid type for the Jacobian");
    return 0;
  }
  is_zero = (lg(V) == 2);
  if (is_zero && s != NULL)
    *s = gerepileupto(av, gel(V, 1));
  else
    avma = av;
  return is_zero;
}

/*
  Given a divisor  D, return 1 if it represents the zero element
  of the Jacobian, 0 if it does not.
*/
int
jacobian_is_zero (GEN X, GEN W_D) {
  return jacobian_is_zero_cert (X, W_D, NULL);
}

/*
  Given two divisors D and E that are either both small or
  both large, return 1 if D and E are linearly equivalent,
  0 if they are not (Khuri-Makdisi, Proposition/Algorithm 3.14).
*/
int
jacobian_equal (GEN X, GEN W_D, GEN W_E) {
  GEN s;
  int dim;
  pari_sp av = avma;

  switch (jacobian_type (X)) {
  case JACOBIAN_TYPE_MEDIUM:
    if (lg (W_D) < 2)
      pari_err_TYPE("jacobian_equal", W_D);
    s = gel (W_D, 1);
    dim = lg (curve_divide_subspaces
	      (X, curve_multiply_section_subspace(X, s, W_E), 4,
	       jacobian_deflate (X, W_D), 2)) - 1;
    break;
  default:
    pari_err (e_MISC, "invalid type for the Jacobian");
    return 0;
  }
  avma = av;
  return dim == 1;
}

/*
  Medium model: return  W_F,  where  F  is an effective divisor
  representing the class of  {\cal L}_0^3(-D - E).
  
  Large model: return  W_F,  where  F  is an effective divisor
  representing the class of  {\cal L}(-D - E)  if  D  and  E  are
  small divisors, or the class of  {\cal L}^2(-D - E)  if  D  and  E
  are large divisors.

  If  s != NULL, return a section with divisor  D + E + F  in  *s.
*/
GEN
jacobian_addflip_cert (GEN X, GEN W_D, GEN W_E, GEN *s) {
  GEN V = jacobian_V (X);
  GEN t, W_F;
  pari_sp av = avma;

  switch (jacobian_type (X)) {
  case JACOBIAN_TYPE_MEDIUM:
    if (jacobian_divisor_ok(X, W_D)
	&& jacobian_divisor_ok(X, W_E)) {
      unsigned long dim = lg (curve_V (X, 2)) - 1;
      GEN W_2_D_E = curve_multiply_subspaces_dim(X, W_D, W_E, dim);
      GEN W0_3_D_E = curve_divide_subspaces (X, W_2_D_E, 4,
					     jacobian_IGS_V_0 (X), 1);
      GEN W0_5_D_E_F;
      t = gel(W0_3_D_E, 1);
      W0_5_D_E_F = curve_multiply_section_subspace(X, t, V);
      W_F = curve_divide_subspaces(X, W0_5_D_E_F, 5,
				   jacobian_deflate (X, W0_3_D_E), 3);
      if (!jacobian_divisor_ok (X, W_F))
	pari_err (e_MISC, "bug in addflip");
    } else {
      pari_err (e_MISC, "divisor of incorrect degree in addflip");
      return NULL;
    }
    break;
  case JACOBIAN_TYPE_LARGE:
    if (jacobian_divisor_small (X, W_D)
	&& jacobian_divisor_small (X, W_E)) {
      GEN W_D_flip, W_D_E;
      t = gel(W_D, 1);
      W_D_flip = jacobian_flip(X, W_D, t);
      W_D_E = curve_divide_subspaces(X, curve_multiply_section_subspace(X, t, W_E), 6,
				     jacobian_deflate(X, W_D_flip), 3);
      W_F = jacobian_flip(X, W_D_E, gel(W_D_E, 1));
    } else if (jacobian_divisor_large (X, W_D)
	       && jacobian_divisor_large (X, W_E)) {
      GEN W_D_flip = jacobian_flip (X, W_D, gel (W_D, 1));
      t = gel(W_E, 1);
      W_F = curve_divide_subspaces(X, curve_multiply_section_subspace(X, t, W_D_flip), 6,
				   jacobian_deflate(X, W_E), 3);
    } else {
      pari_err (e_MISC, "divisors neither both small nor both large");
      return NULL;
    }
    break;
  default:
    pari_err (e_MISC, "invalid type for the Jacobian");
    return NULL;
  }
  if (s != NULL) {
    *s = gcopy(t);
    gerepileall(av, 2, &W_F, s);
    return W_F;
  }
  return gerepileupto(av, W_F);
}

GEN
jacobian_addflip (GEN X, GEN W_D, GEN W_E) {
  return jacobian_addflip_cert (X, W_D, W_E, NULL);
}

GEN
jacobian_negate (GEN X, GEN W_D) {
  pari_sp av = avma;

  switch (jacobian_type (X)) {
  case JACOBIAN_TYPE_MEDIUM:
    if (jacobian_divisor_ok (X, W_D)) {
      GEN W_F = jacobian_flip (X, W_D, gel (W_D, 1));
      if (!jacobian_divisor_ok (X, W_F))
	pari_err (e_MISC, "bug in negate");
      return gerepileupto (av, W_F);
    }
    pari_err (e_MISC, "divisor of incorrect degree in negate");
  case JACOBIAN_TYPE_LARGE:
    if (jacobian_divisor_small (X, W_D)) {
      GEN s = jacobian_s0 (X);
      GEN W_D0_D = curve_divide_subspaces(X, curve_multiply_section_subspace(X, s, W_D), 6,
					  jacobian_IGS_W_2D0(X), 3);
      return gerepileupto (av, jacobian_flip (X, W_D0_D, gel (W_D0_D, 1)));
    }
    if (jacobian_divisor_large (X, W_D)) {
      GEN s = jacobian_s0 (X);
      GEN W_D_flip = jacobian_flip (X, W_D, gel (W_D, 1));
      GEN W_F = curve_divide_subspaces(X, curve_multiply_section_subspace(X, s, W_D_flip), 6,
				       jacobian_IGS_W_D0(X), 3);
      return gerepileupto (av, W_F);
    }
    pari_err (e_MISC, "divisor neither small nor large");
  default:
    pari_err (e_MISC, "invalid type for the Jacobian");
  }
  return NULL;
}

GEN
jacobian_add (GEN X, GEN W_D, GEN W_E) {
  pari_sp av = avma;
  GEN W_F = jacobian_negate(X, jacobian_addflip(X, W_D, W_E));
  return gerepileupto(av, W_F);
}

GEN
jacobian_subtract (GEN X, GEN W_D, GEN W_E) {
  pari_sp av = avma;
  GEN W_F = jacobian_addflip(X, jacobian_negate(X, W_D), W_E);
  return gerepileupto(av, W_F);
}

/*
  Medium model: find a divisor  D  such that  \O_X(P - Q)  and
  {\cal L}(-D) represents the same class.

  Large model: TODO
*/
GEN
jacobian_abel_jacobi_map (GEN X, GEN W_P, GEN W_Q) {
  GEN s, W;
  pari_sp av = avma;
  
  switch (jacobian_type (X)) {
  case JACOBIAN_TYPE_MEDIUM:
    s = gel(W_P, 1);
    W = curve_multiply_section_subspace(X, s, jacobian_V_0(X));
    W = curve_multiply_subspaces(X, W, W_Q);
    W = curve_divide_subspaces (X, W, 3, W_P, 1);
    return gerepileupto (av, W);
  case JACOBIAN_TYPE_LARGE:
    pari_err (e_MISC, "not implemented");
  default:
    pari_err (e_MISC, "invalid type for the Jacobian");
  }
  return NULL;
}

/*
  Given an element  x in Pic^0 X  and a rational point  O
  of  X, return the  O-normalised representative of  x.
*/
GEN
jacobian_normalised_representative (GEN X, GEN W_D, GEN W_rO) {
  unsigned long r = curve_degree (X);
  GEN W_D_rO, W, result;
  pari_sp av = avma;

  switch (jacobian_type (X)) {
  case JACOBIAN_TYPE_MEDIUM:
    for (; r > 0; r--) {
      W_D_rO = curve_multiply_subspaces(X, W_D, gel(W_rO, r));
      W_D_rO = curve_divide_subspaces (X, W_D_rO, 4, jacobian_IGS_V (X), 2);
      if (lg (W_D_rO) == 2) {
	W = curve_multiply_section_subspace(X, gel(W_D_rO, 1), jacobian_V(X));
	result = cgetg (3, t_VEC);
	gel(result, 1) = utoi (curve_degree (X) - r);
	gel(result, 2) = curve_divide_subspaces (X, W, 4,
						 curve_divisor_IGS (X, W_D, 2), 2);
	return gerepileupto (av, result);
      }
    }
    break;
  case JACOBIAN_TYPE_LARGE:
    pari_err (e_MISC, "not implemented");
  default:
    pari_err (e_MISC, "invalid type for the Jacobian");
  }
  /* not reached */
  pari_err(e_MISC, "inconsistent data in jacobian_normalised_representative");
  return NULL;
}

/*
  Given an element  x in Pic^0 X  and a rational point  O
  of  X, return a "normalised" divisor determined by  x.
*/
GEN
jacobian_normalised_divisor (GEN X, GEN W_D, GEN W_rO) {
  unsigned long r = curve_degree (X);
  GEN W4_D_rO, W2_D_rO, W, result;
  pari_sp av = avma;

  switch (jacobian_type (X)) {
  case JACOBIAN_TYPE_MEDIUM:
    for (; r > 0; r--) {
      W4_D_rO = curve_multiply_subspaces(X, W_D, gel(W_rO, r));
      W2_D_rO = curve_divide_subspaces (X, W4_D_rO, 4,
					jacobian_IGS_V (X), 2);
      if (lg (W2_D_rO) == 2) {
	W = curve_multiply_section_subspace(X, gel(W2_D_rO, 1), curve_V(X, 4));
	result = cgetg (3, t_VEC);
	gel(result, 1) = utoi (curve_degree (X) - r);
	gel(result, 2) = curve_divide_subspaces (X, W, 6,
						 curve_divisor_IGS(X, W4_D_rO, 4), 4);
	return gerepileupto (av, result);
      }
    }
    /* not reached */
    pari_err (e_MISC, "inconsistent");
  case JACOBIAN_TYPE_LARGE:
    pari_err (e_MISC, "not implemented");
  default:
    pari_err (e_MISC, "invalid type for the Jacobian");
  }
  return NULL;
}

/* Return the stratification of the point represented by  W_D.  */
GEN
jacobian_stratification (GEN X, GEN W_D, GEN W_rO) {
  unsigned long r = curve_degree (X);
  GEN W_D_rO;
  pari_sp av = avma;

  switch (jacobian_type (X)) {
  case JACOBIAN_TYPE_MEDIUM:
    W_D = jacobian_subtract (X, W_D, gel (W_rO, r));
    for (; r > 0; r--) {
      W_D_rO = curve_multiply_subspaces(X, W_D, gel(W_rO, r));
      W_D_rO = curve_divide_subspaces(X, W_D_rO, 4, jacobian_IGS_V (X), 2);
      if (lg (W_D_rO) == 2)
	return gerepileuptoint (av, utoi (curve_degree (X) - r));
    }
    /* not reached */
    pari_err (e_MISC, "inconsistent");
  case JACOBIAN_TYPE_LARGE:
    pari_err (e_MISC, "not implemented");
  default:
    pari_err (e_MISC, "invalid type for the Jacobian");
  }
  return NULL;
}

/* Apply the given base change to the Jacobian  J.  */
GEN
jacobian_base_change (GEN J, GEN ext) {
  pari_sp av = avma;
  GEN extra;
  switch (jacobian_type (J)) {
  case JACOBIAN_TYPE_MEDIUM:
    extra = mkvec2 (utoipos (JACOBIAN_TYPE_MEDIUM),
		    finite_field_matrix_up (jacobian_zero (J), ext));
    break;
  case JACOBIAN_TYPE_LARGE:
    extra = mkvecn (7, utoipos (JACOBIAN_TYPE_LARGE),
		    finite_field_matrix_up (jacobian_zero (J), ext),
		    finite_field_vector_up (jacobian_s0 (J), ext),
		    finite_field_matrix_up (jacobian_W_D0 (J), ext),
		    finite_field_matrix_up (jacobian_IGS_W_D0 (J), ext),
		    finite_field_matrix_up (jacobian_W_2D0 (J), ext),
		    finite_field_matrix_up (jacobian_IGS_W_2D0 (J), ext));
    break;
  default:
    pari_err (e_MISC, "invalid type for the Jacobian");
    return NULL;
  }
  return gerepileupto (av, gconcat (curve_base_change (J, ext), extra));
}

/*
  Return the number of rational points of J.
*/
GEN
jacobian_count_points (GEN J) {
  GEN f = curve_numerator_zeta_function (J);
  return gsubst (f, varn(f), gen_1);
}

GEN
jacobian_random_point (GEN J) {
  /*
    We need to find a random divisor of degree  d.
    In the case of the large model, the choice below means that
    we pick a small divisor.
  */
  long d = curve_degree (J);
  if (jacobian_type (J) == JACOBIAN_TYPE_MEDIUM)
    return curve_random_divisor (J, d, 2);
  if (jacobian_type (J) == JACOBIAN_TYPE_LARGE)
    return curve_random_divisor (J, d, 3);
  pari_err (e_MISC, "invalid type for the Jacobian");
  return NULL;
}

GEN
jacobian_multiply (GEN J, GEN P, GEN n) {
  pari_sp av = avma, av1;
  long i, j, k, l;
  GEN c, u, v;

  if (gequal0(n))
    return gcopy(jacobian_zero(J));
  if (gequal1(n))
    return gcopy(P);
  if (gequalm1(n))
    return jacobian_negate(J, P);
  c = addflip_chain(n);
  l = lg(c);
  /* Record where each step is used last.  */
  u = zero_zv(l);
  for (i = 1; i < l; i++) {
    j = mael(c, i, 1);
    k = mael(c, i, 2);
    u[j + 1] = u[k + 1] = i;
  }
  av1 = avma;
  v = zerovec(l);
  gel(v, 1) = P;
  for (i = 2; i <= l; i++) {
    if (i < l && u[i + 1] == 0)
      continue;
    j = mael(c, i - 1, 1);
    k = mael(c, i - 1, 2);
    if (j == 0)
      gel(v, i) = jacobian_negate(J, gel(v, k));
    else if (k == 0)
      gel(v, i) = jacobian_negate(J, gel(v, j));
    else
      gel(v, i) = jacobian_addflip(J, gel(v, j), gel(v, k));
    if (j && u[j + 1] < i)
      gel(v, j) = gen_0;
    if (k && v[k + 1] < i)
      gel(v, k) = gen_0;
    if (gc_needed(av1, 2))
      v = gerepilecopy(av1, v);
  }
  return gerepileupto(av, gel(v, l));
}

GEN
jacobian_linear_combination (GEN J, GEN P, GEN n) {
  pari_sp av = avma;
  GEN Q = jacobian_zero (J);
  long j;
  for(j = 1; j < lg(P); j++)
    Q = jacobian_add(J, Q, jacobian_multiply(J, gel(P, j), gel(n, j)));
  return gerepileupto(av, Q);
}

GEN
jacobian_Frob(GEN J, GEN P, unsigned long m) {
  return curve_divisor_Frob(J, P, m);
}

/*
  Compute  f(Frob_q)(P), where  q = p^m.
*/
GEN
jacobian_Frob_polynomial(GEN J, GEN P, GEN f, unsigned long m) {
  pari_sp av = avma;
  long d = poldegree(f, -1), i;
  GEN Frob_P = cgetg(d + 2, t_VEC), Q;

  gel(Frob_P, 1) = P;
  for(i = 1; i <= d; i++)
    gel(Frob_P, i + 1) = jacobian_Frob(J, gel(Frob_P, i), m);
  Q = jacobian_multiply(J, gel(Frob_P, d + 1), truecoeff(f, d));
  for(i = 0; i <= d - 1; i++)
    Q = jacobian_add(J, Q, jacobian_multiply(J, gel(Frob_P, i + 1),
					     truecoeff(f, i)));
  return gerepileupto(av, Q);
}

/*
  Given a Jacobian  J  arising by base change from the given extension,
  descend  P  to a point on the original Jacobian.
*/
GEN
jacobian_descend(GEN J, GEN P, GEN extension, GEN multiples_O) {
  pari_sp av = avma;
  GEN Q = gel(jacobian_normalised_representative(J, P, multiples_O), 2);
  return gerepileupto(av, finite_field_matrix_down(Q, extension));
}
