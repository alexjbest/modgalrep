install(modular_curve_characteristic_polynomial_frobenius, "GL", , "libmodcurve.so");
install(modular_curve_numerator_zeta_function, "GL", , "libmodcurve.so");
install(modular_curve_zeta_function, "GL", , "libmodcurve.so");
install(modular_curve_count_points, "GLD1,L,", , "libmodcurve.so");
install(modular_curve_jacobian_count_points, "GLD1,L,", , "libmodcurve.so");
install(modular_curve, "GLD0,L,", , "libmodcurve.so");
install(modular_curve_jacobian_medium, "GL", , "libmodcurve.so");
install(modular_curve_jacobian_large, "GL", , "libmodcurve.so");
