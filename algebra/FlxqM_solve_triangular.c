#include <pari/pari.h>
#include "FlxqM_solve_triangular.h"

static GEN
FlxqM_rsolve_upper_1(GEN U, GEN B, GEN T, ulong p) {
  return FlxqM_Flxq_mul(B, Flxq_inv(gcoeff(U, 1, 1), T, p), T, p);
}

static GEN
FlxqM_rsolve_upper_2(GEN U, GEN B, GEN T, ulong p) {
  GEN a = gcoeff(U, 1, 1), b = gcoeff(U, 1, 2), d = gcoeff(U, 2, 2);
  GEN D = Flxq_mul(a, d, T, p), Dinv = Flxq_inv(D, T, p);
  GEN ainv = Flxq_mul(d, Dinv, T, p), dinv = Flxq_mul(a, Dinv, T, p);
  GEN B1 = rowslice(B, 1, 1);
  GEN B2 = rowslice(B, 2, 2);
  GEN X2 = FlxqM_Flxq_mul(B2, dinv, T, p);
  GEN X1 = FlxqM_Flxq_mul(FlxM_sub(B1, FlxqM_Flxq_mul(X2, b, T, p), p),
			  ainv, T, p);
  return vconcat(X1, X2);
}

/* Solve U*X = B,  U upper triangular and invertible.  */
GEN
FlxqM_rsolve_upper(GEN U, GEN B, GEN T, ulong p) {
  pari_sp av = avma;
  long n = lg(U) - 1, n1;
  GEN U1, U2, U11, U12, U22;
  GEN B1, B2, X1, X2;

  if (n == 0)
    return B;
  if (n == 1)
    return FlxqM_rsolve_upper_1(U, B, T, p);
  if (n == 2)
    return FlxqM_rsolve_upper_2(U, B, T, p);
  n1 = (n + 1)/2;
  U1 = vecslice(U, 1, n1);
  U2 = vecslice(U, n1 + 1, n);
  U11 = rowslice(U1, 1, n1);
  U12 = rowslice(U2, 1, n1);
  U22 = rowslice(U2, n1 + 1, n);
  B1 = rowslice(B, 1, n1);
  B2 = rowslice(B, n1 + 1, n);
  X2 = FlxqM_rsolve_upper(U22, B2, T, p);
  B1 = FlxM_sub(B1, FlxqM_mul(U12, X2, T, p), p);
  X1 = FlxqM_rsolve_upper(U11, B1, T, p);
  return gerepilecopy(av, vconcat(X1, X2));
}

static GEN
FlxqM_lsolve_upper_1(GEN U, GEN B, GEN T, ulong p) {
  return FlxqM_Flxq_mul(B, Flxq_inv(gcoeff(U, 1, 1), T, p), T, p);
}

static GEN
FlxqM_lsolve_upper_2(GEN U, GEN B, GEN T, ulong p) {
  GEN a = gcoeff(U, 1, 1), b = gcoeff(U, 1, 2), d = gcoeff(U, 2, 2);
  GEN D = Flxq_mul(a, d, T, p), Dinv = Flxq_inv(D, T, p);
  GEN ainv = Flxq_mul(d, Dinv, T, p), dinv = Flxq_mul(a, Dinv, T, p);
  GEN B1 = vecslice(B, 1, 1);
  GEN B2 = vecslice(B, 2, 2);
  GEN X1 = FlxqM_Flxq_mul(B1, ainv, T, p);
  GEN X2 = FlxqM_Flxq_mul(FlxM_sub(B2, FlxqM_Flxq_mul(X1, b, T, p), p),
			  dinv, T, p);
  return shallowconcat(X1, X2);
}

/* Solve X*U = B,  U upper triangular and invertible.  */
GEN
FlxqM_lsolve_upper(GEN U, GEN B, GEN T, ulong p) {
  pari_sp av = avma;
  long n = lg(U) - 1, n1;
  GEN U1, U2, U11, U12, U22;
  GEN B1, B2, X1, X2;

  if (n == 0)
    return B;
  if (n == 1)
    return FlxqM_lsolve_upper_1(U, B, T, p);
  if (n == 2)
    return FlxqM_lsolve_upper_2(U, B, T, p);
  n1 = (n + 1)/2;
  U1 = vecslice(U, 1, n1);
  U2 = vecslice(U, n1 + 1, n);
  U11 = rowslice(U1, 1, n1);
  U12 = rowslice(U2, 1, n1);
  U22 = rowslice(U2, n1 + 1, n);
  B1 = vecslice(B, 1, n1);
  B2 = vecslice(B, n1 + 1, n);
  X1 = FlxqM_lsolve_upper(U11, B1, T, p);
  B2 = FlxM_sub(B2, FlxqM_mul(X1, U12, T, p), p);
  X2 = FlxqM_lsolve_upper(U22, B2, T, p);
  return gerepileupto(av, gconcat(X1, X2));
}

/* Solve  L*X = A, where  L = [u; a_2; ...; a_n].  */
static GEN
FlxqM_rsolve_lower_unit_1(GEN L, GEN A, GEN T, ulong p) {
  return rowslice(A, 1, 1);
}

static GEN
FlxqM_rsolve_lower_unit_2(GEN L, GEN A, GEN T, ulong p) {
  GEN X1 = rowslice(A, 1, 1);
  GEN X2 = FlxM_sub(rowslice(A, 2, 2),
		    FlxqM_Flxq_mul(X1, gcoeff(L, 2, 1), T, p), p);
  return vconcat(X1, X2);
}

/*
  Solve  L*X = A, where  L  is lower triangular with ones on
  the diagonal (at least as many rows as columns).
*/
GEN
FlxqM_rsolve_lower_unit(GEN L, GEN A, GEN T, ulong p) {
  long m = lg(L) - 1, m1, n;
  GEN L1, L2, L11, L21, L22, A1, A2, X1, X2;
  pari_sp av = avma;

  if (m == 0)
    return zeromat(0, lg(A) - 1);
  if (m == 1)
    return FlxqM_rsolve_lower_unit_1(L, A, T, p);
  if (m == 2)
    return FlxqM_rsolve_lower_unit_2(L, A, T, p);
  m1 = (m + 1)/2;
  n = nbrows(L);
  L1 = vecslice(L, 1, m1);
  L2 = vecslice(L, m1 + 1, m);
  L11 = rowslice(L1, 1, m1);
  L21 = rowslice(L1, m1 + 1, n);
  L22 = rowslice(L2, m1 + 1, n);
  A1 = rowslice(A, 1, m1);
  A2 = rowslice(A, m1 + 1, n);
  X1 = FlxqM_rsolve_lower_unit(L11, A1, T, p);
  X2 = FlxqM_rsolve_lower_unit(L22, FlxM_sub(A2, FlxqM_mul(L21, X1, T, p), p), T, p);
  return gerepilecopy(av, vconcat(X1, X2));
}

static GEN
FlxqM_lsolve_lower_unit_2(GEN L, GEN A, GEN T, ulong p) {
  GEN X2 = vecslice(A, 2, 2);
  GEN X1 = FlxM_sub(vecslice(A, 1, 1),
		    FlxqM_Flxq_mul(X2, gcoeff(L, 2, 1), T, p), p);
  return shallowconcat(X1, X2);
}

/*
  Solve  L*X = A, where  L  is square lower triangular
  with ones on the diagonal.
*/
GEN
FlxqM_lsolve_lower_unit(GEN L, GEN A, GEN T, ulong p) {
  long m = lg(L) - 1, m1;
  GEN L1, L2, L11, L21, L22, A1, A2, X1, X2;
  pari_sp av = avma;

  if (m <= 1)
    return A;
  if (m == 2)
    return FlxqM_lsolve_lower_unit_2(L, A, T, p);
  m1 = (m + 1)/2;
  L1 = vecslice(L, 1, m1);
  L2 = vecslice(L, m1 + 1, m);
  L11 = rowslice(L1, 1, m1);
  L21 = rowslice(L1, m1 + 1, m);
  L22 = rowslice(L2, m1 + 1, m);
  A1 = vecslice(A, 1, m1);
  A2 = vecslice(A, m1 + 1, m);
  X2 = FlxqM_lsolve_lower_unit(L22, A2, T, p);
  X1 = FlxqM_lsolve_lower_unit(L11, FlxM_sub(A1, FlxqM_mul(X2, L21, T, p), p), T, p);
  return gerepileupto(av, gconcat(X1, X2));
}
