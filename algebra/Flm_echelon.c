#include <pari/pari.h>
#include <pari/paripriv.h>  /* swap, ucoeff */

/* Redefine ucoeff to avoid problems with -fstrict-aliasing.  */
#undef ucoeff
#define ucoeff(a,i,j) uel(gel(a, j), i)

#include "Flm_echelon.h"
#include "Flm_solve_triangular.h"

static const ulong Flm_ECHELON_LIMIT = 5;

static GEN
indexcompl(GEN v, long n) {
  long i, j = 1, k = 1, m = lg(v) - 1;
  GEN w = cgetg(n - m + 1, t_VECSMALL);
  for (i = 1; i <= n; i++) {
    if (j <= m && v[j] == i)
      j++;
    else
      w[k++] = i;
  }
  return w;
}

static ulong
Flm_echelon_gauss(GEN A, GEN *R, GEN *C, ulong p) {
  long i, j, k, m = nbrows(A), n = lg(A) - 1, pr, pc, u, v;

  *R = cgetg(m + 1, t_VECSMALL);
  for (j = 1, pr = 0; j <= n; j++) {
    for (pr++, pc = 0; pr <= m; pr++) {
      for (k = j; k <= n; k++) {
        v = ucoeff(A, pr, k);
        if (!pc && v)
          pc = k;
      }
      if (pc)
        break;
    }
    if (!pc)
      break;
    (*R)[j] = pr;
    if (pc != j)
      swap(gel(A, j), gel(A, pc));
    u = Fl_inv(ucoeff(A, pr, j), p);
    for (i = pr + 1; i <= m; i++) {
      v = Fl_mul(ucoeff(A, i, j), u, p);
      ucoeff(A, i, j) = v;
      for (k = j + 1; k <= n; k++) {
        ucoeff(A, i, k) = Fl_sub(ucoeff(A, i, k),
                                 Fl_mul(ucoeff(A, pr, k), v, p), p);
      }
    }
  }
  setlg(*R, j);
  *C = vecslice(A, 1, j - 1);
  return j - 1;
}

/* column echelon form */
ulong
Flm_echelon(GEN A, GEN *R, GEN *C, ulong p) {
  long i, j, j1, j2, m = nbrows(A), n = lg(A) - 1, n1, r, r1, r2;
  GEN A1, A2, R1, R1c, C1, R2, C2;
  GEN A12, A22, B2, C11, C21, M12;
  pari_sp av = avma;

  if (m < Flm_ECHELON_LIMIT || n < Flm_ECHELON_LIMIT)
    return Flm_echelon_gauss(Flm_copy(A), R, C, p);

  n1 = (n + 1)/2;
  A1 = vecslice(A, 1, n1);
  A2 = vecslice(A, n1 + 1, n);
  r1 = Flm_echelon(A1, &R1, &C1, p);
  if (!r1)
    return Flm_echelon(A2, R, C, p);
  else if (r1 == m) {
    *R = R1;
    *C = C1;
    return r1;
  }
  R1c = indexcompl(R1, m);
  C11 = rowpermute(C1, R1);
  C21 = rowpermute(C1, R1c);
  if (R1[r1] != r1) {
    for (j = 1; j <= r1; j++)
      for (i = 1; i <= R1[j] - j; i++)
        ucoeff(C21, i, j) = 0;
  }
  A12 = rowpermute(A2, R1);
  A22 = rowpermute(A2, R1c);
  M12 = Flm_rsolve_lower_unit(C11, A12, p);
  B2 = Flm_sub(A22, Flm_mul(C21, M12, p), p);
  r2 = Flm_echelon(B2, &R2, &C2, p);
  if (!r2) {
    *R = R1;
    *C = C1;
    r = r1;
  } else {
    R2 = perm_mul(R1c, R2);
    C2 = rowpermute(vconcat(zero_Flm(r1, r2), C2),
                    perm_inv(vecsmall_concat(R1, R1c)));
    r = r1 + r2;
    *R = cgetg(r + 1, t_VECSMALL);
    *C = cgetg(r + 1, t_MAT);
    for (j = j1 = j2 = 1; j <= r; j++) {
      if (j2 > r2 || (j1 <= r1 && R1[j1] < R2[j2])) {
        gel(*C, j) = gel(C1, j1);
        (*R)[j] = R1[j1++];
      } else {
        gel(*C, j) = gel(C2, j2);
        (*R)[j] = R2[j2++];
      }
    }
  }
  if (gc_needed(av, 1))
    gerepileall(av, 2, R, C);
  return r;
}
