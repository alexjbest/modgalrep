char_conductor(chi) = chi[1];
char_modulus(chi) = length(chi[3]);
char_evaluate(chi,a) = chi[3][a % length(chi[3]) + 1];

dirichlet_group_modulus(G) = G[1];
dirichlet_group_gen(G) = G[2];
dirichlet_group_exponent(G) = G[3];
dirichlet_group_characters(G) = G[4];
dirichlet_group_order(G) = length(G[4]);

install(char_apply_galois, "GL", char_apply_galois, "libmodular.so");
install(char_invert, "G", char_invert, "libmodular.so");
install(char_multiply, "GGG", char_multiply, "libmodular.so");
install(char_divide, "GGG", char_divide, "libmodular.so");
install(char_make_primitive, "G", char_make_primitive, "libmodular.so");
install(dirichlet_group, "LG", dirichlet_group, "libmodular.so");
