/* Multiplication methods; cf. jacobian.h */
MULTIPLY_VALUES = 0;
MULTIPLY_POWER_SERIES = 1;

install (numzeta_base_change, "GL", frobenius_polynomial_base_change, "libjacobian.so");
install (curve_base_field_cardinality, "G", , "libjacobian.so");
install (curve_count_points, "G", , "libjacobian.so");
install (curve_base_change, "GG", curve_base_change, "libjacobian.so");
install (curve_construct, "GLGLGLG", curve_construct, "libjacobian.so");
install (curve_divisor_to_point, "GG", curve_divisor_to_point, "libjacobian.so");
install (curve_point_to_divisor, "GG", curve_point_to_divisor, "libjacobian.so");
install (curve_point_multiples, "GG", curve_point_multiples, "libjacobian.so");
install (curve_decompose_divisor, "GGGL", curve_decompose_divisor, "libjacobian.so");
install (curve_divisor_Frob, "GG", curve_divisor_Frob, "libjacobian.so");
install (curve_random_divisor, "GLL", curve_random_divisor, "libjacobian.so");
install (curve_random_point, "G", , "libjacobian.so");
install (curve_random_prime_divisor, "GLL", curve_random_prime_divisor, "libjacobian.so");
install (curve_jacobian, "GL", curve_jacobian, "libjacobian.so");

install (jacobian_base_change, "GG", jacobian_base_change, "libjacobian.so");
install (jacobian_deflate, "GG", deflation, "libjacobian.so");
install (jacobian_inflate, "GG", inflation, "libjacobian.so");
install (jacobian_random_presentation, "GG",, "libjacobian.so");
install (jacobian_is_zero, "iGG", jacobian_is_zero, "libjacobian.so");
install (jacobian_is_zero_cert, "iGGD&", jacobian_is_zero, "libjacobian.so");
install (jacobian_equal, "iGGG", jacobian_equal, "libjacobian.so");
install (jacobian_negate, "GG", jacobian_negate, "libjacobian.so");
install (jacobian_addflip, "GGG", jacobian_addflip, "libjacobian.so");
install (jacobian_addflip_cert, "GGGD&", jacobian_addflip, "libjacobian.so");
install (jacobian_add, "GGG", jacobian_add, "libjacobian.so");
install (jacobian_subtract, "GGG", jacobian_subtract, "libjacobian.so");
install (jacobian_abel_jacobi_map, "GGG", jacobian_abel_jacobi_map, "libjacobian.so");
install (jacobian_normalised_representative, "GGG", jacobian_normalised_representative, "libjacobian.so");
install (jacobian_stratification, "GGG", jacobian_stratification, "libjacobian.so");
install (jacobian_count_points, "G", , "libjacobian.so");
install (jacobian_random_point, "G", , "libjacobian.so");
install (jacobian_addflip, "GGG", , "libjacobian.so");
install (jacobian_multiply, "GGG", , "libjacobian.so");
install (jacobian_linear_combination, "GGG", , "libjacobian.so");
install (jacobian_Frob, "GGL", , "libjacobian.so");
install (jacobian_tate_pairing, "GGGL", , "libjacobian.so");
install (jacobian_weil_pairing, "GGGL", , "libjacobian.so");

\\ defining data: X[1]
curve_base_field_characteristic(X) = X[2];
curve_base_field_polynomial(X) = X[3];
curve_multiplication_method(X) = X[4];
curve_V_power(X, i) = X[5][i];
curve_V(X) = X[5][1];
curve_V_2(X) = X[5][2];
curve_V_3(X) = X[5][3];
curve_V_4(X) = X[5][4];
curve_max_power(X) = length (X[5]);
curve_IGS_V(X) = X[6];
curve_numerator_zeta_function(X) = X[7];

curve_genus(X) = {
  matsize (curve_V_2 (X))[2] - 2 * matsize (curve_V (X))[2] + 1;
}

curve_degree(X) = {
  matsize (curve_V_2 (X))[2] - matsize (curve_V (X))[2];
}


JACOBIAN_TYPE_MEDIUM = 2;
JACOBIAN_TYPE_LARGE = 3;
jacobian_type(J) = J[9];
jacobian_zero(J) = J[10];


/*
  Compute  n * P  using binary powering.
  This doesn't use the fact that addflip is faster than add.
*/
jacobian_multiply_binpow(J, P, n) = {
  local (Q);
  Q = jacobian_zero (J);
  if (n < 0,
      n = -n;
      P = jacobian_negate (J, P));
  while (n > 0,
	 if (n % 2 == 1,
	     Q = jacobian_add (J, P, Q);
	     n -= 1);
	 P = jacobian_add (J, P, P);
	 n /= 2);
  return (Q);
}

/* Return the  l-torsion subgroup of  J.  */
jacobian_torsion_points(J, l) = {
  local (N, factors_l, points, P);
  N = jacobian_count_points (J);
  factors_l = 0;
  points = [];
  /* Replace  N  by its prime-to-l  part.  */
  while (N % l == 0,
	 N /= l;
	 factors_l++);
  while (length (points) < l^factors_l,
	 print ("random point");
	 P = jacobian_random_point (J);
	 print ("multiply by ", N);
	 P = jacobian_multiply (J, P, N);
	 print ("lookup");
	 found = 0;
	 for (i = 1, length (points),
	      if (jacobian_equal (J, points[i], P),
		  found = 1;
		  break));
	 if (!found,
	     print ("new point (", length (points) + 1, "/", l^factors_l, ")");
	     points = concat (points, [P])));
  return (points);
}

install(matsmall_to_RgM, "GLDG", , "libalgebra.so");
install(RgM_to_matsmall, "GLDG", , "libalgebra.so");

/*
  Given a curve  X,  return the same curve but where sections
  are represented such that all multiplications can be done
  pointwise.  If there are not enough rational points, return
  the original curve.
*/
curve_to_fast_representation(X) = {
  local (Y, max_power, precis, p, T, npoints, points, P, found, i, V);
  max_power = curve_max_power (X);
  precis = max_power * curve_degree (X) + 1;
  points = curve_count_points (X);
  if (points < precis,
      return (X));
  p = curve_base_field_characteristic (X);
  T = curve_base_field_polynomial (X);
  npoints = 0;
  points = vector (precis);
  print ("finding ", precis, " random points");
  while (npoints < precis,
	 /*
	   Generate a random point as a morphism  V -> k.
	   We don't have to normalise  P  because of the way
	   random_point works.
	 */
	 P = curve_random_point (X);
	 /* If we hadn't found the point yet, add it to the list.  */
	 found = 0;
	 for (i = 1, npoints,
	      if (points[i] == P,
		  found = 1;
		  break));
	 if (!found,
	     npoints++;
	     print1 (npoints, " ");
	     points[npoints] = P));
  print ();
  /* Construct a matrix with the points as rows.  */
  if (T != 0,
      points = apply (x -> matsmall_to_RgM(x, p, T), points),
      points = apply (x -> matsmall_to_RgM(x, p), points));
  V = mattranspose (concat (apply (x -> mattranspose (x), points)));
  if (T != 0,
      V = RgM_to_matsmall(V, p, T),
      V = RgM_to_matsmall(V, p));
  return (curve_construct (X[1], p, T, MULTIPLY_VALUES,
			   V, max_power, curve_numerator_zeta_function (X)));
}

jacobian_to_fast_representation(J) = {
  return (curve_jacobian (curve_to_fast_representation (J),
			  jacobian_type (J)));
}
