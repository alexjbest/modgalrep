/* -*- coding: utf-8 -*- */
#include <math.h>  /* log, sqrt */
#include <pari/pari.h>

#include "finite-fields.h"
#include "matsmall.h"

#include "curve.h"


/*
  Implementation of basic operations on linear systems on a curve
  (Khuri-Makdisi, Proposition/Algorithm 2.7)

  We assume we work over a finite field whose elements are of type Fl
  or Flxq (see the PARI manual).
*/

/*
  Elements of \Gamma(X,{\cal L}) are represented as vectors of type
  t_VECSMALL with entries in [0, p) in the case of type Fl, or as
  vectors of type t_COL with entries of type t_VECSMALL (with the same
  meaning as Flx) in the case of type Flxq.

  We can either multiply power series or values of sections at
  rational points.
*/

static GEN
multiply_values_Fl (GEN s, GEN t, unsigned long p) {
  int i;
  GEN result;

  if (typ (s) != t_VECSMALL
      || typ (t) != t_VECSMALL
      || lg (s) != lg (t))
    pari_err_TYPE2("multiply_values_Fl", s, t);
  result = cgetg (lg (s), t_VECSMALL);
  for (i = 1; i < lg (s); i++)
    result[i] = Fl_mul (s[i], t[i], p);
  return result;
}

static GEN
multiply_values_Flxq (GEN s, GEN t, unsigned long p, GEN T) {
  int i;
  GEN result;

  if (typ (s) != t_COL
      || typ (t) != t_COL
      || lg (s) != lg (t))
    pari_err_TYPE2("multiply_values_Flxq", s, t);
  result = cgetg (lg (s), t_COL);
  for (i = 1; i < lg (s); i++)
    gel (result, i) = Flxq_mul (gel (s, i), gel (t, i), T, p);
  return result;
}

static GEN
multiply_power_series_Fl (GEN s, GEN t, unsigned long p) {
  int i, l = lg (s), ls = l, lt = l;
  GEN s1, t1, tmp, result;
  pari_sp av;

  if (typ (s) != t_VECSMALL
      || typ (t) != t_VECSMALL
      || lg (t) != l)
    pari_err_TYPE2("multiply_power_series_Fl", s, t);
  while (ls > 1 && s[ls - 1] == 0)
    ls--;
  while (lt > 1 && t[lt - 1] == 0)
    lt--;
  if (ls == 1 || lt == 1)
    return zero_Flv(l - 1);
  result = cgetg (l, t_VECSMALL);
  av = avma;
  s1 = cgetg(ls + 1, t_VECSMALL);
  t1 = cgetg(lt + 1, t_VECSMALL);
  s1[1] = t1[1] = evalvarn(MAXVARN);
  for (i = 1; i < ls; i++)
    s1[i + 1] = s[i];
  for (i = 1; i < lt; i++)
    t1[i + 1] = t[i];
  tmp = Flx_mul (s1, t1, p);
  for (i = 1; i < ls + lt - 2 && i < l; i++)
    result[i] = tmp[i + 1];
  while (i < l)
    result[i++] = 0;
  avma = av;
  return result;
}

static GEN
multiply_power_series_Flxq (GEN s, GEN t, unsigned long p, GEN T) {
  int i, l = lg (s);
  GEN s1, t1, tmp, result;
  pari_sp av = avma;

  if (typ (s) != t_COL
      || typ (t) != t_COL
      || lg (t) != l)
    pari_err_TYPE2("multiply_power_series_Flxq", s, t);
  s1 = cgetg(l + 1, t_POL);
  t1 = cgetg(l + 1, t_POL);
  s1[1] = evalsigne(1) | evalvarn(MAXVARN);
  t1[1] = evalsigne(1) | evalvarn(MAXVARN);
  for(i = 1; i < l; i++)
    gel (s1 + 1, i) = gel (s, i);
  for(i = 1; i < l; i++)
    gel (t1 + 1, i) = gel (t, i);
  tmp = FlxqX_mul (s1, t1, T, p);
  result = cgetg (l, t_COL);
  for (i = 1; i < lg(tmp) - 1; i++)
    gel (result, i) = gel (tmp, i + 1);
  if (i < l) {
    GEN z = zero_Flx(varn(T));
    for (; i < l; i++)
      gel (result, i) = z;
  }
  return gerepilecopy (av, result);
}

/*
  Return the element s * t.
*/
GEN
multiply_sections (GEN s, GEN t,
		   unsigned long multiplication_method,
		   unsigned long p, GEN T) {
  switch (multiplication_method) {
  case MULTIPLY_VALUES:
    return T ? multiply_values_Flxq (s, t, p, T)
      : multiply_values_Fl (s, t, p);
  case MULTIPLY_POWER_SERIES:
    return T ? multiply_power_series_Flxq (s, t, p, T)
      : multiply_power_series_Fl (s, t, p);
  default:
    pari_err (e_MISC, "unknown multiplication method");
    return NULL;
  }
}

/*
  Compute the product of an element  s \in \Gamma(X,{\cal L})
  and a subspace  W \subset \Gamma(X,{\cal L}) (given as a basis).
  Assume multiplication by  s  is injective.
*/
GEN
multiply_section_subspace (GEN s, GEN W,
			   unsigned long multiplication_method, 
			   unsigned long p, GEN T) {
  int i;
  GEN result = cgetg (lg (W), t_MAT);

  for (i = 1; i < lg (W); i++)
    gel (result, i) = multiply_sections (s, gel (W, i),
					 multiplication_method,
					 p, T);
  return result;
}

/*
  Return a matrix whose column vectors are the products

  [W1[1] * W2[1], W1[1] * W2[2], ..., W1[1] * W2[n],
   W1[2] * W2[1], W1[2] * W2[2], ..., W1[2] * W2[n],
   ...,
   W1[m] * W2[1], W1[m] * W2[2], ..., W1[m] * W2[n]].

  If W1 == W2, only one of W1[i] * W2[j] and W1[j] * W2[i]
  is taken.
*/
GEN
multiply_subsets (GEN W1, GEN W2,
		  unsigned long multiplication_method,
		  unsigned long p, GEN T) {
  int i1, i2, m, n, k, sym;
  GEN result;
  if (typ (W1) != t_MAT
      || typ (W2) != t_MAT)
    pari_err_TYPE2("multiply_subsets", W1, W2);
  m = lg (W1) - 1;
  n = lg (W2) - 1;
  sym = (W1 == W2);
  result = cgetg ((sym ? m*(m + 1)/2 : m*n) + 1, t_MAT);
  for (k = 1, i1 = 1; i1 <= m; i1++)
    for (i2 = (sym ? i1 : 1); i2 <= n; i2++)
      gel (result, k++)
	= multiply_sections (gel (W1, i1), gel (W2, i2),
			     multiplication_method, p, T);
  return result;
}

/*
  Compute a basis for  span { s * t | s in W1, t in W2 }.
*/
GEN
multiply_subspaces (GEN W1, GEN W2,
		    unsigned long multiplication_method,
		    unsigned long p, GEN T) {
  GEN A;
  pari_sp av = avma;

  A = multiply_subsets (W1, W2, multiplication_method, p, T);
  return gerepileupto(av, matsmall_echelon(A, NULL, p, T));
}

/*
  Compute a basis for  span { s * t | s in W1, t in W2 }
  when the dimension of the result is known.
*/
GEN
multiply_subspaces_dim (GEN W1, GEN W2, unsigned long dimension,
			unsigned long multiplication_method,
			unsigned long p, GEN T) {
  GEN A, S1, S2, v;
  pari_sp av = avma;
  /* TODO: maybe s1 and s2 below need to be balanced somehow.  */
  int s1 = (int) ceil (sqrt(dimension)), s2 = s1;

  S1 = matsmall_random_subspace(W1, s1, 0, p, T);
  S2 = matsmall_random_subspace(W2, s2, 0, p, T);
  A = multiply_subspaces (S1, S2, multiplication_method, p, T);
  while (lg (A) - 1 < dimension) {
    v = multiply_subspaces(matsmall_random_subspace(W1, 1, 1, p, T),
			   matsmall_random_subspace(W2, 1, 1, p, T),
			   multiplication_method, p, T);
    A = matsmall_echelon(shallowconcat(A, v), NULL, p, T);
    if (gc_needed(av, 2))
      A = gerepileupto (av, A);
  }
  if (lg (A) - 1 > dimension)
    pari_err (e_MISC, "dimension larger than expected");
  return gerepileupto (av, A);
}

/*
  Compute a basis for the subspace Z ÷ W of V, i.e.
  { v in V | vW subset Z }.  (We do not exactly produce the
  matrix  Q  which appears in Khuri-Makdisi's equation (2.15).)
*/
GEN
divide_subspaces (GEN V, GEN Z, GEN W,
		  unsigned long multiplication_method,
		  unsigned long p, GEN T) {
  int dim_V;
  int h, i, j, m;
  GEN K, R;
  pari_sp av = avma;

  dim_V = lg (V) - 1;
  if (dim_V == 0)
    return cgetg (1, t_MAT);
  /*
    Compute the cokernel of  Z,  i.e. the space of row vectors  v
    with  v * Z == 0.
  */
  K = matsmall_coker (Z, p, T);
  if (lg (K) == 1)
    return gcopy (V);
  m = lg (gel (K, 1)) - 1;
  R = cgetg (lg (V), t_MAT);
  h = lg (W) - 1;
  for (j = 1; j <= dim_V; j++)
    gel (R, j) = cgetg (h * m + 1, (T == NULL) ? t_VECSMALL : t_COL);
  for (; h; h--) {
    GEN s = gel (W, h);
    GEN sV = multiply_section_subspace (s, V, multiplication_method, p, T);
    GEN KsV = matsmall_mul (K, sV, p, T);
    for (i = 1; i <= m; i++)
      for (j = 1; j <= dim_V; j++)
	gel (R, j)[(h - 1) * m + i] = gel (KsV, j)[i];
  }
  return gerepileupto (av, matsmall_mul (V, matsmall_ker (R, p, T), p, T));
}

/*
  Compute an ideal generating set for the space  \Gamma(X,{\cal L}),
  i.e. a basepoint-free subspace of small dimension.

  alpha  is Khuri-Makdisi's  eta,  a fixed real number between 0 and 1.
  A smaller  alpha  increases  h, but also the probability that a given
  random set of  h  sections is an IGS (this probability is at least
  1 - alpha).
*/
static GEN
full_space_IGS (GEN V, GEN V_2,
		unsigned long multiplication_method,
		unsigned long p, GEN T) {
  long degree = lg (V_2) - lg (V), dim_V_3 = 2 * lg (V_2) - lg (V) - 1;
  float alpha = 0.9;
  int h = 1 + ceil (log (degree / alpha) / log_ffsize (p, T));
  GEN IGS, IGS_V_2;
  pari_sp av = avma;

  do {
    avma = av;
    IGS = matsmall_random_subspace(V, h, 0, p, T);
    IGS_V_2 = multiply_subspaces (IGS, V_2, multiplication_method, p, T);
  } while (lg (IGS_V_2) - 1 < dim_V_3);
  cgiv (IGS_V_2);
  return IGS;
}


/*** Curves ***/

/* Construct a curve from the given data.  */
GEN
curve_construct (GEN info, unsigned long p, GEN T,
		 unsigned long multiplication_method, GEN V,
		 unsigned long max_power, GEN num_zeta_function) {
  GEN Y = cgetg (9, t_VEC);
  int i;

  if (T != NULL && gequal0 (T))
    T = NULL;

  gel (Y, 1) = gcopy (info);
  gel (Y, 2) = stoi (p);
  gel (Y, 3) = (T == NULL) ? gen_0 : gcopy (T);
  gel (Y, 4) = stoi (multiplication_method);
  gel (Y, 5) = cgetg (max_power + 1, t_VEC);
  gmael (Y, 5, 1) = gcopy (V);
  for (i = 2; i <= max_power; i++)
    gmael (Y, 5, i) = multiply_subspaces (curve_V (Y, 1),
					  curve_V (Y, i - 1),
					  multiplication_method, p, T);
  gel (Y, 6) = cgetg (max_power / 3 + 1, t_VEC);
  for (i = 1; i <= max_power / 3; i++)
    gmael (Y, 6, i) = full_space_IGS (curve_V (Y, i),
				      curve_V (Y, 2 * i),
				      multiplication_method, p, T);
  gel (Y, 7) = cgetg (max_power + 1, t_VEC);
  for (i = 1; i <= max_power; i++) {
    pari_sp av = avma;
    gmael (Y, 7, i) = gerepileupto(av, gel(matsmall_indexrank(gmael(Y, 5, i), p, T), 1));
  }
  gel (Y, 8) = gcopy (num_zeta_function);

  return Y;
}

/* Return the cardinality of the base field of  X.  */
GEN
curve_base_field_cardinality (GEN X) {
  GEN p = gel (X, 2);
  GEN T = curve_base_field_polynomial (X);
  return (T == NULL) ? gcopy (p) : powiu (p, degpol (T));
}

/* Return the degree of the base field of  X  over its prime field.  */
unsigned long
curve_base_field_degree (GEN X) {
  GEN T = curve_base_field_polynomial (X);
  return (T == NULL) ? 1 : degpol (T);
}

/* Return the number of rational points of  X.  */
GEN
curve_count_points (GEN X) {
  GEN f = curve_numerator_zeta_function (X);
  return gadd(gaddgs(polcoeff0 (f, 1, -1), 1),
	      curve_base_field_cardinality (X));
}

/*
  Wrapper for multiply_section_subspace().
*/
GEN
curve_multiply_sections(GEN X, GEN s, GEN t) {
  unsigned long p = curve_base_field_characteristic(X);
  GEN T = curve_base_field_polynomial(X);
  unsigned long mm = curve_multiplication_method(X);
  return multiply_sections(s, t, mm, p, T);
}

/*
  Wrapper for multiply_section_subspace().
*/
GEN
curve_multiply_section_subspace(GEN X, GEN s, GEN V) {
  unsigned long p = curve_base_field_characteristic(X);
  GEN T = curve_base_field_polynomial(X);
  unsigned long mm = curve_multiplication_method(X);
  return multiply_section_subspace(s, V, mm, p, T);
}

/*
  Wrapper for multiply_subspaces().
*/
GEN
curve_multiply_subspaces(GEN X, GEN V, GEN W) {
  unsigned long p = curve_base_field_characteristic(X);
  GEN T = curve_base_field_polynomial(X);
  unsigned long mm = curve_multiplication_method(X);
  return multiply_subspaces(V, W, mm, p, T);
}

/*
  Wrapper for multiply_subspaces_dim().
*/
GEN
curve_multiply_subspaces_dim(GEN X, GEN V, GEN W, unsigned long dim) {
  unsigned long p = curve_base_field_characteristic(X);
  GEN T = curve_base_field_polynomial(X);
  unsigned long mm = curve_multiplication_method(X);
  return multiply_subspaces_dim(V, W, dim, mm, p, T);
}

/*
  Compute a basis for the subspace Z ÷ W of V, i.e.
  { v in V_i | vW subset Z }.  (We do not exactly produce the
  matrix  Q  which appears in Khuri-Makdisi's equation (2.15).)
*/
GEN
curve_divide_subspaces (GEN X, GEN Z, unsigned long i_Z,
			GEN W, unsigned long i_W) {
  unsigned long p = curve_base_field_characteristic(X);
  GEN T = curve_base_field_polynomial(X);
  int h = lg(W) - 1, i, i_V = i_Z - i_W;
  GEN V = curve_V(X, i_V), pivots, K, M, R, Zt;
  pari_sp av = avma;

  /* Extract the useful rows.  */
  pivots = curve_pivots_V(X, i_Z);
  Zt = shallowextract(matsmall_transpose(Z, p, T), pivots);
  K = matsmall_ker(Zt, p, T);
  M = cgetg(h + 1, t_VEC);
  for (i = 1; i <= h; i++) {
    GEN s = gel(W, i);
    GEN sV = curve_multiply_section_subspace(X, s, V);
    GEN sVt = shallowextract(matsmall_transpose(sV, p, T), pivots);
    gel(M, i) = matsmall_mul(sVt, K, p, T);
  }
  R = matsmall_transpose(shallowconcat1(M), p, T);
  return gerepileupto(av, matsmall_mul(V, matsmall_ker(R, p, T), p, T));
}


/*** Morphisms ***/

/*
  A finite morphism  f: X -> Y  is represented by the following data:
  - a curve  X, represented in the usual way;
  - a basepoint-free subspace  V \subseteq \Gamma(X,{\cal L}).
*/

/* TODO: algorithms (image/inverse image of divisors, push-forward,
   residue field degree and ramification index at a point, ... */ 


/*** Base extension ***/

/*
  Given the numerator of the zeta function of a curve,
  return the numerator of the zeta function of the curve
  after a base change of degree  n.
*/
GEN
numzeta_base_change (GEN z, int n) {
  pari_sp av = avma;
  long vx = varn (z);
  long vy = fetch_var ();
  /* Construct the polynomial  x^n - y.  */
  GEN P = cgetg (n + 3, t_POL);
  int i;

  P[1] = evalsigne (1) | evalvarn (vx);
  gel (P, 2) = gneg (pol_x (vy));
  for (i = 1; i < n; i++)
    gel (P, i + 2) = gen_0;
  gel (P, n + 2) = gen_1;
  /*
    Compute the numerator of the base-changed zeta function
    by taking the resultant of it and  x^n - y.
  */
  z = polresultant0(z, P, vx, 1);
  /* numzeta is now a polynomial in y.  */
  setvarn (z, vx);
  delete_var ();
  return gerepileupto (av, z);
}

/*
  Apply the base change described by  EXT  to the curve  X.
  See finite-fields.c for the description of  EXT.
*/
GEN
curve_base_change (GEN X, GEN ext) {
  GEN Y, T = curve_base_field_polynomial (X);
  int i, n;
  pari_sp av = avma;

  if (cmpii (gel (X, 2), gel (ext, 1)))
    pari_err (e_MISC, "base field characteristics do not agree");
  if ((T == NULL && (degpol (gel (ext, 2)) != 1))
      || (T != NULL && !gequal(T, gel(ext, 2))))
    pari_err (e_MISC, "base fields do not agree");
  n = degpol (gel (ext, 3)) / degpol (gel (ext, 2));
  Y = cgetg (9, t_VEC);
  gel (Y, 1) = gcopy (gel (X, 1));
  gel (Y, 2) = icopy (gel (X, 2));
  gel (Y, 3) = gcopy (gel (ext, 3));
  gel (Y, 4) = icopy (gel (X, 4));
  gel (Y, 5) = cgetg (lg (gel (X, 5)), t_VEC);
  for (i = 1; i < lg (gel (X, 5)); i++)
    gmael (Y, 5, i) = finite_field_matrix_up (gmael (X, 5, i), ext);
  gel (Y, 6) = cgetg (lg (gel (X, 6)), t_VEC);
  for (i = 1; i < lg (gel (X, 6)); i++)
    gmael (Y, 6, i) = finite_field_matrix_up (gmael (X, 6, i), ext);
  gel (Y, 7) = gcopy(gel (X, 7));
  gel (Y, 8) = numzeta_base_change (gel (X, 8), n);
  return gerepilecopy(av, Y);
}
