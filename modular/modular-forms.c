#include <pari/pari.h>

#include "cusp-forms.h"
#include "dirichlet-characters.h"
#include "eisenstein-series.h"
#include "modular-forms.h"
#include "modular-groups.h"

static GEN
block_matrix(GEN v)
{
  return shallowmatconcat(diagonal(v));
}

GEN
modular_forms_q_expansion_basis(GEN group, unsigned long weight, GEN p, unsigned long prec)
{
  GEN E, S, z, result;
  long bound;
  pari_sp av = avma;
  
  bound = gtos(modular_group_modular_forms_degree(group, weight)) + 1;
  if (prec < bound)
    prec = bound;
  E = eisenstein_series_q_expansion_basis(eisenstein_series(group, weight, prec), p, prec);
  S = cusp_forms_q_expansion_basis(cusp_forms(group, weight, p), prec - 1);
  z = zerovec(lg(S) - 1);
  if (!gequal0(p))
    z = gmodulo(z, p);
  result = gconcat(E, shallowmatconcat(mkcol2(z, S)));
  if (rank(result) != lg (result) - 1) {
    /* congruence between Eisenstein series and cusp forms */
    result = modular_forms_q_expansion_basis(group, weight, gen_0, prec);
    result = gmodulo(matrixqz0(result, p), p);
  }
  return gerepileupto(av, result);
}

/* TODO: case where there is a congruence between Eisenstein series and cusp forms */
GEN
modular_forms_as_hecke_module(GEN group, unsigned long weight, GEN p, unsigned long prec)
{
  GEN E, S, z, qexp, diam, T;
  unsigned long bound;
  long i, level = modular_group_level(group);
  pari_sp av = avma;

  bound = gtos(modular_group_modular_forms_degree(group, weight)) + 1;
  if (prec < bound)
    prec = bound;
  E = eisenstein_series_as_hecke_module(group, weight, p, prec);
  S = cusp_forms_as_hecke_module(group, weight, p, prec - 1);
  z = zerovec(lg(gel(S, 1)) - 1);
  if (!gequal0(p))
    z = gmodulo(z, p);
  qexp = shallowconcat(gel(E, 1), shallowmatconcat(mkcol2(z, gel(S, 1))));
  diam = cgetg(level + 1, t_VEC);
  for (i = 1; i <= level; i++)
    gel(diam, i) = block_matrix(mkvec2(gmael(E, 2, i), gmael(S, 2, i)));
  T = cgetg(prec, t_VEC);
  for (i = 1; i < prec; i++)
    gel(T, i) = block_matrix(mkvec2(gmael(E, 3, i), gmael(S, 3, i)));
  return gerepilecopy(av, mkvec3(qexp, diam, T));
}
