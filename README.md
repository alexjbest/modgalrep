modgalrep
=========

A program to compute Galois representations attached to modular forms
over finite fields.


Author
------

Peter Bruin, <P.J.Bruin@math.leidenuniv.nl>


Prerequisites
-------------

- A recent version of PARI/GP, <http://pari.math.u-bordeaux.fr/>.

- To install from the Git repository: GNU autotools.


Installation
------------

If you obtained this package from the Git repository, first install
Autoconf files:

    $ autoreconf -i

If necessary, set the environment variables ``CFLAGS`` and ``LDFLAGS``
to indicate where PARI/GP is installed (replace ``<paridir>`` below by
the correct directory):

    $ export CFLAGS="-g -O3 -Wall -I<paridir>/include"
    $ export LDFLAGS="-L<paridir>/lib -Wl,-rpath -Wl,<paridir>/lib"

Then build and install the package:

    $ ./configure
    $ make
    $ make install


Usage
-----

Here is an example:

    $ cd examples/level_11_weight_2_mod_2
    $ make

After ``make`` finishes, the file ``info.txt`` will contain a summary
of the data computed.  Not everything is computed by default; look at
the file ``Makefile.form.in`` for possible ``make`` targets.


TODO
----

- Better documentation

- Better reduction of output data to obtain polynomials with small
  height
