\r jacobian

\\ to convert matrices to small format
install (ZM_to_Flm, "GL");

\\ TODO: is there some smart alternative using serreverse?
xy_series(f, var, P, prec) = {
  local(x = var[1], y = var[2], df_dx, df_dy, ser, ser_new);

  if(substvec(f, [x, y], P) != 0,
     error("point not on curve"));

  f = substvec(f, [x, y], [x, y] + P);
  df_dx = deriv(f, x);
  df_dy = deriv(f, y);

  if(substvec(df_dy, [x, y], [0, 0]) != 0,
     v = y;
     f = subst(f, x, u);
     df_dy = subst(df_dy, x, u);
     ser = O(u^prec);
     while(1,
	   ser_new = ser - subst(f, y, ser)/subst(df_dy, y, ser);
	   if(ser_new == ser, break);
	   ser = ser_new);
     return([u + O(u^prec), ser] + P));
  if(substvec(df_dx, [x, y], [0, 0]) != 0,
     f = subst(f, y, u);
     df_dx = subst(df_dx, y, u);
     ser = O(u^prec);
     while(1,
	   ser_new = ser - subst(f, x, ser)/subst(df_dy, x, ser);
	   if(ser_new == ser, break);
	   ser = ser_new);
     return([ser, u + O(u^prec)] + P));
  error("not smooth");
}

/*
  Convert a plane curve to Khuri-Makdisi's representation.
*/
make_curve(description, f, deg, var, P, functions, p, numzeta=0) = {
  local(max_power = 7,
	prec = max_power * deg + 1,
	xy = xy_series(f, var, P, prec),
	fs = substvec(functions, var, xy),
	V = matrix(prec, length(functions), i, j,
		   polcoeff(fs[j], i - 1)),
	X);

  V = ZM_to_Flm(lift(V*Mod(1, p)), p);
  X = curve_construct(description, p, 0, MULTIPLY_POWER_SERIES,
		      V, max_power, numzeta);
  return(X);
}

/*
  Convert a point to Khuri-Makdisi's representation.
*/
plane_curve_point(X, P) = {
  local(p = curve_base_field_characteristic(X));
  P = ZM_to_Flm(Mat(P), p);
  return(curve_point_to_divisor(X, P));
}

/*
  Return all multiples of  P  up to  deg X.
*/
plane_curve_point_multiples(X, P) = {
  local(p = curve_base_field_characteristic(X));
  P = ZM_to_Flm(Mat(P), p);
  return(curve_point_multiples(X, P));
}

/*
  Compute the Jacobian of a plane curve.
*/
plane_curve_jacobian(description, f, deg, var, P, functions, p, numzeta=0) = {
  local(X = make_curve(description, f, deg, var, P, functions, p, numzeta));
  return(curve_jacobian(X, JACOBIAN_TYPE_MEDIUM));
}

/*
  Given points P_0, ..., P_n on the curve,
  return the classes [P_1 - P_0], ..., [P_n - P_0].
*/
plane_curve_jacobian_points(J, points) = {
  points = apply(Q -> plane_curve_point(J, Q), points);
  return(vector(length(points) - 1, i,
		jacobian_abel_jacobi_map(J, points[i + 1], points[1])));
}
