#ifndef EISENSTEIN_SERIES
#define EISENSTEIN_SERIES

/*
  An Eisenstein series is specified as  [n, k, epsilon, chi1, chi2, t],
  where  n  is the level,  k  the weight,  t  a positive integer,
  and  chi1, chi2  are Dirichlet characters such that
  modulus(chi1) * modulus(chi2) * t | n.

  If k == 2 and chi1, chi2 are the trivial character, then
    E = E_2(q) - t.E_2(q^t);
  otherwise
    E = E_{k,chi1,chi2}(q^t).
*/

static inline GEN
eisenstein_series_level(GEN E) {
  return gel(E, 1);
}

static inline GEN
eisenstein_series_weight(GEN E) {
  return gel(E, 2);
}

static inline GEN
eisenstein_series_character(GEN E) {
  return gel(E, 3);
}

static inline GEN
eisenstein_series_chi1(GEN E) {
  return gel(E, 4);
}

static inline GEN
eisenstein_series_chi2(GEN E) {
  return gel(E, 5);
}

GEN eisenstein_series_q_expansion(GEN E, unsigned long prec);
GEN eisenstein_series_with_character(GEN eps, GEN D, unsigned long weight, unsigned long prec);
GEN eisenstein_series(GEN group, unsigned long weight, unsigned long prec);
GEN eisenstein_series_galois(GEN E, long a);
GEN eisenstein_series_q_expansion_basis(GEN E, GEN p, unsigned long prec);
GEN eisenstein_series_as_hecke_module(GEN group, unsigned long weight, GEN p, unsigned long prec);

#endif
