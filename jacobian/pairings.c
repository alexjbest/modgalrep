#include <pari/pari.h>

#include "matsmall.h"

#include "addflip-chain.h"
#include "curve.h"
#include "divisor.h"
#include "jacobian.h"
#include "pairings.h"


static GEN
norm_of_section(GEN X, GEN s, GEN V1, GEN V2, GEN Q1, GEN Q2) {
  unsigned long p = curve_base_field_characteristic(X);
  GEN T = curve_base_field_polynomial(X);
  GEN M = curve_divisor_section_as_matrix(X, s, V1, V2, Q1, Q2);
  return matsmall_det(M, p, T);
}

/*
  Linearity of the norm functor
  (thesis, Algorithm IV.2.9, simplified).
*/
static GEN
norm_functor_linearity(GEN X, long i, long j,
		       GEN W_2_E, GEN W_i_2_D1,
		       GEN W_j_2_D2, GEN W_i_j_2_D1_D2,
		       GEN Q_2, GEN Q_i_2_D1,
		       GEN Q_j_2_D2, GEN Q_i_j_2_D1_D2) {
  unsigned long p = curve_base_field_characteristic(X);
  GEN T = curve_base_field_polynomial(X);
  GEN V_2 = curve_V(X, 2);
  GEN W_j_4_D2, W_i_j_4_D1_D2;
  GEN W_j_4_D2_E, W_i_j_4_D1_D2_E;
  GEN Q_j_4_D2, Q_i_j_4_D1_D2;
  GEN beta0, beta2;
  GEN delta0, delta1, delta2, delta3;
  GEN lambda;
  pari_sp av = avma;

  W_j_4_D2 = curve_multiply_subspaces(X, V_2, W_j_2_D2);
  W_i_j_4_D1_D2 = curve_multiply_subspaces(X, V_2, W_i_j_2_D1_D2);
  W_j_4_D2_E = curve_multiply_subspaces(X, W_2_E, W_j_2_D2);
  W_i_j_4_D1_D2_E = curve_multiply_subspaces(X, W_2_E, W_i_j_2_D1_D2);
  Q_j_4_D2 = matsmall_quotient(W_j_4_D2, W_j_4_D2_E, p, T);
  Q_i_j_4_D1_D2 = matsmall_quotient(W_i_j_4_D1_D2, W_i_j_4_D1_D2_E, p, T);

  do {
    beta0 = gel(matsmall_random_subspace(V_2, 1, 1, p, T), 1);
    delta0 = norm_of_section(X, beta0, W_j_2_D2, W_j_4_D2,
			     Q_j_2_D2, Q_j_4_D2);
  } while (gequal0(delta0));
  delta1 = norm_of_section(X, beta0, W_i_j_2_D1_D2, W_i_j_4_D1_D2,
			   Q_i_j_2_D1_D2, Q_i_j_4_D1_D2);

  do {
    beta2 = gel(matsmall_random_subspace(W_j_2_D2, 1, 1, p, T), 1);
    delta2 = norm_of_section(X, beta2, V_2, W_j_4_D2,
			     Q_2, Q_j_4_D2);
  } while (gequal0(delta2));
  delta3 = norm_of_section(X, beta2, W_i_2_D1, W_i_j_4_D1_D2,
			   Q_i_2_D1, Q_i_j_4_D1_D2);

  lambda = gdiv(gmul(delta0, delta3),
		gmul(delta1, delta2));
  return gerepileupto(av, lambda);
}

static void
quotient_maps(GEN X, GEN D, GEN W_E, GEN *W3_D, GEN *Q3_D) {
  unsigned long p = curve_base_field_characteristic(X);
  GEN T = curve_base_field_polynomial(X);
  long l, m = lg(D) - 2;
  GEN W3_Dl, W4_Dl_E, W3_Dl_E;

  *W3_D = cgetg(m + 2, t_VEC);
  *Q3_D = cgetg(m + 2, t_VEC);
  for (l = 0; l <= m; l++) {
    W3_Dl = curve_multiply_subspaces(X, curve_V(X, 1), gel(D, l + 1));
    W4_Dl_E = curve_multiply_subspaces(X, gel(D, l + 1), W_E);
    W3_Dl_E = curve_divide_subspaces(X, W4_Dl_E, 4,
				     curve_IGS_V(X, 1), 1);
    gel(*W3_D, l + 1) = W3_Dl;
    gel(*Q3_D, l + 1) = matsmall_quotient(W3_Dl, W3_Dl_E, p, T);
  }
}

static GEN
compute_gamma(GEN X, GEN D, GEN c, GEN W_E, GEN s, GEN W2, GEN Q2,
	      GEN W3_D, GEN Q3_D, GEN gamma_0, GEN gamma_1) {
  unsigned long p = curve_base_field_characteristic(X);
  GEN T = curve_base_field_polynomial(X);
  long i, j, l, m = lg(c);
  GEN Q4_Di_Dj, Q5_Di_Dj_Dl;
  GEN W6_Di_Dj_Dl, W5_Di_Dj_Dl;
  GEN W4_Di_Dj, W6_Di_Dj_E, W4_Di_Dj_E;
  GEN W7_Di_Dj_Dl_E, W5_Di_Dj_Dl_E; 
  GEN gamma, lambda1, lambda2, lambda, M;

  gamma = cgetg(m + 2, t_VEC);
  gel(gamma, 1) = gamma_0;
  gel(gamma, 2) = gamma_1;
  for (l = 2; l <= m; l++) {
    i = mael(c, l - 1, 1);
    j = mael(c, l - 1, 2);
    W4_Di_Dj = curve_multiply_subspaces(X, gel(D, i + 1), gel(D, j + 1));
    W6_Di_Dj_E = curve_multiply_subspaces(X, W4_Di_Dj, W_E);
    W4_Di_Dj_E = curve_divide_subspaces(X, W6_Di_Dj_E, 6,
					curve_IGS_V(X, 2), 2);
    Q4_Di_Dj = matsmall_quotient(W4_Di_Dj, W4_Di_Dj_E, p, T);
    lambda1 = norm_functor_linearity(X, 1, 1, W_E,
				     gel(W3_D, i + 1),
				     gel(W3_D, j + 1),
				     W4_Di_Dj,
				     Q2,
				     gel(Q3_D, i + 1),
				     gel(Q3_D, j + 1),
				     Q4_Di_Dj);

    W6_Di_Dj_Dl = curve_multiply_subspaces(X, W4_Di_Dj, gel(D, l + 1));
    W5_Di_Dj_Dl = curve_divide_subspaces(X, W6_Di_Dj_Dl, 6,
					 curve_IGS_V(X, 1), 1);
    W7_Di_Dj_Dl_E = curve_multiply_subspaces(X, W5_Di_Dj_Dl, W_E);
    W5_Di_Dj_Dl_E = curve_divide_subspaces(X, W7_Di_Dj_Dl_E, 7,
					   curve_IGS_V(X, 2), 2);
    Q5_Di_Dj_Dl = matsmall_quotient(W5_Di_Dj_Dl, W5_Di_Dj_Dl_E, p, T);
    M = curve_divisor_section_as_matrix(X, gel(s, l - 1),
					W2, W5_Di_Dj_Dl,
					Q2, Q5_Di_Dj_Dl);
    Q5_Di_Dj_Dl = matsmall_solve(M, Q5_Di_Dj_Dl, p, T);

    lambda2 = norm_functor_linearity(X, 2, 1, W_E,
				     W4_Di_Dj,
				     gel(W3_D, l + 1),
				     W5_Di_Dj_Dl,
				     Q2,
				     Q4_Di_Dj,
				     gel(Q3_D, l + 1),
				     Q5_Di_Dj_Dl);
    lambda = gmul(lambda1, lambda2);
    gel(gamma, l + 1) = gdiv(lambda, gmul(gel(gamma, i + 1),
					  gel(gamma, j + 1)));
  }
  return gamma;
}

/*
  Compute isomorphisms of the form  I^E_{s,t}
  (thesis, Algorithm IV.3.8).
*/
static GEN
iso(GEN X, GEN c, GEN D, GEN W_E, GEN s, GEN u, GEN v) {
  unsigned long p = curve_base_field_characteristic(X);
  GEN T = curve_base_field_polynomial(X);
  long m = lg(c);
  GEN W2, Q2, W3_D, Q3_D;
  GEN gamma, delta, M;
  pari_sp av = avma;

  W2 = curve_V(X, 2);
  Q2 = matsmall_quotient(W2, W_E, p, T);
  quotient_maps(X, D, W_E, &W3_D, &Q3_D);

  /* normalise the data for D_0 */
  M = curve_divisor_section_as_matrix(X, u, W2, gel(W3_D, 1),
				      Q2, gel(Q3_D, 1));
  gel(Q3_D, 1) = matsmall_solve(M, gel(Q3_D, 1), p, T);

  gamma = compute_gamma(X, D, c, W_E, s, W2, Q2, W3_D, Q3_D,
			gen_1, gen_1);
  delta = norm_of_section(X, v, W2, gel(W3_D, m + 1),
			  Q2, gel(Q3_D, m + 1));
  return gerepileupto(av, ginv(gmul(gel(gamma, m + 1), delta)));
}

static void
torsion_data(GEN J, GEN W_D, GEN u_D, GEN c,
	     GEN *D, GEN *s, GEN *v) {
  long i, j, l, m = lg(c);

  *D = cgetg(m + 2, t_VEC);  /* D_i = gel(*D, i + 1) */
  *s = cgetg(m, t_VEC);
  gel(*D, 1) = curve_multiply_section_subspace(J, u_D, curve_V(J, 1));
  gel(*D, 2) = W_D;
  for (l = 2; l <= m; l++) {
    i = mael(c, l - 1, 1);
    j = mael(c, l - 1, 2);
    gel(*D, l + 1) = jacobian_addflip_cert(J, gel(*D, i + 1), gel(*D, j + 1),
					   &gel(*s, l - 1));
  }
  if (!jacobian_is_zero_cert(J, gel(*D, m + 1), v))
    pari_err(e_MISC, "inconsistent: line bundle is non-trivial");
}

/*
  Compute the Tate-Lichtenbaum-Frey-Rück pairing: let  m  be a
  positive integer not divisible by the characteristic of the
  base field  k, and such that  k  contains the  m-th roots of
  unity.  Then there is a non-degenerate pairing

    J(k)[m] \times J(k)/mJ(k) ----> k^\times/k^{\times m}
              (x, y)         \mapsto      {x, y}_m

  which can be computed as described by Frey and Rück.  The points
  x  and  y  are represented as  {\cal L}(-D)  and  {\cal L}(-E).
  Because  x  is an  m-torsion point,  {\cal L}^m  has a global
  section  f  whose divisor equals  mD.  We take any rational
  section  g  of  {\cal L}(-E)  and check whether its divisor
  div(g)  is disjoint from  D.  If so, the result  {x, y}_m
  is equal  to  f(div(g))  (up to sign).
*/
GEN
jacobian_tate_pairing(GEN J, GEN W_D, GEN W_E, long n) {
  GEN q = curve_base_field_cardinality(J);
  GEN c, D, s, u, v, Iplus, Iminus;
  pari_sp av = avma;

  if (n <= 0 || smodis(q, n) != 1)
    pari_err(e_MISC,
	     "base field does not contain the roots of unity of order %li",
	     n);

  if (jacobian_type(J) != JACOBIAN_TYPE_MEDIUM)
    pari_err_IMPL("jacobian_tate_pairing for non-medium models");

  c = addflip_chain(stoi(n));
  u = gel(curve_V(J, 1), 1);
  torsion_data(J, W_D, u, c, &D, &s, &v);

  Iplus = iso(J, c, D, jacobian_zero(J), s, u, v);
  Iminus = iso(J, c, D, W_E, s, u, v);
  return gerepileupto(av, powgi(gdiv(Iplus, Iminus),
				diviuexact(subis(q, 1), n)));
}

static GEN
eval_half(GEN X, GEN c, GEN D, GEN W_E, GEN s, GEN u, GEN v) {
  unsigned long p = curve_base_field_characteristic(X);
  GEN T = curve_base_field_polynomial(X);
  long m = lg(c);
  GEN W2, Q2, W3, Q3, W3_E, W3_D, Q3_D;
  GEN gamma_0, gamma_1, gamma, delta, M0, M1;
  pari_sp av = avma;

  W2 = curve_V(X, 2);
  Q2 = matsmall_quotient(W2, W_E, p, T);
  W3 = curve_V(X, 3);
  W3_E = curve_multiply_subspaces(X, curve_V(X, 1), W_E);
  Q3 = matsmall_quotient(W3, W3_E, p, T);

  quotient_maps(X, D, W_E, &W3_D, &Q3_D);

  gamma_0 = norm_of_section(X, u, W2, gel(W3_D, 1),
			    Q2, gel(Q3_D, 1));
  M0 = matsmall_solve_left(gel(Q3_D, 1),
			   matsmall_mul(Q3, matsmall_solve(W3, gel(W3_D, 1),
							   p, T),
					p, T),
			   p, T);
  M1 = matsmall_solve_left(gel(Q3_D, 2),
			   matsmall_mul(Q3, matsmall_solve(W3, gel(W3_D, 2),
							   p, T),
					p, T),
			   p, T);
  gamma_1 = gmul(matsmall_det(matsmall_solve(M0, M1, p, T), p, T),
		 gamma_0);
  gamma = compute_gamma(X, D, c, W_E, s, W2, Q2, W3_D, Q3_D,
			gamma_0, gamma_1);
  delta = norm_of_section(X, v, W2, gel(W3_D, m + 1),
			  Q2, gel(Q3_D, m + 1));
  return gerepileupto(av, ginv(gmul(gel(gamma, m + 1), delta)));
}

static GEN
eval_function(GEN J, GEN c, GEN W_D, GEN u_D, GEN W_E, GEN u_E) {
  GEN D, s_D, v_D;
  GEN W0_E = curve_multiply_section_subspace(J, u_E, curve_V(J, 1));
  torsion_data(J, W_D, u_D, c, &D, &s_D, &v_D);
  return gdiv(eval_half(J, c, D, W_E, s_D, u_D, v_D),
	      eval_half(J, c, D, W0_E, s_D, u_D, v_D));
}

static long
divisors_disjoint(GEN J, GEN W_D, GEN W_E) {
  unsigned long p = curve_base_field_characteristic(J);
  GEN T = curve_base_field_polynomial(J);
  GEN W_F;
  W_D = curve_multiply_subspaces(J, W_D, curve_V(J, 1));
  W_E = curve_multiply_subspaces(J, W_E, curve_V(J, 1));
  W_F = matsmall_image(shallowconcat(W_D, W_E), p, T);
  return curve_divisor_degree(J, W_F, 3) == 0;
}

GEN
jacobian_weil_pairing(GEN J, GEN W_D, GEN W_E, long n) {
  unsigned long p = curve_base_field_characteristic(J);
  GEN T = curve_base_field_polynomial(J);
  GEN c, W, Z, u_D, u_E, f_E, g_D;
  pari_sp av = avma;

  if (jacobian_type(J) != JACOBIAN_TYPE_MEDIUM)
    pari_err_IMPL("jacobian_weil_pairing for non-medium models");

  c = addflip_chain(stoi(n));

  while (!divisors_disjoint(J, W_D, W_E))
    W_E = jacobian_random_presentation(J, W_E);

  do {
    u_D = gel(matsmall_random_subspace(curve_V(J, 1), 1, 1, p, T), 1);
    W = curve_multiply_section_subspace(J, u_D, curve_V(J, 1));
  } while (!divisors_disjoint(J, W, W_E));
  do {
    u_E = gel(matsmall_random_subspace(curve_V(J, 1), 1, 1, p, T), 1);
    Z = curve_multiply_section_subspace(J, u_E, curve_V(J, 1));
  } while (!divisors_disjoint(J, Z, W_D)
	   || !divisors_disjoint(J, W, Z));

  f_E = eval_function(J, c, W_D, u_D, W_E, u_E);
  g_D = eval_function(J, c, W_E, u_E, W_D, u_D);

  return gerepileupto(av, gdiv(f_E, g_D));
}
