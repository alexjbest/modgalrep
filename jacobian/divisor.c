/* -*- coding: utf-8 -*- */

/***

    Points and divisors

***/

#include <math.h>  /* log */
#include <pari/pari.h>

#include "algsmall.h"
#include "matsmall.h"

#include "curve.h"
#include "divisor.h"

/*
  This flag makes generation of random divisors faster,
  but makes the distribution possibly non-uniform.
*/
static const int FASTER = 1;

/*
  Given a curve  X  and a point  P  represented as a
  subspace of \Gamma(X,{\cal L}) of codimension 1, return
  the corresponding morphism \Gamma(X,{\cal L}) -> k.
*/
GEN
curve_divisor_to_point (GEN X, GEN P) {
  unsigned long p = curve_base_field_characteristic (X);
  GEN T = curve_base_field_polynomial (X);
  pari_sp av = avma;
  P = matsmall_solve (curve_V (X, 1), P, p, T);
  P = matsmall_coker (P, p, T);
  return gerepileupto (av, P);
}

/*
  Given a curve  X  and a point  P  represented as a
  morphism  \Gamma(X,{\cal L}) -> k, return the corresponding
  subspace of  \Gamma(X,{\cal L}) of codimension 1.
*/
GEN
curve_point_to_divisor (GEN X, GEN P) {
  unsigned long p = curve_base_field_characteristic (X);
  GEN T = curve_base_field_polynomial (X);
  pari_sp av = avma;
  P = matsmall_ker (P, p, T);
  P = matsmall_mul (curve_V (X, 1), P, p, T);
  return gerepileupto (av, P);
}

GEN
curve_point_multiples (GEN X, GEN P) {
  unsigned long i, r = curve_degree (X);
  pari_sp av = avma;
  GEN W_P = curve_point_to_divisor (X, P);
  GEN W_rP = cgetg (r + 1, t_VEC);

  gel (W_rP, 1) = curve_multiply_subspaces (X, curve_V(X, 1), W_P);
  for (i = 1; i < r; i++)
    gel (W_rP, i + 1) = curve_divide_subspaces(X, curve_multiply_subspaces(X, W_P, gel(W_rP, i)), 3,
					       curve_IGS_V(X, 1), 1);
  return gerepilecopy (av, W_rP);
}

/*
  Compute an ideal generating set for  \Gamma(X,{\cal L}^i(-D)),
  i.e. a basepoint-free subspace of small dimension.

  alpha  is Khuri-Makdisi's  eta,  a fixed real number between 0 and 1.
  A smaller  alpha  increases  h, but also the probability that a given
  random set of  h  sections is an IGS (this probability is at least
  1 - alpha).
*/
GEN
curve_divisor_IGS (GEN X, GEN W_D, unsigned long i) {
  unsigned long p = curve_base_field_characteristic (X);
  GEN T = curve_base_field_polynomial (X);
  long deg_D = lg (curve_V (X, i)) - lg (W_D),
    d = curve_degree (X),
    g = curve_genus (X),
    j = i + (2*g - 1 - deg_D + (d - 1))/d,  /* minimal with j*d >= 2*g - 1 + i*d - deg_D */
    expected_dim = 1 - g + ((i + j)*d - deg_D);
  float alpha = 0.9;
  int h = 1 + ceil (log ((i*d - deg_D) / alpha) / log_ffsize (p, T));
  GEN V = curve_V (X, j),  IGS, IGS_V;
  pari_sp av = avma;

  if (lg(W_D) < 4)
    return gcopy(W_D);
  do {
    avma = av;
    IGS = matsmall_random_subspace(W_D, h, 0, p, T);
    IGS_V = curve_multiply_subspaces(X, IGS, V);
  } while (lg (IGS_V) - 1 < expected_dim);
  cgiv (IGS_V);
  return IGS;
}

/*
  Given a section  s in \Gamma(X,{\cal L})  such that
  multiplication by  s  induces a map  V1 -> V2,  return
  the induced map on quotient spaces.
*/
GEN
curve_divisor_section_as_matrix (GEN X, GEN s, GEN V1, GEN V2,
				 GEN Q1, GEN Q2) {
  unsigned long p = curve_base_field_characteristic(X);
  GEN T = curve_base_field_polynomial(X);
  pari_sp av = avma;
  GEN s_V1 = curve_multiply_section_subspace(X, s, V1);
  GEN M = matsmall_solve(V2, s_V1, p, T);
  M = matsmall_solve_left(Q1, matsmall_mul(Q2, M, p, T), p, T);
  return gerepileupto(av, M);
}

/*
  Given an effective divisor  D  on  X, three subspaces
  V_1, V_2, V_3  of space of the form  \Gamma(X,{\cal L}^i)
  such that  V_3  is the product of  V_1 and V_2, and quotient
  maps to the corresponding spaces \Gamma(D,{\cal L}^i),
  return the bilinear map  Q_1 \times Q_2 \to Q_3.
*/
GEN
curve_divisor_bilinear_map (GEN X, GEN V1, GEN V2, GEN V3,
			    GEN Q1, GEN Q2, GEN Q3) {
  unsigned long p = curve_base_field_characteristic(X);
  GEN T = curve_base_field_polynomial(X);
  GEN sol, basis, mu;
  long i, n;
  pari_sp av = avma;

  if(lg(Q1) <= 1 || lg(Q2) <= 1 || lg(Q3) <= 1)
    pari_err_DIM("curve_divisor_bilinear_map");

  n = lg(gel(Q1, 1)) - 1;
  if(lg(gel(Q2, 1)) - 1 != n || lg(gel(Q3, 1)) - 1 != n)
    pari_err_DIM("curve_divisor_bilinear_map");

  /*
    Compute a set of vectors in  \Gamma(X,{\cal L})  mapping to the
    standard basis vectors of  \Gamma(D,i^*{\cal L}).
  */
  Q1 = matsmall_to_RgM(Q1, p, T);
  sol = RgM_invimage(Q1, matid(n));
  sol = RgM_to_matsmall(sol, p, T);
  basis = matsmall_mul(V1, sol, p, T);

  mu = cgetg(n + 1, t_VEC);
  for (i = 1; i <= n; i++)
    gel(mu, i) = curve_divisor_section_as_matrix(X, gel(basis, i),
						 V2, V3, Q2, Q3);
  return gerepileupto(av, mu);
}

/*
  Given a curve  X  and a divisor  D  represented as the
  _two_ subspaces  W_D = \Gamma(X,{\cal L}^d(-D))  and
  W_2_D = \Gamma(X,{\cal L}^{2d}(-D), this function
  returns the finite algebra  A = \Gamma(D, \O_D).
  The inequality
    d * deg{\cal L} - \deg D >= 2g - 1
  is assumed to hold.
  The result is returned as a pair [A, Q], where  Q  is
  the composition of the projection
    \Gamma(X,{\cal L}^d}) -> \Gamma(D, i^*{\cal L})
  with a  \Gamma(D, \O_D)-module isomorphism
    \Gamma(D, i^*{\cal L})) -> \Gamma(D, \O_D).
*/
GEN
curve_divisor_algebra (GEN X, GEN W_D, GEN W_2_D,
		       unsigned long d) {
  pari_sp av = avma;
  unsigned long p = curve_base_field_characteristic(X);
  GEN T = curve_base_field_polynomial(X);
  GEN V = curve_V(X, d);
  GEN V_2 = curve_V(X, 2 * d);
  GEN Q = matsmall_quotient(V, W_D, p, T);
  GEN Q_2 = matsmall_quotient(V_2, W_2_D, p, T);
  GEN algebra, basis;
  int try_monogenic = 1;  /* TODO: useful criterion */

  if (try_monogenic) {
    GEN s, t, M, N, A, v;
    long deg_D = lg(V) - lg(W_D), i;
    int done = 0;
    while (1) {
      s = gel(matsmall_random_subspace(V, 1, 1, p, T), 1);
      M = curve_divisor_section_as_matrix(X, s, V, V_2, Q, Q_2);
      if (matsmall_rank(M, p, T) == deg_D)
	break;
    }
    while (!done) {
      t = gel(matsmall_random_subspace(V, 1, 1, p, T), 1);
      N = curve_divisor_section_as_matrix(X, t, V, V_2, Q, Q_2);
      A = matsmall_solve(M, N, p, T);
      algebra = matsmall_charpoly(A, 0, p, T);
      while (1) {
	basis = cgetg(deg_D + 1, t_MAT);
	v = matsmall_random(deg_D, 1, p, T);
	gel(basis, 1) = gel(v, 1);
	for (i = 2; i <= deg_D; i++) {
	  v = matsmall_mul(A, v, p, T);
	  gel(basis, i) = gel(v, 1);
	}
	if (matsmall_rank(basis, p, T) == deg_D) {
	  done = 1;
	  break;
	}
      }
    }
  } else {
    GEN mu = curve_divisor_bilinear_map(X, V, V, V_2, Q, Q, Q_2);
    /*
      Compute the algebra  \Gamma(D,\O_D)  and its action
      on  \Gamma(D,i^*{\cal L}).
    */
    GEN algebra_basis = symmetric_bilinear_map_to_algsmall(mu, p, T);
    algebra = gel(algebra_basis, 1);
    basis = gel(algebra_basis, 2);
  }
  /* Compute the quotient in terms of the good basis.  */
  Q = matsmall_solve(basis, Q, p, T);
  return gerepilecopy(av, mkvec2(algebra, Q));
}

GEN
algebra_decompose(GEN algebra, unsigned long p, GEN T) {
  long i, j, n;
  GEN ideals, multiplicities;
  pari_sp av = avma;

  if (typ(algebra) == t_POL) {
    GEN fact = factor(algebra);
    n = nbrows(fact);
    ideals = cgetg(n + 1, t_VEC);
    for (i = 1; i <= n; i++) {
      GEN f = gmael(fact, 1, i);
      /* Compute the maximal ideal generated by  f.  */
      long dim = degpol(algebra) - degpol(f);
      GEN m = cgetg(dim + 1, t_MAT);
      for (j = 1; j <= dim; j++)
	gel(m, j) = RgX_to_RgC(RgX_shift_shallow(f, j - 1), degpol(algebra));
      m = RgM_to_matsmall(m, p, T);
      gel(ideals, i) = m;
    }
    multiplicities = gtovecsmall(gel(fact, 2));
  } else {
    GEN primary_decomposition = algsmall_primary_decomposition(algebra, p, T);
    long n = lg(primary_decomposition) - 1;
    GEN components, projections;

    components = cgetg(n + 1, t_VEC);
    for (i = 1; i <= n; i++)
      gel(components, i) = gmael(primary_decomposition, i, 1);
    projections = matsmall_direct_sum_projections(components, p, T);

    ideals = cgetg(n + 1, t_VEC);
    multiplicities = cgetg(n + 1, t_VECSMALL);

    for (i = 1; i <= n; i++) {
      GEN comp = gmael(primary_decomposition, i, 2);
      /* Compute the maximal ideal of the i-th component.  */
      GEN P_comp = algsmall_local_maximal_ideal(comp, p, T);
      /* Convert it to a maximal ideal of  A.  */
      GEN P = matsmall_inverse_image(gel(projections, i), P_comp, p, T);
      /*
	Compute the subspace of  F = V / W_D  corresponding to
	the prime ideal  P  of  A.  The action of  A  on  F  is given by
	standard matrix multiplication.  Because of the way we represent
	our algebras, this means that the subspace  P * F  can be
	represented by the same basis as  P  itself.
      */
      GEN F_P = P;
      /* Compute the subspace of  V  corresponding to the point  P.  */
      gel(ideals, i) = F_P;
      multiplicities[i] = (lg(comp) - 1) / (lg(comp) - lg(P_comp));
    }
  }
  return gerepilecopy(av, mkvec2(ideals, multiplicities));
}

/*
  Given a curve  X  and a divisor  D  represented as the
  _two_ subspaces  W_D = \Gamma(X,{\cal L}^d(-D))  and
  W_2_D = \Gamma(X,{\cal L}^{2d}(-D), this function
  returns the decomposition of  D  into prime divisors.
  The inequality
    d * deg{\cal L} - \deg D >= 2g - 1
  is assumed to hold.
  The result is returned as a pair consisting of a t_VEC
  of prime divisors and the corresponding t_VECSMALL of
  multiplicities.
*/
GEN
curve_decompose_divisor (GEN X, GEN W_D, GEN W_2_D,
			 unsigned long d) {
  unsigned long p = curve_base_field_characteristic (X);
  GEN T = curve_base_field_polynomial (X);
  GEN V = curve_V (X, d);
  long i, l;
  pari_sp av;
  GEN alg_Q, algebra, Q;
  GEN ideals_mult, ideals, points, multiplicities;

  if(lg(W_D) == lg(V)) {
    GEN r = cgetg(3, t_VEC);
    gel(r, 1) = cgetg(1, t_VEC);
    gel(r, 2) = cgetg(1, t_VECSMALL);
    return r;
  }

  av = avma;
  alg_Q = curve_divisor_algebra(X, W_D, W_2_D, d);
  algebra = gel(alg_Q, 1);
  Q = gel(alg_Q, 2);
  ideals_mult = algebra_decompose(algebra, p, T);
  ideals = gel(ideals_mult, 1);
  multiplicities = gel(ideals_mult, 2);

  points = cgetg_copy(ideals, &l);
  for (i = 1; i < l; i++) {
    GEN tmp = matsmall_inverse_image(Q, gel(ideals, i), p, T);
    gel(points, i) = matsmall_mul(V, tmp, p, T);
  }
  return gerepilecopy(av, mkvec2(points, multiplicities));
}

/*
  Generate a uniformly random prime divisor of degree  n  on  X,
  represented as a subspace of  \Gamma(X,{\cal L}^d).
*/
GEN
curve_random_prime_divisor (GEN X, unsigned long n, unsigned long d) {
  unsigned long p = curve_base_field_characteristic (X);
  GEN T = curve_base_field_polynomial (X);
  pari_sp av = avma;
  GEN P = NULL;

  /*
    We use hypersurfaces of degree  i, where  i  is the least
    positive integer such that  i * \deg{\cal L} - n >= 2g - 1.
  */
  int i = ceil (((float) (n + 2 * curve_genus (X) + 1))
		/ ((float) curve_degree (X)));

  printf ("finding prime divisor of degree %li using hypersurface of degree %i\n", n, i);
  if (curve_max_V (X) < 2 * i + 2)
    pari_err (e_MISC, "divisor of too large degree requested");

  if (d < i)
    pari_err (e_MISC, "a line bundle of higher degree is required to represent this divisor");

  do {
    GEN s, W_D, W_2_D, decomposition, prime_divisors, points;
    int j, n_points = 0;
    avma = av;
    
    /* Generate a random hypersurface section  D  of degree  i.  */
    s = gel(matsmall_random_subspace(curve_V(X, i), 1, 1, p, T), 1);
    /*
      Compute the subspaces  W_D  and  W_2_D  of
      V_{i + 1}  and  V_{2i + 2}, respectively.
    */
    W_D = curve_multiply_section_subspace(X, s, curve_V(X, 1));
    W_2_D = curve_multiply_section_subspace(X, s, curve_V(X, i + 2));

    /* Decompose the hyperplane section  D.  */
    decomposition = curve_decompose_divisor (X, W_D, W_2_D, i + 1);
    prime_divisors = gel (decomposition, 1);
    points = zerovec (lg (prime_divisors));

    /* Find the set of prime divisors of degree  n.  */
    for (j = 1; j < lg (prime_divisors); j++) {
      if (lg (curve_V (X, i + 1))
	  - lg (gel (prime_divisors, j)) == n)
	gel (points, ++n_points) = gel (prime_divisors, j);
    }

    if (FASTER) {
      /* faster, but not guaranteed uniform */
      if (n_points > 0)
	P = gel (points, random_Fl (n_points) + 1);
    } else {
      int r = random_Fl (i * (curve_degree (X) / n));
      if (r < n_points)
	P = gel (points, r + 1);
    }
  } while (P == NULL);

  if (d < i + 1) {
    int a = i + 1 - d;
    GEN IGS = (a <= curve_max_IGS_V (X)) ? curve_IGS_V (X, a)
      : curve_V (X, a);
    P = curve_divide_subspaces(X, P, i + 1, IGS, a);
  } else if (d > i + 1) {
    int a = d - i - 1;
    GEN IGS = (a <= curve_max_IGS_V (X)) ? curve_IGS_V (X, a)
      : curve_V (X, a);
    P = curve_multiply_subspaces(X, IGS, P);
  }

  if (lg (curve_V (X, d)) - lg (P) != n)
    pari_err (e_MISC, "bug in curve_random_prime_divisor");
  return gerepileupto (av, P);
}

/*
  Return a uniformly random point on  X, represented by a
  morphism  \Gamma(X,{\cal L}) -> k.
*/
GEN
curve_random_point (GEN X) {
  GEN P = curve_random_prime_divisor (X, 1, 1);
  return curve_divisor_to_point (X, P);
}


/*
  The following algorithms to find a random divisor are
  taken from Claus Diem's Habilitationsschrift, page 150.
*/

/*
  Return the number of prime divisors of each degree  <= d
  on the curve X.
*/
static GEN
num_prime_divisors (GEN X, int d) {
  GEN q, L, power_sums, B;
  int m, n;
  pari_sp av = avma;

  if (d == 0)
    return cgetg (1, t_VEC);
  q = curve_base_field_cardinality (X);
  L = curve_numerator_zeta_function (X);
  /* Newton's identity:  -L'/L = \sum_{m\ge 1} s_m t^{m-1}.  */
  power_sums = gadd (gneg (gdiv (derivpol (L), L)),
		     zeroser (varn (L), d + 1));
  /* Compute the number of prime divisors of each degree.  */
  B = cgetg (d + 1, t_VEC);
  gel (B, 1) = subii (addsi (1, q), truecoeff (power_sums, 0));
  for (n = 2; n <= d; n++) {
    gel (B, n) = gen_0;
    for (m = 1; m <= n; m++)
      if (n % m == 0)
	gel (B, n) = addii (gel (B, n),
			    mulsi (moebius (stoi (n / m)),
				   subii (gpowgs (q, m),
					  truecoeff (power_sums,
						     m - 1))));
    gel (B, n) = diviiexact (gel (B, n), stoi (n));
  }
  return gerepileupto (av, B);
}

/*
  Return the power series  (1 - t^m)^B + O(t^{n+1}) =
  sum_{k=0}^{floor(n/m)} binomial(B, k) (-t^m)^k + O(t^{n+1}).
*/
static GEN
aux_series (GEN B, int m, int n) {
  pari_sp av = avma;
  GEN y = gneg(gpowgs(pol_x(MAXVARN), m));
  GEN ser = zeroser(MAXVARN, n + 1);
  int k;
  for (k = 0; k <= n/m; k++)
    ser = gadd(ser, gmul(binomial(B, k), gpowgs(y, k)));
  return gerepileupto(av, ser);
}

/*
  Given the number of prime divisors of each degree, this function
  returns the decomposition type of a uniformly random effective
  divisor of degree  n  such that all the prime divisors in its
  support have degree at most  m.

  TODO: return NULL (instead of crashing) if no such divisor exists.
*/
static GEN
random_smooth_decomposition_type (GEN B, int n, int m) {
  GEN Psi_lt, Psi_le, b, x;
  int l;
  pari_sp av = avma;

  if (m == 1)
    return mkvecsmall (n);

  /*
    Compute the power series  Psi_lt = \prod_{l<m}(1 - t^l)^{-B_l}  and
    Psi_le = \prod_{l\le m}(1 - t^l)^{-B_l}  up to  O(t^{n+1}).
  */
  Psi_lt = gen_1;
  for (l = 1; l <= m - 1; l++)
    Psi_lt = gmul(Psi_lt, aux_series(negi(gel(B, l)), l, n));
  Psi_le = gmul(Psi_lt, aux_series(negi(gel(B, m)), m, n));

  /*
    In the following lines we pick a number  l  indicating how many prime
    divisors of degree  m  we take in our random divisor.
  */
  for (l = 0, x = randomi (truecoeff (Psi_le, n)), b = truecoeff (Psi_lt, n);
       gcmp (x, b) >= 0;
       l++, b = addii (b, mulii (binomial (addis (gel (B, m), l - 1), l),
				 truecoeff (Psi_lt, n - l * m))));
  return gerepileupto (av, vecsmall_concat (random_smooth_decomposition_type (B, n - l * m, m - 1),
					    mkvecsmall (l)));
}

/*
  Generate a uniformly random  m-smooth divisor of degree  n
  on  X, represented as a subspace of  \Gamma(X,{\cal L}^d).
*/
static GEN
curve_random_smooth_divisor (GEN X, unsigned long n,
			     unsigned long m, unsigned long d) {
  pari_sp av = avma;
  GEN V = curve_V (X, d);
  GEN IGS = (d <= curve_max_IGS_V (X)) ? curve_IGS_V (X, d)
    : curve_V (X, d);
  GEN B, type;
  /* Start with the zero divisor.  */
  GEN D = V;
  unsigned long dim = lg (curve_V (X, 2*d)) - 1;
  int e;

  printf("looking for random %li-smooth divisor of degree %li\n",
	 m, n);
  if (2 * d + 2 > curve_max_V (X)
      || d * curve_degree (X) - n < 2 * curve_genus (X))
    pari_err (e_MISC, "divisor of too large degree requested");

  B = num_prime_divisors (X, m);
  type = random_smooth_decomposition_type (B, n, m);
  pari_printf("looking for decomposition type %Ps\n", type);

  for (e = 1; e <= m; e++) {
    /* TODO: the following doesn't quite give a uniform result.  */  
    for (; type[e]; type[e]--) {
      GEN P = curve_random_prime_divisor (X, e, d);
      dim -= e;
      D = curve_multiply_subspaces_dim(X, D, P, dim);
      D = curve_divide_subspaces(X, D, 2*d, IGS, d);
    }
  }

  /* Check whether the subspace has the right codimension.  */
  if (lg (V) - lg (D) != n)
    pari_err (e_MISC, "bug in curve_random_divisor_smooth");
  return gerepileupto (av, D);
}


/*
  Return a random divisor of degree  n  on  X, represented as a
  subspace of  \Gamma(X,{\cal L}^d).

  The distribution is not (guaranteed to be) uniformly random.
  Instead, we take uniformly random hypersurface sections of degree d,
  decompose these, and keep going until the supply of prime divisors
  allows us to construct a divisor of degree n.

  TODO: this is actually not always faster!
*/
GEN
curve_random_divisor_fast (GEN X, unsigned long n, unsigned long d) {
  unsigned long p = curve_base_field_characteristic (X);
  GEN T = curve_base_field_polynomial (X);
  GEN V = curve_V (X, d);
  GEN IGS_V = (d <= curve_max_IGS_V (X)) ? curve_IGS_V (X, d) : V;
  pari_sp av = avma;
  int dX = curve_degree (X), dim = lg (curve_V (X, 2*d)) - 1;
  GEN P, type, D = V;
  int i, j, first_iteration = 1;

  /*
    We use the line bundle {\cal L}^i, where  i  is the least
    positive integer such that  i * \deg{\cal L} >= n.
  */
  i = ceil (((float) n) / ((float) curve_degree (X)));

  printf ("finding prime divisor of degree %li using hypersurface of degree %i\n", n, i);
  if (curve_max_V (X) < 2 * i + 2)
    pari_err (e_MISC, "divisor of too large degree requested");

  if (d <= i)
    pari_err (e_MISC, "a line bundle of higher degree is required to represent this divisor");

  P = cgetg (i * dX + 1, t_VEC);
  for (j = 1; j <= i * dX; j++)
    gel (P, j) = cgetg (1, t_VEC);

  while (1) {
    /* Generate a random hypersurface section  H  of degree  i.  */
    GEN s = gel(matsmall_random_subspace(curve_V(X, i), 1, 1, p, T), 1);
    /*
      Compute the subspaces  W_H  and  W_2_H  of
      V_{d + 1}  and  V_{2d + 2}, respectively.
    */
    GEN W_H = curve_multiply_section_subspace(X, s, curve_V(X, 1));
    GEN W_2_H = curve_multiply_section_subspace(X, s, curve_V(X, i + 2));
    /* Decompose the hyperplane section  H.  */
    GEN decomposition = curve_decompose_divisor (X, W_H, W_2_H, i + 1);
    GEN prime_divisors = gel (decomposition, 1);
    GEN B;

    /* Add prime divisors to the list.  */
    for (j = 1; j < lg (prime_divisors); j++) {
      GEN Q = gel (prime_divisors, j);
      int dQ = lg (curve_V (X, i + 1)) - lg (Q);
      gel (P, dQ) = shallowconcat (gel (P, dQ), mkvec (Q));
    }
    if (first_iteration) {
      first_iteration = 0;
      continue;
    }
    /* B = [length(P[j]) | j <- [1..i*dX]] */
    B = cgetg (i * dX + 1, t_VEC);
    for (j = 1; j <= i * dX; j++)
      gel (B, j) = stoi (lg (gel (P, j)) - 1);
    pari_printf("B = %Ps\n", B);
    type = random_smooth_decomposition_type (B, n, n);
    if (type != NULL)
      break;
  }
  pari_printf("looking for decomposition type %Ps\n", type);

  for (j = 1; j <= n; j++) {
    for (; type[j]; type[j]--) {
      int l = lg (gel (P, j)) - 1;
      GEN Q = gmael (P, j, random_Fl (l) + 1);
      dim -= j;
      D = curve_multiply_subspaces_dim(X, D, Q, dim);
      D = curve_divide_subspaces(X, D, 2*d, IGS_V, d);
    }
  }

  if (d > i + 1) {
    int a = d - i - 1;
    GEN IGS = (a <= curve_max_IGS_V (X)) ? curve_IGS_V (X, a)
      : curve_V (X, a);
    D = curve_multiply_subspaces(X, IGS, D);
  }

  if (lg (curve_V (X, d)) - lg (D) != n)
    pari_err (e_MISC, "bug in curve_random_divisor_fast");
  return gerepileupto (av, D);
}


/*
  Return a random divisor of degree  n  on  X, represented as a
  subspace of  \Gamma(X,{\cal L}^d).
*/
GEN
curve_random_divisor (GEN X, unsigned long n, unsigned long d) {
  if (FASTER) {
    /*
      To make the computation faster, we only use prime divisors
      of degree small enough so that we can find the prime divisors
      by intersecting  X  with a hyperplane (or in the worst case
      a quadratic hypersurface).
    */
    int m = curve_degree (X) - 2 * curve_genus (X) - 1;
    if (m == 0)
      m = 2 * curve_degree (X) - 2 * curve_genus (X) - 1;
    return curve_random_smooth_divisor (X, n, m, d);
  } else  {
    return curve_random_smooth_divisor (X, n, n, d);
  }
}

/* Apply the (p^m)-power Frobenius map to the entries of  V.  */
static GEN
FlxqM_Frob(GEN V, unsigned long p, GEN T, unsigned long m) {
  GEN W;
  int i, j;

  W = cgetg (lg (V), t_MAT);
  for (i = 1; i < lg (V); i++) {
    gel (W, i) = cgetg (lg (gel (V, i)), t_COL);
    for (j = 1; j < lg (gel (V, i)); j++)
      gmael(W, i, j) = Flxq_pow(gmael(V, i, j), powuu(p, m), T, p);
  }
  return W;
}

static GEN
matsmall_Frob(GEN V, unsigned long p, GEN T, unsigned long m) {
  if (T == NULL)
    /* Frobenius is the identity on the prime field.  */
    return gcopy (V);
  else
    return FlxqM_Frob(V, p, T, m);
}

/* Apply the (p^m)-power Frobenius map to the divisor  D.  */
GEN
curve_divisor_Frob(GEN X, GEN W_D, unsigned long m) {
  unsigned long p = curve_base_field_characteristic(X);
  GEN T = curve_base_field_polynomial(X);
  return matsmall_Frob(W_D, p, T, m);
}
