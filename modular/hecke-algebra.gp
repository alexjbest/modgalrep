\r cusp-forms

/*
  Given a set of generators for a matrix algebra A as a \Z-module,
  this algorithm returns [M, B], where M is the multiplication table
  of A with respect to some \Z-basis of A and where B is the matrix
  expressing the given generators of A on the same \Z-basis.
  For efficiency, this algorithm also takes the rank of A as input.
*/

alg_find_basis (A, d) = {
  local (n, e, v, Av, AAv, Bv, denom, denom2, U, M);
  n = length (A);
  e = 0;
  while (1,
         e++;
         /* Generate a random column vector with entries < 2^e.  */
         v = vector (d, i, random (2^e))~;
         print ("using random vector ", v);
         Av = Mat (vector (n, i, A[i] * v));
         if (matrank (Av) == d, break));
  print ("computing AAv");
  AAv = matrix (n, n, i, j, A[i] * (A[j] * v));
  /* Clear denominators to apply LLL with integral coefficients.  */
  print ("LLL");
  denom = denominator (Av);
  Av *= denom;
  U = qflll (Av, 1);
  /* Compute the action of the new basis vectors on  v.  */
  Bv = Av * U;
  denom2 = denominator (AAv);
  AAv *= denom2;
  print ("computing AAvU");
  AAvU = matrix (n, d, i, q, sum (j = 1, n, AAv[i, j] * U[j, q]));
  print ("computing M");
  M = vector (d, p, print (p); matsolve (Bv, Mat (vector (d, q,
              sum (i = 1, n, U[i, p] * AAvU[i, q])))));
  M = M * (denom / denom2);
  return (M);
\\  return ([M, matsolve (Bv, Av)]);
}

alg_mult(A, x, y) = {
  return (sum (i = 1, length (A), A[i] * x[i]) * y);
}

/*
  Compute the Hecke algebra on the space of cusp forms of the
  given weight for the given group.  Assumes the space is non-zero.
*/
hecke_algebra(group, weight = 2, bound) = {
  local (bound0, S, dim, alg, diam, T, M, denom, U, MU_vec, MU_mat,
         i, d, gen, ngen, V);
  /*
    Compute a  \Z-basis for the Hecke algebra.
    We know the Hecke algebra is generated as a  \Z-module
    by  T_1, \dots, T_m, where  m  is the degree of the line
    bundle of cusp forms plus one.
  */
  bound0 = max (0, modular_group_cusp_forms_degree (group, weight) + 1);
  bound = max (bound, bound0);
  S = cusp_forms (group, weight, 0);
  dim = cusp_forms_dimension (S);
  alg = cusp_forms_hecke_algebra (S, bound);
  diam = alg[1];
  T = alg[2];
  print ("hecke_algebra: finding LLL-reduced basis");
  result = alg_find_basis (vector (bound0, i, T[i]), dim);
  return (result);
/*
  diam = vector (level, d, matrix (dim, dim));
  for (i = 1, ngen,
       d = gen[i][2];
       print1 (" ", d);
       diam[d + 1] = denom * matsolve (MU_vec,
                                       matrix_to_vector (diam[d + 1], dim)));
  diam = denom * matsolve (MU_vec, matrices_to_vectors (diam, dim));
  print ("hecke_algebra: expressing Hecke operators on the new basis");
  T = denom * matsolve (MU_vec, matrices_to_vectors (T, dim));
  return ([V, diam, T]);
*/
}

/* A handy shortcut.  */
T_1(n) = hecke_algebra (Gamma_1(n), 2);

algebra_discriminant(A) = {
  local (d = length (A),
	 M = matrix (d, d));
  print ("computing matrix of the trace form");
  for (i = 1, d, for (j = i, d,
       M[i, j] = M[j, i] = sum (k = 1, d, sum (l = 1, d,
                                A[i][k, l] * A[j][l, k]))));
  print ("computing discriminant");
  return (matdet (M));
}

hecke_info(n, k = 2) = {
  local(T = hecke_algebra (Gamma_1(n), k),
	d = length(T),
	D = algebra_discriminant(T));
  print ("D = ", D);
  print ("n = ", n, ", rank = ", d, ", log|D| = ", log (abs(D)), ", factorisation(D) = ");
  factorint(D);
}
