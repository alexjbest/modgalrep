#ifndef L_TORSION
#define L_TORSION

GEN jacobian_l_torsion_relations(GEN J, unsigned long l, GEN P);
GEN jacobian_l_torsion_basis(GEN J, unsigned long l, GEN multiples_O);
GEN jacobian_l_torsion_Frob_matrix(GEN J, unsigned long l,
				   GEN basis, unsigned long m);

#endif
