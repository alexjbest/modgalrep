#include "config.h"

#include <argp.h>
#include <fcntl.h>     /* creat */
#include <stdio.h>
#include <stdlib.h>    /* strtoul */
#include <string.h>    /* strlen */
#include <sys/file.h>  /* flock */

#include <pari/pari.h>

#include "local-representation.h"
#include "modular-symbols.h"

const char *argp_program_version = PACKAGE_STRING;
const char *argp_program_bug_address = PACKAGE_BUGREPORT;

static char doc[] = "Compute modular Galois representations.";

static char args_doc[] = "FORM_FILE P OUTPUT_FILE";

static struct argp_option options[] = {
  { "max-degree", 'd', "D", 0,
    "Raise an error if a finite field of degree > D"
    " over its prime field is needed" },
  { "path", 'p', "PATH", 0,
    "Set search path for GP files to PATH" },
  { "tries", 't', "N", 0,
    "Generate at most N random points to find a basis" },
  { NULL }
};

struct arguments {
  char *form_file;
  char *output_file;
  char *path;
  unsigned long p;
  unsigned long max_degree;
  unsigned long tries;
};

static error_t
parser(int key, char *arg, struct argp_state *state) {
  struct arguments *arguments = state->input;
  switch (key) {
  case 'd':
    arguments->max_degree = strtoul(arg, NULL, 0);
    break;
  case 'p':
    arguments->path = arg;
    break;
  case 't':
    arguments->tries = strtoul(arg, NULL, 0);
    break;
  case ARGP_KEY_ARG:
    switch (state->arg_num) {
    case 0:
      arguments->form_file = arg;
      break;
    case 1:
      arguments->p = strtoul(arg, NULL, 0);
      break;
    case 2:
      arguments->output_file = arg;
      break;
    default:
      argp_usage(state);
    }
    break;
  case ARGP_KEY_END:
    if (state->arg_num < 3)
      argp_usage(state);
    break;
  default:
    return ARGP_ERR_UNKNOWN;
  }
  return 0;
}

static struct argp argp = { options, parser, args_doc, doc };

int
main(int argc, char* argv[]) {
  struct arguments arguments = { NULL, NULL, NULL, 0, 0, 0 };
  GEN form, modsym, subspace, result;
  FILE *file;
  int lockfile;
  int i;

  argp_parse(&argp, argc, argv, 0, 0, &arguments);

  pari_init(1 << 20, 500000);
  paristack_setsize((size_t) 1 << 24, (size_t) 1 << 34);

  if (arguments.path != NULL)
    setdefault("path", arguments.path, d_SILENT);

  form = gp_readvec_file(arguments.form_file);
  modsym = gel(form, 1);
  subspace = gel(form, 2);

  /*
    For a Galois representation with image GL_2(F_l), we expect
    roughly half of all Frobenius elements to have order <= l - 1.
  */
  if (arguments.max_degree == 0)
    arguments.max_degree = modular_symbols_characteristic(modsym) - 1;

  if (arguments.tries == 0)
    arguments.tries = modular_symbols_characteristic(modsym);

  pari_CATCH(e_MISC) {
    GEN err = pari_err_last();
    char *s = pari_err2str(err);
    s[strlen(s) - 1] = '\0';  /* remove trailing '.' */
    lockfile = creat("/var/lock/modgalrep.lock", 0600);
    if (flock(lockfile, LOCK_EX))
      perror("cannot obtain lock");
    else {
      file = fopen("bad_primes.gp", "a");
      fprintf(file, "%-6li\\\\ %s\n", arguments.p, s);
      fclose(file);
    }
    close(lockfile);
    result = NULL;  /* silence compiler warning */
    pari_err(0, err);
  } pari_TRY {
    result = local_representation(modsym, subspace, arguments.p,
				  arguments.max_degree, arguments.tries);
  } pari_ENDCATCH;

  file = fopen(arguments.output_file, "w");
  for (i = 1; i < lg(result); i++)
    pari_fprintf(file, "%Ps\n", gel(result, i));
  fclose(file);

  pari_close();
  return 0;
}
