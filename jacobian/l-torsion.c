#include <pari/pari.h>
#include <math.h>  /* ceil, log */

#include "curve.h"
#include "finite-fields.h"
#include "jacobian.h"
#include "l-torsion.h"
#include "linear-algebra.h"
#include "pairings.h"


/*
  Find a basis for the space of linear relations between the
  l-torsion points  P_1, ..., P_n  of  J
  (thesis, Algorithm IV.3.11).
*/
GEN
jacobian_l_torsion_relations(GEN J, unsigned long l, GEN P) {
  unsigned long m, n = lg(P) - 1, i, j;
  unsigned long p = curve_base_field_characteristic(J);
  GEN T = curve_base_field_polynomial(J);
  GEN ll = stoi(l), pp = stoi(p);
  GEN roots, zeta, order, Q, M, K, lincomb;
  const double alpha = 0.5;
  int done;

  roots = FqX_roots(polcyclo(l, 0), Flx_to_ZX(T), pp);
  if (lg(roots) <= 1)
    pari_err(e_MISC, "curve does not contain a primitive l-th root of unity");
  zeta = gel(roots, 1);
  if (typ(zeta) == t_INT)
    zeta = Fl_to_Flx(itos(zeta), T[1]);
  else
    zeta = ZX_to_Flx(zeta, p);

  if (T == NULL)
    order = stoi(Fl_order(zeta[2], p - 1, p));
  else
    order = Flxq_order(zeta, factor_pn_1(pp, degpol(T)), T, p);
  if (!gequalsg(l, order))
    pari_err(e_MISC, "wrong order: %Ps instead of %li\n", order, l);

  m = (n == 0) ? 0 : n - 1 + ceil(-log(1 - pow(alpha, 1./n))/log(l));

  done = 0;
  while (!done) {
    Q = cgetg(m + 1, t_VEC);
    for (i = 1; i <= m; i++)
      gel(Q, i) = jacobian_random_point(J);
    M = cgetg(n + 1, t_MAT);
    for (j = 1; j <= n; j++) {
      gel(M, j) = cgetg(m + 1, t_VEC);
      for (i = 1; i <= m; i++) {
	GEN tp = jacobian_tate_pairing(J, gel(P, j), gel(Q, i), l);
	if (!gequal1(gpowgs(tp, l)))
	  pari_err(e_MISC, "%Ps does not have order %l\n", tp, l);
	gcoeff(M, i, j) = gmodulo(Flxq_log(ZX_to_Flx(liftall(tp), p),
					   zeta, ll, T, p),
				  ll);
      }
    }
    K = ker(M);
    done = 1;
    for (i = 1; i < lg(K); i++) {
      lincomb = jacobian_linear_combination(J, P, liftint(gel(K, i)));
      if (!jacobian_is_zero(J, lincomb))
	done = 0;
    }
  }
  return K;
}

/*
  Compute the matrix of the (p^m)-power Frobenius map on a
  Frobenius-stable  l-torsion subspace with respect to the
  given basis.
*/
GEN
jacobian_l_torsion_Frob_matrix(GEN J, unsigned long l, GEN basis, unsigned long m) {
  pari_sp av = avma;
  long b, i;
  GEN Frob_basis, minus_Frob_matrix, generators, relations;

  Frob_basis = cgetg_copy(basis, &b);
  for (i = 1; i < b; i++)
    gel(Frob_basis, i) = jacobian_Frob(J, gel(basis, i), m);
  generators = shallowconcat(basis, Frob_basis);
  relations = jacobian_l_torsion_relations(J, l, generators);
  /* compute [-Frob_matrix; id] */
  minus_Frob_matrix = gel(reduced_column_echelon_form(relations, NULL, 1), 2);
  for (i = 1; i < b; i++)
    setlg(gel(minus_Frob_matrix, i), b);
  return gerepileupto(av, gneg(minus_Frob_matrix));
}

/* Return the smallest  k  such that  L^k * P == 0.  */
static unsigned long
order_l_power(GEN J, GEN P, GEN L) {
  unsigned long k = 0;
  while (!jacobian_is_zero(J, P)) {
    P = jacobian_multiply(J, P, L);
    k++;
  }
  return k;
}

/*
  Compute a basis for the  l-torsion of the Picard group
  (thesis, Algorithm IV.3.12).
*/
GEN
jacobian_l_torsion_basis(GEN J, unsigned long l, GEN multiples_O) {
  unsigned long p = curve_base_field_characteristic(J);
  GEN T = curve_base_field_polynomial(J);
  unsigned long base_field_degree = curve_base_field_degree(J);
  GEN extension, J_a, generators, relations, basis, Q;
  GEN Frob_matrix, rational_basis, rational_points;
  GEN L, chi, chibar, t_minus_1, f1, g1, n_a, m_a, fg, f, g;
  GEN idempotent, kummer_polynomial;
  long v, a, b, c_a, precision, r, i, dim;
  const double alpha = 0.5;
  pari_sp av = avma;

  L = utoi(l);
  chi = polrecip(curve_numerator_zeta_function(J));
  pari_printf("characteristic polynomial of Frobenius = %Ps\n", chi);
  v = gvar(chi);
  chibar = gmodulo(chi, L);
  t_minus_1 = gmodulo(gsub(pol_x(v), gen_1), L);
  b = gvaluation(chibar, t_minus_1);
  printf("generalised 1-eigenspace of Frob_{%li^%li} on J[%li] has dimension %li\n",
	 p, base_field_degree, l, b);
  f1 = gpowgs(t_minus_1, b);
  g1 = gdiv(chibar, f1);
  for (a = 1; a < b; a *= l) ;
  printf("using auxiliary extension of degree %li\n", a);

  extension = finite_field_extension(a, p, T);
  J_a = jacobian_base_change(J, extension);
  n_a = jacobian_count_points(J_a);
  c_a = gvaluation(n_a, L);
  m_a = gdiv(n_a, gpowgs(L, c_a));

  precision = c_a + 2;
  fg = polhensellift(chi, liftint(mkvec2(f1, g1)), L, precision);
  f = gadd(gel(fg, 1), zeropadic(L, precision));
  g = gadd(gel(fg, 2), zeropadic(L, precision));
  idempotent = gmod(gmul(g, gel(gcdext0(f, g), 2)), chi);
  kummer_polynomial = gdiv(gmod(gsub(gpowgs(pol_x(v), a), gen_1), f), L);
  pari_printf("idempotent = %Ps\nKummer polynomial = %Ps\n",
	      idempotent, kummer_polynomial);
  r = (b == 0) ? 0 : b - 1 + ceil(-log(1 - pow(alpha, 1./b))/log(l));

  while (1) {
    generators = cgetg(r + 1, t_VEC);
    for (i = 1; i <= r; i++) {
      GEN idemp_red, kummer_red;
      long o;
      Q = jacobian_random_point(J_a);
      printf("projecting onto %li-torsion\n", l);
      pari_printf("m_a = %Ps\n", m_a);
      Q = jacobian_multiply(J_a, Q, m_a);
      o = order_l_power(J_a, Q, L);
      idemp_red = centerlift(gmodulo(idempotent, gpowgs(L, o)));
      pari_printf("idempotent (reduced) = %Ps\n", idemp_red);
      o = order_l_power(J_a, Q, L);
      kummer_red = centerlift(gmodulo(kummer_polynomial, gpowgs(L, o)));
      pari_printf("Kummer polynomial (reduced) = %Ps\n", kummer_red);
      Q = jacobian_Frob_polynomial(J_a, Q, idemp_red, base_field_degree);
      Q = jacobian_Frob_polynomial(J_a, Q, kummer_red, base_field_degree);
      if (jacobian_is_zero(J_a, Q))
	pari_warn(warner, "point is zero");
      if (!jacobian_is_zero(J_a, jacobian_multiply(J_a, Q, L)))
	pari_err(e_MISC, "image under the Kummer map is not %li-torsion", l);
      gel(generators, i) = Q;
    }
    printf("computing relation matrix\n");
    relations = jacobian_l_torsion_relations(J_a, l, generators);
    pari_printf("relation matrix = %Ps\n", relations);
    if (lg(relations) - 1 == r - b)
      break;
  }
  if (r == b)
    basis = generators;
  else
    basis = shallowextract(generators, imagecompl(gtrans(relations)));

  printf("computing Frobenius matrix\n");
  Frob_matrix = jacobian_l_torsion_Frob_matrix(J_a, l, basis, base_field_degree);
  pari_printf("Frobenius matrix = %Ps\n", Frob_matrix);
  rational_basis = liftint(ker(gsub(matid(b), Frob_matrix)));
  dim = lg(rational_basis) - 1;
  rational_points = cgetg(dim + 1, t_VEC);
  for (i = 1; i <= dim; i++) {
    Q = jacobian_linear_combination(J_a, basis, gel(rational_basis, i));
    if (!jacobian_is_zero(J_a, jacobian_multiply(J_a, Q, L)))
      pari_err(e_MISC, "point is not annihilated by %li", l);
    gel(rational_points, i) = jacobian_descend(J_a, Q, extension,
					       multiples_O);
    if (!jacobian_is_zero(J, jacobian_multiply(J, gel(rational_points, i), L)))
      pari_err(e_MISC, "descended point is not annihilated by %li", l);
  }
  return gerepilecopy(av, rational_points);
}
