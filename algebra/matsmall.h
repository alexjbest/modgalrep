GEN matsmall_id (long n, unsigned long p, GEN T);
GEN matsmall_zero (long m, long n, unsigned long p, GEN T);
GEN matsmall_transpose (GEN A, unsigned long p, GEN T);
GEN matsmall_mul (GEN A, GEN B, unsigned long p, GEN T);
GEN matsmall_col_mul (GEN A, GEN v, unsigned long p, GEN T);
GEN matsmall_inv (GEN A, unsigned long p, GEN T);
GEN matsmall_pow(GEN A, long n, unsigned long p, GEN T);
GEN matsmall_ker (GEN A, unsigned long p, GEN T);
GEN matsmall_image (GEN A, unsigned long p, GEN T);
GEN matsmall_coker (GEN V, unsigned long p, GEN T);
GEN matsmall_solve (GEN A, GEN B, unsigned long p, GEN T);
GEN matsmall_solve_left (GEN A, GEN B, unsigned long p, GEN T);
GEN matsmall_inverse_image (GEN A, GEN V, unsigned long p, GEN T);
GEN matsmall_quotient(GEN V, GEN W, unsigned long p, GEN T);
GEN matsmall_random (unsigned long rows, unsigned long cols,
		     unsigned long p, GEN T);
GEN matsmall_random_subspace(GEN V, unsigned long h, int check,
			     unsigned long p, GEN T);
long matsmall_rank (GEN A, unsigned long p, GEN T);
GEN matsmall_indexrank(GEN A, unsigned long p, GEN T);
GEN matsmall_echelon(GEN A, GEN *Rp, ulong p, GEN T);

/* conversion functions */
GEN matsmall_to_RgM(GEN A, unsigned long p, GEN T);
GEN RgM_to_matsmall(GEN A, unsigned long p, GEN T);

/* convenience functions */
GEN matsmall_charpoly (GEN A, long v, unsigned long p, GEN T);
GEN matsmall_det (GEN A, unsigned long p, GEN T);
GEN matsmall_linear_combination (GEN A, GEN v, unsigned long p, GEN T);
GEN matsmall_direct_sum_projections (GEN spaces, unsigned long p, GEN T);
