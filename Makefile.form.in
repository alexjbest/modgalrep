### -*- Makefile -*-
### Defining polynomials of Galois representations

# Configuration variables

PRIMES ?= 0
GP ?= gp -q
MODGALREP ?= @bindir@/modgalrep -p .:@pkgdatadir@
ifdef MAX_DEGREE
MODGALREP := $(MODGALREP) -d $(MAX_DEGREE)
endif
ifdef TRIES
MODGALREP := $(MODGALREP) -t $(TRIES)
endif
ifdef MODFORMS
export MODFORMS
endif

# Evaluate $(1) in GP, redirecting standard output to $(2).
gp_eval_raw = echo $(1) | $(GP) @pkgdatadir@/functions.gp > $(2)
# Evaluate $(1) in GP, catching errors and writing the result to $(2).
gp_eval = $(call gp_eval_raw,"iferr(" $(1) ",e,write(\"/dev/stderr\",e);quit(1))",$(2))
# Evaluate $(1) in GP, catching errors and writing the elements of
# the result (which must be a vector) to $(2), separated by newlines.
gp_eval_vec = $(call gp_eval_raw,"iferr(apply(x->print(x),if(1," $(1) ")),e,write(\"/dev/stderr\",e);quit(1));",$(2))

all: projective_field.gp dual_pair.gp reduced_dual_pair.gp info.txt

.DELETE_ON_ERROR:

form.gp:
	echo $(FORM) | $(GP) @pkgdatadir@/init.gp > $@

ramified_primes.gp: form.gp
	echo "ramified_primes(readvec(\"$<\"))" | $(GP) @pkgdatadir@/init.gp > $@

info.txt: relative_equation.gp scalar_mul_absolute.gp
	$(call gp_eval_raw,"info()",$@)

projective_field.gp: projective_polynomial_reduced.gp | ramified_primes.gp
	$(call gp_eval,"nfinit([readvec(\"$<\")[1],read(\"ramified_primes.gp\")])",$@)

relative_field.gp: projective_field.gp relative_polynomial_reduced.gp
	$(call gp_eval,"g=readvec(\"relative_polynomial_reduced.gp\")[1];K=read(\"projective_field.gp\");rnfinit(K,g)",$@)

relative_equation.gp: projective_field.gp relative_polynomial_reduced.gp
	$(call gp_eval,"relative_equation()",$@)

absolute_field.gp: relative_field.gp
	$(call gp_eval,"nfinit(read(\"$<\"))",$@)

projective_polynomial_reduced.gp: projective_polynomial.gp
	$(call gp_eval_vec,"polredbest(read(\"$<\"),1)",$@)

subgroups.gp: form.gp
	$(call gp_eval_vec,"subgroup_chain(znstar(characteristic(readvec(\"$<\")[2])))",$@)

polynomial.gp: | polynomials
	$(call gp_eval,"reconstruct_polynomial()",$@)

polynomials.gp: | polynomials
	$(call gp_eval_vec,"reconstruct_polynomials()",$@)

projective_polynomial.gp: | projective_polynomials
	$(call gp_eval,"reconstruct_projective_polynomial()",$@)

relative_polynomial.gp: | relative_polynomials
	$(call gp_eval,"reconstruct_relative_polynomial()",$@)

absolute_polynomial_reduced.gp: relative_equation.gp
	$(call gp_eval_vec,"polredbest(read(\"$<\")[1],1)",$@)

intermediate_polynomials_0.gp: | subgroups.gp suitable_primes.gp intermediate_polynomials_0
	$(call gp_eval_vec,"reconstruct_intermediate_polynomials(0)",$@)

intermediate_polynomials_1.gp: intermediate_polynomials_reduced_0.gp \
	| subgroups.gp suitable_primes_ipoly_0.gp intermediate_polynomials_1
	$(call gp_eval_vec,"reconstruct_intermediate_polynomials(1)",$@)

intermediate_polynomials_2.gp: intermediate_polynomials_reduced_1.gp \
	| subgroups.gp suitable_primes_ipoly_1.gp intermediate_polynomials_2
	$(call gp_eval_vec,"reconstruct_intermediate_polynomials(2)",$@)

intermediate_polynomials_3.gp: intermediate_polynomials_reduced_2.gp \
	| subgroups.gp suitable_primes_ipoly_2.gp intermediate_polynomials_3
	$(call gp_eval_vec,"reconstruct_intermediate_polynomials(3)",$@)

intermediate_polynomials_4.gp: intermediate_polynomials_reduced_3.gp \
	| subgroups.gp suitable_primes_ipoly_3.gp intermediate_polynomials_4
	$(call gp_eval_vec,"reconstruct_intermediate_polynomials(4)",$@)

.PHONY: intermediate_polynomials_reduced intermediate_fields

intermediate_fields: subgroups.gp
	$(MAKE) intermediate_fields_`echo "length(readvec(\"$<\")[2])-1" | $(GP)`.gp

intermediate_fields_%.gp: intermediate_polynomials_reduced_%.gp | ramified_primes.gp
	$(call gp_eval_vec,"intermediate_fields($*)",$@)

intermediate_polynomials_reduced: subgroups.gp
	$(MAKE) intermediate_polynomials_reduced_`echo "length(readvec(\"$<\")[2])-1" | $(GP)`.gp

intermediate_polynomials_reduced_0.gp: intermediate_polynomials_0.gp
	$(call gp_eval_vec,"intermediate_polynomials_reduced(0)",$@)

intermediate_polynomials_reduced_1.gp: intermediate_fields_0.gp intermediate_polynomials_1.gp | ramified_primes.gp
	$(call gp_eval_vec,"intermediate_polynomials_reduced(1)",$@)

intermediate_polynomials_reduced_2.gp: intermediate_fields_1.gp intermediate_polynomials_2.gp | ramified_primes.gp
	$(call gp_eval_vec,"intermediate_polynomials_reduced(2)",$@)

intermediate_polynomials_reduced_3.gp: intermediate_fields_2.gp intermediate_polynomials_3.gp | ramified_primes.gp
	$(call gp_eval_vec,"intermediate_polynomials_reduced(3)",$@)

intermediate_polynomials_reduced_4.gp: intermediate_fields_3.gp intermediate_polynomials_4.gp | ramified_primes.gp
	$(call gp_eval_vec,"intermediate_polynomials_reduced(4)",$@)

origin.gp: | suitable_primes.gp
	$(call gp_eval,"reconstruct_origin()",$@)

origins.gp: | suitable_primes.gp
	$(call gp_eval_vec,"reconstruct_origins()",$@)

reduced_bases.gp: origins.gp | intermediate_fields
	$(call gp_eval_vec,"reduced_bases()",$@)

addition.gp: polynomial.gp | suitable_primes_poly.gp
	$(MAKE) $(patsubst %,addition_%.gp,$(shell cat suitable_primes_poly.gp))
	$(call gp_eval,"reconstruct_addition()",$@)

scalar_mul.gp: polynomial.gp | suitable_primes_poly.gp
	$(MAKE) $(patsubst %,scalar_mul_%.gp,$(shell cat suitable_primes_poly.gp))
	$(call gp_eval,"reconstruct_scalar_mul()",$@)

polynomial_star.gp: polynomial.gp origin.gp
	$(call gp_eval,"read(\"polynomial.gp\")/(x-read(\"origin.gp\"))",$@)

projectivisation.gp: polynomial_star.gp projective_polynomial.gp | suitable_primes_poly.gp
	$(MAKE) $(patsubst %,projectivisation_%.gp,$(shell cat suitable_primes_poly.gp))
	$(call gp_eval,"reconstruct_projectivisation()",$@)

projectivisation_to_reduced.gp: projectivisation.gp projective_polynomial_reduced.gp
	$(call gp_eval,"projectivisation_to_reduced()",$@)

relative_polynomial_reduced.gp: projective_field.gp relative_polynomial.gp
	$(call gp_eval_vec,"relative_polynomial_reduced()",$@)

scalar_mul_rel.gp: relative_polynomial.gp | suitable_primes_strict.gp
	$(MAKE) $(patsubst %,scalar_mul_rel_%.gp,$(shell cat suitable_primes_strict.gp))
	$(call gp_eval,"reconstruct_scalar_mul_rel()",$@)

scalar_mul_rel_reduced.gp: scalar_mul_rel.gp relative_polynomial_reduced.gp
	$(call gp_eval,"scalar_mul_rel_reduced()",$@)

scalar_mul_absolute.gp: scalar_mul_rel_reduced.gp relative_equation.gp
	$(call gp_eval,"scalar_mul_absolute()",$@)

pairing.gp: | pairings
	$(call gp_eval,"reconstruct_pairing()",$@)

dual_pair.gp: polynomials.gp pairing.gp
	cat $^ > $@

reduced_pairing.gp: | reduced_pairings
	$(call gp_eval,"reconstruct_reduced_pairing()",$@)

reduced_dual_pair.gp: reduced_pairing.gp
	$(call gp_eval_vec,"reduced_dual_pair()",$@)

.PHONY: values polynomials projective_polynomials relative_polynomials \
	intermediate_polynomials_0 intermediate_polynomials_1 intermediate_polynomials_2 \
	intermediate_polynomials_3 intermediate_polynomials_4 pairings reduced_pairings

values: primes.gp
	$(MAKE) $(patsubst %,values_%.gp,$(shell cat $<))
	$(MAKE) primes.gp

polynomials: suitable_primes.gp
	$(MAKE) $(patsubst %,poly_%.gp,$(shell cat suitable_primes.gp))

projective_polynomials: suitable_primes.gp
	$(MAKE) $(patsubst %,ppoly_%.gp,$(shell cat suitable_primes.gp))

relative_polynomials: suitable_primes_rpoly.gp
	$(MAKE) $(patsubst %,rpoly_%.gp,$(shell cat suitable_primes_rpoly.gp))

intermediate_polynomials_0: | suitable_primes.gp
	$(MAKE) $(patsubst %,ipoly_0_%.gp,$(shell cat suitable_primes.gp))

intermediate_polynomials_1: intermediate_polynomials_reduced_0.gp | suitable_primes_ipoly_red_0.gp
	$(MAKE) $(patsubst %,ipoly_1_%.gp,$(shell cat suitable_primes_ipoly_red_0.gp))

intermediate_polynomials_2: intermediate_polynomials_reduced_1.gp | suitable_primes_ipoly_red_1.gp
	$(MAKE) $(patsubst %,ipoly_2_%.gp,$(shell cat suitable_primes_ipoly_red_1.gp))

intermediate_polynomials_3: intermediate_polynomials_reduced_2.gp | suitable_primes_ipoly_red_2.gp
	$(MAKE) $(patsubst %,ipoly_3_%.gp,$(shell cat suitable_primes_ipoly_red_2.gp))

intermediate_polynomials_4: intermediate_polynomials_reduced_3.gp | suitable_primes_ipoly_red_3.gp
	$(MAKE) $(patsubst %,ipoly_4_%.gp,$(shell cat suitable_primes_ipoly_red_3.gp))

pairings: suitable_primes_dual.gp
	$(MAKE) $(patsubst %,pairing_%.gp,$(shell cat $<))

reduced_pairings: suitable_primes_pairing.gp
	$(MAKE) $(patsubst %,pairing_red_%.gp,$(shell cat $<))

poly_%.gp: values_%.gp
	$(call gp_eval_vec,"polynomials($*)",$@)

ppoly_%.gp: values_%.gp
	$(call gp_eval,"projective_polynomial($*)",$@)

rpoly_%.gp: values_%.gp projective_polynomial_reduced.gp
	$(call gp_eval,"relative_polynomial($*)",$@)

ipoly_0_%.gp: values_%.gp
	$(call gp_eval_vec,"intermediate_polynomials(0,$*)",$@)

ipoly_1_%.gp: values_%.gp intermediate_polynomials_reduced_0.gp
	$(call gp_eval_vec,"intermediate_polynomials(1,$*)",$@)

ipoly_2_%.gp: values_%.gp intermediate_polynomials_reduced_1.gp
	$(call gp_eval_vec,"intermediate_polynomials(2,$*)",$@)

ipoly_3_%.gp: values_%.gp intermediate_polynomials_reduced_2.gp
	$(call gp_eval_vec,"intermediate_polynomials(3,$*)",$@)

ipoly_4_%.gp: values_%.gp intermediate_polynomials_reduced_3.gp
	$(call gp_eval_vec,"intermediate_polynomials(4,$*)",$@)

addition_%.gp: values_%.gp
	$(call gp_eval,"addition($*)",$@)

scalar_generator.gp: form.gp
	$(call gp_eval,"liftint(znprimroot(characteristic(readvec(\"$<\")[2])))",$@)

scalar_mul_%.gp: values_%.gp scalar_generator.gp
	$(call gp_eval,"scalar_multiplication($*,read(\"scalar_generator.gp\"))",$@)

projectivisation_%.gp: values_%.gp
	$(call gp_eval,"projectivisation($*)",$@)

scalar_mul_rel_%.gp: values_%.gp scalar_generator.gp
	$(call gp_eval,"scalar_multiplication_relative($*,read(\"scalar_generator.gp\"))",$@)

pairing_%.gp: values_%.gp
	$(call gp_eval,"pairing($*)",$@)

pairing_red_%.gp: pairing_%.gp reduced_bases.gp
	$(call gp_eval,"pairing_red($*)",$@)

.PRECIOUS: values_%.gp

values_%.gp: form.gp
	$(MODGALREP) $< $* $@ || $(MAKE) update_bad_primes

primes.gp: bad_primes.gp Makefile
	$(call gp_eval_vec,"setminus(primes($(PRIMES)),readvec(\"bad_primes.gp\"))",$@)

suitable_primes.gp: primes.gp | values
	$(call gp_eval_vec,"suitable_primes()",$@)

suitable_primes_poly.gp: suitable_primes.gp | polynomials
	$(call gp_eval_vec,"suitable_primes_poly()",$@)
	if [ ! -s $@ ]; then echo "map appears to be non-injective" > /dev/stderr; false; fi

suitable_primes_ppoly.gp: suitable_primes.gp | projective_polynomials
	$(call gp_eval_vec,"suitable_primes_ppoly()",$@)
	if [ ! -s $@ ]; then echo "projective map appears to be non-injective" > /dev/stderr; false; fi

suitable_primes_rpoly.gp: suitable_primes_ppoly.gp projective_polynomial_reduced.gp
	$(call gp_eval_vec,"suitable_primes_rpoly()",$@)

suitable_primes_ipoly_0.gp: suitable_primes.gp | intermediate_polynomials_0
	$(call gp_eval_vec,"suitable_primes_ipoly(0)",$@)
	if [ ! -s $@ ]; then echo "intermediate map 0 appears to be non-injective" > /dev/stderr; false; fi

suitable_primes_ipoly_1.gp: suitable_primes_ipoly_red_0.gp intermediate_polynomials_reduced_0.gp | intermediate_polynomials_1
	$(call gp_eval_vec,"suitable_primes_ipoly(1)",$@)
	if [ ! -s $@ ]; then echo "intermediate map 1 appears to be non-injective" > /dev/stderr; false; fi

suitable_primes_ipoly_2.gp: suitable_primes_ipoly_red_1.gp intermediate_polynomials_reduced_1.gp | intermediate_polynomials_2
	$(call gp_eval_vec,"suitable_primes_ipoly(2)",$@)
	if [ ! -s $@ ]; then echo "intermediate map 2 appears to be non-injective" > /dev/stderr; false; fi

suitable_primes_ipoly_3.gp: suitable_primes_ipoly_red_2.gp intermediate_polynomials_reduced_2.gp | intermediate_polynomials_3
	$(call gp_eval_vec,"suitable_primes_ipoly(3)",$@)
	if [ ! -s $@ ]; then echo "intermediate map 3 appears to be non-injective" > /dev/stderr; false; fi

suitable_primes_ipoly_4.gp: suitable_primes_ipoly_red_3.gp intermediate_polynomials_reduced_3.gp | intermediate_polynomials_4
	$(call gp_eval_vec,"suitable_primes_ipoly(4)",$@)
	if [ ! -s $@ ]; then echo "intermediate map 4 appears to be non-injective" > /dev/stderr; false; fi

suitable_primes_ipoly_red_%.gp: suitable_primes_ipoly_%.gp intermediate_polynomials_reduced_%.gp
	$(call gp_eval_vec,"suitable_primes_ipoly_red($*)",$@)

suitable_primes_strict.gp: suitable_primes_poly.gp suitable_primes_rpoly.gp
	$(call gp_eval_vec,"setintersect(readvec(\"suitable_primes_poly.gp\"),readvec(\"suitable_primes_rpoly.gp\"))",$@)

suitable_primes_dual.gp: suitable_primes.gp | polynomials
	$(call gp_eval_vec,"suitable_primes_dual()",$@)
	if [ ! -s $@ ]; then echo "map appears to be non-injective" > /dev/stderr; false; fi

suitable_primes_pairing.gp: suitable_primes_dual.gp reduced_bases.gp
	$(call gp_eval_vec,"suitable_primes_pairing()",$@)

bad_primes.gp:
	touch $@

.PHONY: update_bad_primes

update_bad_primes:
	( flock 9 && sort -nu -o bad_primes.gp bad_primes.gp  \
	) 9> /var/lock/modgalrep.lock
