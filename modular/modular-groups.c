/***

    Modular groups

***/

#include <pari/pari.h>
#include "modular-groups.h"


/*** Counting cusps ***/

static int
count_cusps_Gamma_0(long n) {
  long c = 0, d, i;
  GEN div_n = divisorsu(n);

  for(i = 1; i < lg(div_n); i++) {
    d = div_n[i];
    c += eulerphiu(ugcd(d, n/d));
  }
  return c;
}

static int
count_cusps_Gamma_1(long n) {
  long c, d, i;
  GEN div_n;

  switch(n) {
  case 1:
    return 1;
  case 2:
  case 3:
    return 2;
  case 4:
    return 3;
  default:
    c = 0;
    div_n = divisorsu(n);
    for(i = 1; i < lg(div_n); i++) {
      d = div_n[i];
      c += eulerphiu(d) * eulerphiu(n/d);
    }
    return c / 2;
  }
}


/*** Testing equality of cusps ***/

static int
cusps_equal_Gamma_0(long n, long u1, long v1,
		    long u2, long v2) {
  long d, e;
  d = cgcd(v1, n);
  if(d != cgcd(v2, n))
    return 0;
  e = ugcd(d, n/d);
  return (smodss(u1*v1/d, e) == smodss(u2*v2/d, e));
}

static int
cusps_equal_Gamma_1(long n, long u1, long v1,
		    long u2, long v2) {
  v1 = smodss(v1, n);
  v2 = smodss(v2, n);
  if(2*v1 > n) {
    u1 = -u1; v1 = n - v1;
  }
  if(2*v2 > n) {
    u2 = -u2; v2 = n - v2;
  }
  if(v1 == 0 && v2 == 0)
    return (smodss(u1 - u2, n) == 0 || smodss(u1 + u2, n) == 0);
  if(2*v1 == n && 2*v2 == n)
    return (smodss(u1 - u2, v1) == 0 || smodss(u1 + u2, v1) == 0);
  return (v1 == v2 && smodss(u1 - u2, cgcd(v1, n)) == 0);
}


/*** Counting elliptic points ***/

static int
count_elliptic_points_2_Gamma_0(long n) {
  GEN f;
  long i, r;
  if(n % 4 == 0)
    return 0;
  f = factoru(n);
  r = 1;
  for(i = 1; i < lg(gel(f, 1)); i++)
    r *= (1 + kross(-4, coeff(f, i, 1)));
  cgiv(f);
  return r;
}

static int
count_elliptic_points_3_Gamma_0(long n) {
  GEN f;
  long i, r;
  if(n % 2 == 0 || n % 9 == 0)
    return 0;
  f = factoru(n);
  r = 1;
  for(i = 1; i < lg(gel(f, 1)); i++)
    r *= (1 + kross(-3, coeff(f, i, 1)));
  cgiv(f);
  return r;
}

static int
count_elliptic_points_2_Gamma_1(long n) {
  return (n == 1 || n == 2) ? 1 : 0;
}

static int
count_elliptic_points_3_Gamma_1(long n) {
  return (n == 1 || n == 3) ? 1 : 0;
}


/*** Counting representatives in Gamma\SL_2(Z) ***/

/*
  Return the index of  \Gamma_0(n)  in  \Gamma(1), i.e. the degree
  of the morphism of coarse modular curves  X_0(n) -> X(1).
*/
static int
count_representatives_Gamma_0(long n) {
  long nrep = 1, p, e, i;
  GEN f = factoru(n);

  for(i = 1; i < (n == 1 ? 0 : lg(gel(f, 1))); i++) {
    p = coeff(f, i, 1);
    e = coeff(f, i, 2);
    nrep *= ((p + 1) * upowuu(p, e - 1));
  }
  cgiv(f);
  return nrep;
}

/*
  Return the index of \Gamma_1(n) / {\pm1}  in  \Gamma(1) / {\pm1}, i.e.
  the degree of the morphism of coarse modular curves X_1(n) -> X(1).
  For n >= 3, this is half the index of \Gamma_1(n) in \Gamma(1),
  i.e. half the degree of the morphism between the moduli stacks.
*/
static int
count_representatives_pmGamma_1(long n) {
  long nrep, d, i;
  GEN div_n;

  switch(n) {
  case 1:
    return 1;
  case 2:
    return 3;
  default:
    div_n = divisorsu(n);
    nrep = 0;
    for(i = 1; i < lg(div_n); i++) {
      d = div_n[i];
      nrep += upowuu(d, 2) * moebiusu(n/d);
    }
    cgiv(div_n);
    return nrep / 2;
  }
}

/*
  Return the index of \Gamma_1(n) in  \Gamma(1), i.e.
  the degree of the morphism of moduli stacks X_1(n) -> X(1).
*/
static int
count_representatives_Gamma_1(long n) {
  long nrep, d, i;
  GEN div_n;

  switch(n) {
  case 1:
    return 1;
  case 2:
    return 3;
  default:
    div_n = divisorsu(n);
    nrep = 0;
    for(i = 1; i < lg(div_n); i++) {
      d = div_n[i];
      nrep += upowuu(d, 2) * moebiusu(n/d);
    }
    cgiv(div_n);
    return nrep;
  }
}


/*** Testing equality of representatives ***/

static int
representatives_equal_Gamma_0(long n, long c1, long d1,
			      long c2, long d2) {
  return (smodss(c1 * d2 - c2 * d1, n) == 0);
}

static int
representatives_equal_pmGamma_1(long n, long c1, long d1,
				long c2, long d2) {
  return ((smodss(c1 - c2, n) == 0 && smodss(d1 - d2, n) == 0)
	  || (smodss(c1 + c2, n) == 0 && smodss(d1 + d2, n) == 0));
}

static int
representatives_equal_Gamma_1(long n, long c1, long d1,
			      long c2, long d2) {
  return (smodss(c1 - c2, n) == 0 && smodss(d1 - d2, n) == 0);
}


/*** Tables of functions for specific types of groups ***/

struct group_type_info {
  int (*count_cusps)(long n);
  int (*cusps_equal)(long n, long u1, long v1,
		     long u2, long v2);
  int (*count_elliptic_points_2)(long n);
  int (*count_elliptic_points_3)(long n);
  int (*count_representatives)(long n);
  int (*representatives_equal)(long n, long u1, long v1,
			       long u2, long v2);
};

static struct group_type_info group_type_info[3] = {
  {
    &count_cusps_Gamma_0,
    &cusps_equal_Gamma_0,
    &count_elliptic_points_2_Gamma_0,
    &count_elliptic_points_3_Gamma_0,
    &count_representatives_Gamma_0,
    &representatives_equal_Gamma_0
  },
  {
    &count_cusps_Gamma_1,
    &cusps_equal_Gamma_1,
    &count_elliptic_points_2_Gamma_1,
    &count_elliptic_points_3_Gamma_1,
    &count_representatives_Gamma_1,
    &representatives_equal_Gamma_1
  },
  {
    &count_cusps_Gamma_1,
    &cusps_equal_Gamma_1,
    &count_elliptic_points_2_Gamma_1,
    &count_elliptic_points_3_Gamma_1,
    &count_representatives_pmGamma_1,
    &representatives_equal_pmGamma_1
  }
};

/*
  Hashtable of group_info structures.  Hash keys are
  of the form Vecsmall([t, n]).  Hash values are of
  the following type:
*/

struct group_info {
  long type;
  long level;
  GEN cusps;
  GEN representatives;
  hashtable *cusp_hashtable;
  hashtable *repr_hashtable;
};

/*** Hashtables for cusps ***/

/*
  Hash keys are of the form Vecsmall([t, n, u, v]).
  The hash of such a key is gcd(v, n).
*/
static ulong
hash_cusp(void *key) {
  GEN x = (GEN) key;
  return cgcd(x[4], x[2]);
}

/* Test whether two keys are equal.  */
static int
eq_cusp(void *key1, void *key2) {
  GEN x = (GEN) key1, y = (GEN) key2;
  long t = x[1], n = x[2];

  return (y[1] == t && y[2] == n
	  && (*(group_type_info[t].cusps_equal))(n, x[3], x[4], y[3], y[4]) != 0);
}


/*** Hashtables for representatives ***/

/*
  Hash keys are of the form Vecsmall([t, n, c, d]).
  The hash of such a key is gcd(c, n).
*/
static ulong
hash_rep(void *key) {
  GEN x = (GEN) key;
  return cgcd(x[3], x[2]);
}

/* Test whether two keys are equal.  */
static int
eq_rep(void *key1, void *key2) {
  GEN x = (GEN) key1, y = (GEN) key2;
  long t = x[1], n = x[2];
  return (y[1] == t && y[2] == n
	  && (*(group_type_info[t].representatives_equal))(n, x[3], x[4], y[3], y[4]) != 0);
}


/*** Utility functions for hashtables ***/

/*
  Create a naive n by n table from a hashtable whose keys are pairs of
  integers modulo n and whose values are integers stored in the last
  two entries of a mkvecsmall(4).
*/
static GEN
table_from_hashtable(hashtable *h, long n) {
  hashentry *entry;
  GEN t = zeromatcopy(n, n), key;
  long i, x, y;

  for(i = 0; i < h->len; i++) {
    for(entry = h->table[i]; entry != NULL; entry = entry->next) {
      key = (GEN) entry->key;
      x = key[3]; y = key[4];
      gcoeff(t, x + 1, y + 1) = stoi((long) entry->val);
    }
  }
  return t;
}

/*
  Generic function to clear hashtables whose keys are of type GEN and
  whose values are integers.
*/ 
static void
clear_hashtable(hashtable *h) {
  hashentry *entry;
  GEN key;
  long i;

  for(i = 0; i < h->len; i++) {
    for(entry = h->table[i]; entry != NULL; entry = entry->next) {
      key = (GEN) entry->key;
      gunclone(key);
      /* value is an integer, don't need to destroy */
    }
  }
  hash_destroy(h);
}


/*** Create a modular group ***/

static GEN
modular_group_create(long t, long n) {
  pari_sp av = avma;
  GEN G, reps, cusps, repr_table, cusp_table, key;
  long nrep, ncusps, count, h, c, d, u, v;
  hashtable *hash;
  hashentry *entry;
  struct group_type_info *type_info = &group_type_info[t];

  /* Compute representatives */
  nrep = (*(type_info->count_representatives))(n);
  reps = cgetg(nrep + 1, t_VEC);
  hash = hash_create(n * n, &hash_rep, &eq_rep, 0);
  key = cgetg(5, t_VECSMALL);
  count = 0;
  for(c = 0; c < n; c++) {
    for(d = 0; d < n; d++) {
      if(ugcd(ugcd(c, d), n) == 1) {
	key[1] = t; key[2] = n; key[3]= c; key[4] = d;
	entry = hash_search(hash, key);
	if(entry) {
	  h = (long) entry->val;
	} else {
	  h = ++count;
	  gel(reps, h) = mkvecsmall2(c, d);
	}
	hash_insert(hash, gclone(key),(void *)h);
      }
    }
  }
  if(count != nrep)
    pari_err(e_BUG, "modular_group_create: wrong number of representatives");
  repr_table = table_from_hashtable(hash, n);
  clear_hashtable(hash);

  /* Compute cusps */
  ncusps = (*(type_info->count_cusps))(n);
  cusps = cgetg(ncusps + 1, t_VEC);
  hash = hash_create(n * n, &hash_cusp, &eq_cusp, 0);
  key = cgetg(5, t_VECSMALL);
  count = 0;
  for(v = 0; v < n; v++) {
    for(u = 0; u < n; u++) {
      if(ugcd(ugcd(u, v), n) == 1) {
	key[1] = t; key[2] = n; key[3] = u; key[4] = v;
	entry = hash_search(hash, key);
	if(entry) {
	  h = (long) entry->val;
	} else {
	  h = ++count;
	  gel(cusps, h) = mkvecsmall2(u, v);
	}
	hash_insert(hash, gclone(key), (void *)h);
      }
    }
  }
  if(count != ncusps)
    pari_err(e_BUG, "modular_group_create: wrong number of cusps");
  cusp_table = table_from_hashtable(hash, n);
  clear_hashtable(hash);

  G = cgetg(9, t_VEC);
  gel(G, 1) = stoi(t);
  gel(G, 2) = stoi(n);
  gel(G, 3) = reps;
  gel(G, 4) = cusps;
  gel(G, 5) = stoi((*(type_info->count_elliptic_points_2))(n));
  gel(G, 6) = stoi((*(type_info->count_elliptic_points_3))(n));
  gel(G, 7) = repr_table;
  gel(G, 8) = cusp_table;
  return gerepilecopy(av, G);
}


/*** Modular forms ***/

long
modular_group_contains_minus1(GEN G) {
  pari_sp av = avma;
  long ind1 = modular_group_repr_index(G, mkvecsmall2(0, 1)),
    ind2 = modular_group_repr_index(G, mkvecsmall2(0, -1));
  avma = av;
  return ind1 == ind2;
}

/*
  Return the degree of the line bundle of modular forms on the
  coarse modular curve associated to the group  G,  assuming
  this line bundle exists on the coarse modular curve.
*/
GEN
modular_group_modular_forms_degree(GEN G, long weight) {
  pari_sp av = avma;
  GEN d = gdivgs(stoi(weight * modular_group_degree(G)), 24);
  if(modular_group_contains_minus1(G))
    d = gmulgs(d, 2);
  return gerepileupto(av, d);
}

/*
  Return the degree of the line bundle of cusp forms on the
  coarse modular curve associated to the group  G,  assuming
  this line bundle exists on the coarse modular curve.
*/
GEN
modular_group_cusp_forms_degree(GEN G, long weight) {
  pari_sp av = avma;
  GEN d = gsubgs(modular_group_modular_forms_degree(G, weight),
		 modular_group_num_cusps(G));
  return gerepileupto(av, d);
}

/*
  Return the genus of the (coarse) modular curve associated
  to the modular group G.
*/
long
modular_group_genus(GEN G) {
  return 1 + (gtos(gmulsg(6, modular_group_cusp_forms_degree(G, 2)))
	      - 3*itos(gel(G, 5)) - 4*itos(gel(G, 6)))/12;
}


/*** Constructors ***/

GEN
Gamma_0(long n) {
  return modular_group_create(GAMMA_0, n);
}

GEN
Gamma_1(long n) {
  return modular_group_create(GAMMA_1, n);
}

GEN
pmGamma_1(long n) {
  return modular_group_create(pmGAMMA_1, n);
}
