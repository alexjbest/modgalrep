#ifndef DIVISOR
#define DIVISOR

GEN curve_divisor_to_point (GEN X, GEN P);
GEN curve_point_to_divisor (GEN X, GEN P);
GEN curve_point_multiples (GEN X, GEN P);
GEN curve_divisor_IGS (GEN X, GEN W_D, unsigned long i);
GEN curve_divisor_section_as_matrix (GEN X, GEN s, GEN V1, GEN V2,
				     GEN Q1, GEN Q2);
GEN curve_divisor_bilinear_map (GEN X, GEN V1, GEN V2, GEN V3,
				GEN Q1, GEN Q2, GEN Q3);
GEN curve_divisor_algebra (GEN X, GEN W_D, GEN W_2_D,
			   unsigned long d);
GEN curve_decompose_divisor (GEN X, GEN W_D, GEN W_2_D,
			     unsigned long d);
GEN curve_random_prime_divisor (GEN X, unsigned long n,
				unsigned long d);
GEN curve_random_point (GEN X);
GEN curve_random_divisor_fast (GEN X, unsigned long n, unsigned long d);
GEN curve_random_divisor (GEN X, unsigned long n, unsigned long d);
GEN curve_divisor_Frob(GEN X, GEN W_D, unsigned long m);

#endif
