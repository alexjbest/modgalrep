#include <pari/pari.h>
#include <pari/paripriv.h>  /* ucoeff */
#include "Flm_solve_triangular.h"

static GEN
Flm_rsolve_upper_1(GEN U, GEN B, ulong p) {
  return Flm_Fl_mul(B, Fl_inv(coeff(U, 1, 1), p), p);
}

static GEN
Flm_rsolve_upper_2(GEN U, GEN B, ulong p) {
  ulong a = ucoeff(U, 1, 1), b = ucoeff(U, 1, 2), d = ucoeff(U, 2, 2);
  ulong D = Fl_mul(a, d, p), Dinv = Fl_inv(D, p);
  ulong ainv = Fl_mul(d, Dinv, p), dinv = Fl_mul(a, Dinv, p);
  GEN B1 = rowslice(B, 1, 1);
  GEN B2 = rowslice(B, 2, 2);
  GEN X2 = Flm_Fl_mul(B2, dinv, p);
  GEN X1 = Flm_Fl_mul(Flm_sub(B1, Flm_Fl_mul(X2, b, p), p),
		      ainv, p);
  return vconcat(X1, X2);
}

/* Solve U*X = B,  U upper triangular and invertible.  */
GEN
Flm_rsolve_upper(GEN U, GEN B, ulong p) {
  pari_sp av = avma;
  long n = lg(U) - 1, n1;
  GEN U1, U2, U11, U12, U22;
  GEN B1, B2, X1, X2;

  if (n == 0)
    return B;
  if (n == 1)
    return Flm_rsolve_upper_1(U, B, p);
  if (n == 2)
    return Flm_rsolve_upper_2(U, B, p);
  n1 = (n + 1)/2;
  U1 = vecslice(U, 1, n1);
  U2 = vecslice(U, n1 + 1, n);
  U11 = rowslice(U1, 1, n1);
  U12 = rowslice(U2, 1, n1);
  U22 = rowslice(U2, n1 + 1, n);
  B1 = rowslice(B, 1, n1);
  B2 = rowslice(B, n1 + 1, n);
  X2 = Flm_rsolve_upper(U22, B2, p);
  B1 = Flm_sub(B1, Flm_mul(U12, X2, p), p);
  X1 = Flm_rsolve_upper(U11, B1, p);
  return gerepilecopy(av, vconcat(X1, X2));
}

static GEN
Flm_lsolve_upper_1(GEN U, GEN B, ulong p) {
  return Flm_Fl_mul(B, Fl_inv(coeff(U, 1, 1), p), p);
}

static GEN
Flm_lsolve_upper_2(GEN U, GEN B, ulong p) {
  ulong a = ucoeff(U, 1, 1), b = ucoeff(U, 1, 2), d = ucoeff(U, 2, 2);
  ulong D = Fl_mul(a, d, p), Dinv = Fl_inv(D, p);
  ulong ainv = Fl_mul(d, Dinv, p), dinv = Fl_mul(a, Dinv, p);
  GEN B1 = vecslice(B, 1, 1);
  GEN B2 = vecslice(B, 2, 2);
  GEN X1 = Flm_Fl_mul(B1, ainv, p);
  GEN X2 = Flm_Fl_mul(Flm_sub(B2, Flm_Fl_mul(X1, b, p), p),
		      dinv, p);
  return shallowconcat(X1, X2);
}

/* Solve X*U = B,  U upper triangular and invertible.  */
GEN
Flm_lsolve_upper(GEN U, GEN B, ulong p) {
  pari_sp av = avma;
  long n = lg(U) - 1, n1;
  GEN U1, U2, U11, U12, U22;
  GEN B1, B2, X1, X2;

  if (n == 0)
    return B;
  if (n == 1)
    return Flm_lsolve_upper_1(U, B, p);
  if (n == 2)
    return Flm_lsolve_upper_2(U, B, p);
  n1 = (n + 1)/2;
  U1 = vecslice(U, 1, n1);
  U2 = vecslice(U, n1 + 1, n);
  U11 = rowslice(U1, 1, n1);
  U12 = rowslice(U2, 1, n1);
  U22 = rowslice(U2, n1 + 1, n);
  B1 = vecslice(B, 1, n1);
  B2 = vecslice(B, n1 + 1, n);
  X1 = Flm_lsolve_upper(U11, B1, p);
  B2 = Flm_sub(B2, Flm_mul(X1, U12, p), p);
  X2 = Flm_lsolve_upper(U22, B2, p);
  return gerepileupto(av, gconcat(X1, X2));
}

/* Solve  L*X = A, where  L = [u; a_2; ...; a_n].  */
static GEN
Flm_rsolve_lower_unit_1(GEN L, GEN A, ulong p) {
  return rowslice(A, 1, 1);
}

static GEN
Flm_rsolve_lower_unit_2(GEN L, GEN A, ulong p) {
  GEN X1 = rowslice(A, 1, 1);
  GEN X2 = Flm_sub(rowslice(A, 2, 2),
		   Flm_Fl_mul(X1, ucoeff(L, 2, 1), p), p);
  return vconcat(X1, X2);
}

/*
  Solve  L*X = A, where  L  is lower triangular with ones on
  the diagonal (at least as many rows as columns).
*/
GEN
Flm_rsolve_lower_unit(GEN L, GEN A, ulong p) {
  long m = lg(L) - 1, m1, n;
  GEN L1, L2, L11, L21, L22, A1, A2, X1, X2;
  pari_sp av = avma;

  if (m == 0)
    return zero_Flm(0, lg(A) - 1);
  if (m == 1)
    return Flm_rsolve_lower_unit_1(L, A, p);
  if (m == 2)
    return Flm_rsolve_lower_unit_2(L, A, p);
  m1 = (m + 1)/2;
  n = nbrows(L);
  L1 = vecslice(L, 1, m1);
  L2 = vecslice(L, m1 + 1, m);
  L11 = rowslice(L1, 1, m1);
  L21 = rowslice(L1, m1 + 1, n);
  L22 = rowslice(L2, m1 + 1, n);
  A1 = rowslice(A, 1, m1);
  A2 = rowslice(A, m1 + 1, n);
  X1 = Flm_rsolve_lower_unit(L11, A1, p);
  X2 = Flm_rsolve_lower_unit(L22, Flm_sub(A2, Flm_mul(L21, X1, p), p), p);
  return gerepilecopy(av, vconcat(X1, X2));
}

static GEN
Flm_lsolve_lower_unit_2(GEN L, GEN A, ulong p) {
  GEN X2 = vecslice(A, 2, 2);
  GEN X1 = Flm_sub(vecslice(A, 1, 1),
		   Flm_Fl_mul(X2, ucoeff(L, 2, 1), p), p);
  return shallowconcat(X1, X2);
}

/*
  Solve  L*X = A, where  L  is square lower triangular
  with ones on the diagonal.
*/
GEN
Flm_lsolve_lower_unit(GEN L, GEN A, ulong p) {
  long m = lg(L) - 1, m1;
  GEN L1, L2, L11, L21, L22, A1, A2, X1, X2;
  pari_sp av = avma;

  if (m <= 1)
    return A;
  if (m == 2)
    return Flm_lsolve_lower_unit_2(L, A, p);
  m1 = (m + 1)/2;
  L1 = vecslice(L, 1, m1);
  L2 = vecslice(L, m1 + 1, m);
  L11 = rowslice(L1, 1, m1);
  L21 = rowslice(L1, m1 + 1, m);
  L22 = rowslice(L2, m1 + 1, m);
  A1 = vecslice(A, 1, m1);
  A2 = vecslice(A, m1 + 1, m);
  X2 = Flm_lsolve_lower_unit(L22, A2, p);
  X1 = Flm_lsolve_lower_unit(L11, Flm_sub(A1, Flm_mul(X2, L21, p), p), p);
  return gerepileupto(av, gconcat(X1, X2));
}
