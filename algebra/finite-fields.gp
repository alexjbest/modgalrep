install (finite_field_extension, "LLDG", finite_field_extension, "libalgebra.so");
install (finite_field_vector_up, "GG", finite_field_vector_up, "libalgebra.so");
install (finite_field_vector_down, "GG", finite_field_vector_down, "libalgebra.so");
install (finite_field_vector_to_prime_field, "GG", finite_field_vector_to_prime_field, "libalgebra.so");
install (finite_field_matrix_up, "GG", finite_field_matrix_up, "libalgebra.so");
install (finite_field_matrix_down, "GG", finite_field_matrix_down, "libalgebra.so");
install (finite_field_matrix_to_prime_field, "GG", finite_field_matrix_to_prime_field, "libalgebra.so");
