/***

    Dirichlet characters

***/

#include <pari/pari.h>
#include "dirichlet-characters.h"

/*
  Apply the automorphism of  Q(zeta_n)  given by
  zeta_n |--> zeta_n^a  to the character  chi.
*/
GEN
char_apply_galois(GEN chi, long a) {
  GEN chi_a = cgetg(4, t_VEC), x;
  long j, ngen = lg(gel(chi, 2)) - 1, n = lg(gel(chi, 3)) - 1;

  gel(chi_a, 1) = icopy(gel(chi, 1));
  gel(chi_a, 2) = cgetg(ngen + 1, t_VEC);
  for(j = 1; j <= ngen; j++)
    gmael(chi_a, 2, j) = gpowgs(gmael(chi, 2, j), a);
  gel(chi_a, 3) = cgetg(n + 1, t_VEC);
  for(j = 1; j <= n; j++) {
    x = gmael(chi, 3, j);
    gmael(chi_a, 3, j) = gequal0(x) ? gen_0 : gpowgs(x, a);
  }
  return chi_a;
}

GEN
char_invert(GEN chi) {
  return char_apply_galois(chi, -1);
}

GEN
char_multiply(GEN G, GEN chi1, GEN chi2) {
  pari_sp av = avma;
  long i, j, n = char_modulus(chi1), ngen = lg(gel(chi1, 2)) - 1;
  GEN v = cgetg(ngen + 1, t_VEC), chi;

  if(n != char_modulus(chi2))
    pari_err(e_MISC, "char_multiply: characters have different moduli");
  for(j = 1; j <= ngen; j++)
    gel(v, j) = gmul(gmael(chi1, 2, j), gmael(chi2, 2, j));
  for(i = 1; i <= dirichlet_group_order(G); i++) {
    chi = gel(dirichlet_group_characters(G), i);
    if(gequal(v, gel(chi, 2))) {
      avma = av;
      return gcopy(chi);
    }
  }
  pari_err(e_MISC, "char_multiply: product not found");
  return NULL;
}

GEN
char_divide(GEN G, GEN chi1, GEN chi2) {
  pari_sp av = avma;
  long i, j, n = char_modulus(chi1), ngen = lg(gel(chi1, 2)) - 1;
  GEN v = cgetg(ngen + 1, t_VEC), chi;

  if(n != char_modulus(chi2))
    pari_err(e_MISC, "char_divide: characters have different moduli");
  for(j = 1; j <= ngen; j++)
    gel(v, j) = gdiv(gmael(chi1, 2, j), gmael(chi2, 2, j));
  for(i = 1; i <= dirichlet_group_order(G); i++) {
    chi = gel(dirichlet_group_characters(G), i);
    if(gequal(v, gel(chi, 2))) {
      avma = av;
      return gcopy(chi);
    }
  }
  pari_err(e_MISC, "char_divide: quotient not found");
  return NULL;
}

/*
  For integers m | n and a in [0, m) coprime to n,
  find the least non-negative integer b coprime to n
  such that b == a (mod m).
*/
static long
lift_coprime(long a, long m, long n) {
  long b, d;

  for(d = 0; d < n/m; d++) {
    b = a + d*m;
    if(ugcd(b, n) == 1)
      return b;
  }
  pari_err(e_MISC, "lift_coprime: no solution");
  return 0;
}

GEN
char_make_primitive(GEN chi) {
  pari_sp av = avma;
  long n = char_conductor(chi);
  GEN gen = znstar(gel(chi, 1)), vg, v, y, a, b, g;
  long ngen = lg(gel(gen, 2)) - 1, j, y_j;
  forvec_t T;

  /* Compute the values of chi on the standard generators.  */
  vg = cgetg(ngen + 1, t_VEC);
  for(j = 1; j <= ngen; j++) {
    gel(vg, j) = char_evaluate(chi, lift_coprime(itos(lift(gmael(gen, 3, j))), n,
						 char_modulus(chi)));
  }

  /* Let y_j range from 0 to gen[2][j] - 1.  */
  g = cgetg(ngen + 1, t_VEC);
  for(j = 1; j <= ngen; j++)
    gel(g, j) = mkvec2(gen_0, gsubgs(gmael(gen, 2, j), 1));
  forvec_init(&T, g, 0);

  /* Compute the list of all values.  */
  v = zerovec(n);
  while((y = forvec_next(&T)) != NULL) {
    a = gen_1;
    b = gen_1;
    for(j = 1; j <= ngen; j++) {
      y_j = itos(gel(y, j));
      a = gmul(a, gpowgs(gmael(gen, 3, j), y_j));
      b = gmul(b, gpowgs(gel(vg, j), y_j));
    }
    gel(v, (itos(lift(a)) % n) + 1) = b;
  }
  return gerepilecopy(av, mkvec3(gel(chi, 1), vg, v));
}

/*
  Check whether chi is trivial on the group of elements
  that are congruent to 1 modulo d.
*/
static int
check_trivial(GEN val, long d) {
  long n = lg(val) - 1, i;
  for(i = 0; i < n/d; i++) {
    if(ugcd(1 + i*d, n) == 1 && !gequal0(gel(val, 2 + i*d)))
      return 0;
  }
  return 1;
}

/*
  Compute the conductor of a character given its values.
*/
static long
conductor_from_values(GEN val) {
  long n = lg(val) - 1, i, p, e;
  GEN f = factoru(n);
  for(i = 1; i < lg(gel(f, 1)); i++) {
    p = coeff(f, i, 1);
    e = coeff(f, i, 2);
    while(e > 0 && check_trivial(val, n/p)) {
      n /= p;
      e--;
    }
  }
  return n;
}

/*
  Compute the group  Hom((Z/nZ)^\times, Z/eZ), where  e  is the
  exponent of (Z/nZ)^\times, in the form
    [n, gen, e, [c_1, ..., c_{\phi(n)}]].
*/
static GEN
abstract_characters(long n) {
  GEN gen = znstar(stoi(n));
  long order = itos(gel(gen, 1));
  long exponent = itos(glcm0(gel(gen, 2), NULL));
  GEN orders = gtovecsmall(gel(gen, 2));
  long ngen = lg(orders) - 1;
  GEN characters = cgetg(order + 1, t_VEC);
  GEN a, b, v = cgetg(ngen + 1, t_VEC), x, y;
  GEN values, values_gen;
  long j, k = 0, x_j, y_j;
  forvec_t T, U;

  for(j = 1; j <= ngen; j++)
    gel(v, j) = mkvec2(gen_0, stoi(orders[j] - 1));
  forvec_init(&T, v, 0);
  while((x = forvec_next(&T)) != NULL) {
    k++;
    values_gen = cgetg(ngen + 1, t_VEC);
    values = zerovec(n);
    for(j = 1; j <= ngen; j++) {
      x_j = itos(gel(x, j));
      gel(values_gen, j) = gmodulss(x_j * exponent/orders[j],
				    exponent);
    }
    forvec_init(&U, v, 0);
    while((y = forvec_next(&U)) != NULL) {
      a = gmodulss(1, n);
      b = gmodulss(0, exponent);
      for(j = 1; j <= ngen; j++) {
	y_j = itos(gel(y, j));
	a = gmul(a, gpowgs(gmael(gen, 3, j), y_j));
	b = gadd(b, gmulgs(gel(values_gen, j), y_j));
      }
      gel(values, itos(lift(a)) + 1) = b;
    }
    gel(characters, k) = mkvec3(stoi(conductor_from_values(values)),
				values_gen, values);
  }
  return mkvec4(stoi(n), gen, stoi(exponent), characters);
}

/*
  Compute the group of Dirichlet characters modulo  n  with values
  in a given ring containing a primitive  e-th roots of unity  z,
  where  e  is the exponent of (Z/nZ)^\times, in the form
    [n, gen, e, [c_1, ..., c_{\phi(n)}]].
*/
GEN
dirichlet_group(long n, GEN z) {
  pari_sp av = avma;
  GEN G = abstract_characters(n), chi, a;
  long j, k;
  for(k = 1; k <= dirichlet_group_order(G); k++) {
    chi = gel(dirichlet_group_characters(G), k);
    for(j = 1; j < lg(gel(chi, 2)); j++) {
      a = gmael(chi, 2, j);
      gmael(chi, 2, j) = gpowgs(z, itos(lift(a)));
    }
    for(j = 1; j < lg(gel(chi, 3)); j++) {
      a = gmael(chi, 3, j);
      if(ugcd(n, j - 1) > 1)
	gmael(chi, 3, j) = gen_0;
      else
	gmael(chi, 3, j) = gpowgs(z, itos(lift(a)));
    }
  }
  return gerepilecopy(av, G);
}
