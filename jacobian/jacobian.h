#ifndef JACOBIAN
#define JACOBIAN

/*** Jacobians ***/

#define jacobian_deg_L_0(X) curve_degree (X)
#define jacobian_V_0(X) curve_V (X, 1)
#define jacobian_V_0_2(X) curve_V (X, 2)
#define jacobian_V_0_3(X) curve_V (X, 3)
#define jacobian_V_0_4(X) curve_V (X, 4)
#define jacobian_IGS_V_0(X) curve_IGS_V (X, 1)

#define JACOBIAN_TYPE_MEDIUM 2
#define JACOBIAN_TYPE_LARGE 3

#define jacobian_type(X) itos (gel (X, 9))
#define jacobian_zero(X) gel (X, 10)

/* the following is only valid for the large model */
#define jacobian_s0(X) gel (X, 11)
#define jacobian_W_D0(X) gel (X, 12)
#define jacobian_IGS_W_D0(X) gel (X, 13)
#define jacobian_W_2D0(X) gel (X, 14)
#define jacobian_IGS_W_2D0(X) gel (X, 15)

/*
  Return the degree of the divisor associated to W_D.
*/
#define jacobian_divisor_deg(X, W_D) (lg (jacobian_V (X)) - lg (W_D))

GEN curve_jacobian (GEN X, unsigned long model);

GEN jacobian_deflate (GEN X, GEN W_D);
GEN jacobian_inflate (GEN X, GEN IGS_D);
GEN jacobian_flip (GEN X, GEN W_D, GEN s);
GEN jacobian_random_presentation(GEN X, GEN W_D);
int jacobian_is_zero (GEN X, GEN W_D);
int jacobian_is_zero_cert (GEN X, GEN W_D, GEN *s);
int jacobian_equal (GEN X, GEN W_D, GEN W_E);
GEN jacobian_addflip (GEN X, GEN W_D, GEN W_E);
GEN jacobian_addflip_cert (GEN X, GEN W_D, GEN W_E, GEN *s);
GEN jacobian_negate (GEN X, GEN W_D);
GEN jacobian_add (GEN X, GEN W_D, GEN W_E);
GEN jacobian_subtract (GEN X, GEN W_D, GEN W_E);
GEN jacobian_abel_jacobi_map (GEN X, GEN W_P, GEN W_Q);
GEN jacobian_normalised_representative (GEN X, GEN W_D, GEN W_rO);
GEN jacobian_normalised_divisor (GEN X, GEN W_D, GEN W_rO);
GEN jacobian_stratification (GEN X, GEN W_D, GEN W_rO);
GEN jacobian_base_change (GEN J, GEN ext);
GEN jacobian_count_points (GEN J);
GEN jacobian_random_point (GEN J);
GEN jacobian_multiply (GEN J, GEN P, GEN n);
GEN jacobian_linear_combination (GEN J, GEN P, GEN n);
GEN jacobian_Frob(GEN J, GEN P, unsigned long m);
GEN jacobian_Frob_polynomial(GEN J, GEN P, GEN f, unsigned long m);
GEN jacobian_descend(GEN J, GEN P, GEN extension, GEN multiples_O);

#endif
