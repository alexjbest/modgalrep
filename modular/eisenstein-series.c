#include <pari/pari.h>

#include "linear-algebra.h"

#include "dirichlet-characters.h"
#include "eisenstein-series.h"
#include "modular-groups.h"

/*
  Compute exp(x) to O(x^prec).  x must be a polynomial or zero, in
  which case 1 is returned.
*/
static GEN
exponential(GEN x, unsigned long prec)
{
  long k;
  GEN ex;
  pari_sp av;

  if (gequal0(x))
    return gen_1;

  av = avma;
  ex = gen_0;
  for (k = 0; k < prec; ++k)
    ex = gadd(ex, gdiv(gpowgs(x, k), mpfact(k)));
  return gerepileupto(av, gadd(ex, ggrando(x, prec)));
}

/*
  The k-th generalised Bernouilli number associated to the
  character chi (Stein, \S 5.2).

  TODO: check [Leopoldt] to see whether this makes sense for
  imprimitive characters.
*/
static GEN
gen_bernoulli(unsigned long k, GEN chi)
{
  GEN p1, p2, x = pol_x(0);
  long a, conductor = char_conductor(chi);

  if (char_modulus(chi) != conductor)
    pari_err(e_MISC, "gen_bernoulli: only defined for primitive characters");

  p1 = gen_0;
  for (a = 1; a <= conductor; a++) {
    if (cgcd(a, conductor) == 1)
      p2 = gmul(char_evaluate(chi, a), exponential(gmulsg(a, x), k + 1));
    else
      p2 = gen_0;
    p1 = gadd(p1, p2);
  }
  return gmul(mpfact(k), polcoeff0(gdiv(p1, gdiv(gsubgs(exponential(gmulsg(conductor, x), k + 2), 1), x)), k, 0));
}

/*
  Return the q-expansion of the Eisenstein series  E  up to O(q^prec).
*/
GEN
eisenstein_series_q_expansion(GEN E, unsigned long prec)
{
  GEN k = gen_0, chi1 = gen_0, chi2 = gen_0, m = gen_0,
    n = gen_0, t = gen_0, n2 = gen_0, p1 = gen_0;
  pari_sp av = avma;
  GEN q = pol_x(fetch_user_var("q"));
  k = eisenstein_series_weight(E);
  chi1 = eisenstein_series_chi1(E);
  chi2 = eisenstein_series_chi2(E);
  t = gel(E, 6);
  n2 = stoi(char_conductor(chi2));
  if (!gequalsg(char_modulus(chi2), n2))
    pari_err(e_MISC, "q_exp_eisenstein_series: only implemented for primitive chi2");
  if ((gequalgs(k, 2) && char_conductor(chi1) == 1) && gequal1(n2))
  {
    GEN p2, p3, p4, p5;
    p2 = stoi(prec - 1);
    p3 = gdivgs(gen_m1, 24);
    for (m = gen_1; gcmp(m, p2) <= 0; m = addis(m, 1))
      p3 = gadd(p3, gmul(sumdiv(m), powgi(q, m)));
    p4 = gfloor(gdivsg(prec - 1, t));
    p5 = gdivgs(gen_m1, 24);
    for (m = gen_1; gcmp(m, p4) <= 0; m = addis(m, 1))
      p5 = gadd(p5, gmul(sumdiv(m), gpow(q, gmul(t, m), prec)));
    p1 = gadd(gsub(p3, gmul(t, p5)), ggrando(q, prec));
  }
  else
  {
    GEN p6, p7, p8 = gen_0;
    p6 = gfloor(gdivsg(prec - 1, t));
    if (char_modulus(chi1) == 1)
      p8 = gdiv(gneg(gen_bernoulli(itos(k), chi2)), gmulsg(2, k));
    else
      p8 = gen_0;
    p7 = p8;
      for (m = gen_1; gcmp(m, p6) <= 0; m = addis(m, 1))
      {
        GEN p9;
        GEN p10;
        p9 = divisors(m);
        p10 = gen_0;
        {
          long l11;
          for (l11 = 1; l11 < lg(p9); ++l11)
          {
            n = icopy(gel(p9, l11));
            p10 = gadd(p10, gmul(gmul(char_evaluate(chi1, itos(gdiv(m, n))), char_evaluate(chi2, itos(n))), gpow(n, gsubgs(k, 1), prec)));
          }
        }
        p7 = gadd(p7, gmul(p10, gpow(q, gmul(t, m), prec)));
      }
    p1 = gadd(p7, ggrando(q, prec));
  }
  return gerepileupto(av, p1);
}

/*
  Return a vector containing the Eisenstein series of weight  k
  with character  eps  in the Dirichlet group  D.
*/
GEN
eisenstein_series_with_character(GEN eps, GEN D, unsigned long weight, unsigned long prec)
{
  pari_sp av = avma;
  GEN characters, chi1, chi2;
  long i, j, n, n1, n2, t;
  GEN p2, p4;
  GEN L = cgetg(1, t_VEC);

  if (!gequal(char_evaluate(eps, -1), gpowgs(gen_m1, weight)))
    return L;

  if (weight == 1)
    pari_err(e_MISC, "eisenstein_series_with_character: weight 1 not implemented");
  n = itos(dirichlet_group_modulus(D));
  characters = dirichlet_group_characters(D);

  for (j = 1; j < lg(characters); j++) {
    chi1 = gel(characters, j);
    chi2 = char_divide(D, eps, chi1);
    n1 = char_conductor(chi1);
    n2 = char_conductor(chi2);
    if (n % (n1 * n2) == 0) {
      p2 = divisorsu(n / (n1 * n2));
      for (i = 1; i < lg(p2); i++) {
	t = p2[i];
	if (!(weight == 2 && n1 == 1 && n2 == 1 && t == 1)) {
	  p4 = cgetg(7, t_VEC);
	  gel(p4, 1) = stoi(n);
	  gel(p4, 2) = stoi(weight);
	  gel(p4, 3) = eps;
	  gel(p4, 4) = char_make_primitive(chi1);
	  gel(p4, 5) = char_make_primitive(chi2);
	  gel(p4, 6) = stoi(t);
	  L = shallowconcat(L, mkvec(p4));
	}
      }
    }
  }
  return gerepilecopy(av, L);
}

/*
  Return a vector containing the Eisenstein series of weight  k
  for the given modular group.

  TODO: allow Eisenstein series over finite fields.
*/
GEN
eisenstein_series(GEN group, unsigned long weight, unsigned long prec) {
  pari_sp av = avma;
  GEN e, z_e, D, characters, M;
  long j, n;
  GEN q = pol_x(fetch_user_var("q"));  /* only for variable ordering */
  GEN z = pol_x(fetch_user_var("z"));
  n = modular_group_level(group);
  e = glcm0(gel(znstar(stoi(n)), 2), NULL);
  z_e = gmodulo(z, polcyclo_eval(itos(e), z));
  D = dirichlet_group(n, z_e);
  if (gequal0(gel(group, 1))) {
    characters = mkvec(gel(dirichlet_group_characters(D), 1));
  } else if (gequal1(gel(group, 1)) || gequalgs(gel(group, 1), 2)) {
    characters = dirichlet_group_characters(D);
  } else {
    pari_err(e_MISC, "eisenstein_series: not implemented for groups other than Gamma_0 and Gamma_1");
    return NULL;
  }
  M = cgetg(lg(characters), t_VEC);
  for (j = 1; j < lg(characters); ++j) {
    gel(M, j) = eisenstein_series_with_character(gel(characters, j), D, weight, prec);
  }
  return gerepileupto(av, gconcat(M, NULL));
}

/*
  Return the (permutation) matrix of the automorphism  \zeta -> \zeta^a
  acting on the space E of Eisenstein series.
*/
GEN
eisenstein_series_galois(GEN E, long a) {
  GEN M, chi1, chi2;
  long i, j, dim = glength(E);

  M = zeromatcopy(dim, dim);
  for (j = 1; j <= dim; j++) {
    chi1 = char_apply_galois(eisenstein_series_chi1(gel(E, j)), a);
    chi2 = char_apply_galois(eisenstein_series_chi2(gel(E, j)), a);

    for (i = 1; i <= dim; i++) {
      if (gequal(eisenstein_series_chi1(gel(E, i)), chi1)
	  && gequal(eisenstein_series_chi2(gel(E, i)), chi2)) {
	gcoeff(M, i, j) = gen_1;
	break;
      }
    }
  }
  return M;
}

/*
  Return a basis of q-expansions of Eisenstein series up to O(q^prec).

  TODO: this doesn't check whether the required precision is enough.
*/
GEN
eisenstein_series_q_expansion_basis(GEN E, GEN p, unsigned long prec) {
  GEN Eq, M, result, a;
  long dim = lg(E) - 1, i, j;
  pari_sp av = avma;

  printf("eisenstein_series_q_expansion_basis: computing q-expansions\n");

  Eq = cgetg(dim + 1, t_VEC);
  for (i = 1; i <= dim; ++i)
    gel(Eq, i) = eisenstein_series_q_expansion(gel(E, i), prec);

  printf("eisenstein_series_q_expansion_basis: computing echelon form\n");

  M = cgetg(dim + 1, t_MAT);
  for (i = 1; i <= dim; ++i) {
    gel(M, i) = cgetg(prec + 1, t_COL);
    for (j = 1; j <= prec; ++j)
      gcoeff(M, j, i) = polcoeff0(gel(Eq, i), j - 1, -1);
  }

  /*
    Computing the reduced column echelon form will give us a basis
    over the prime field.
  */
  result = reduced_column_echelon_form(M, NULL, 0);
  /* result[1] is the rank */
  if (itos(gel(result, 1)) != dim)
    pari_err(e_MISC, "eisenstein_series_q_expansion_basis: wrong rank");

  M = cgetg(dim + 1, t_MAT);
  for (i = 1; i <= dim; ++i) {
    gel(M, i) = cgetg(prec + 1, t_COL);
    for (j = 1; j <= prec; ++j) {
      a = lift(gcoeff(gel(result, 2), j, i));
      if (typ(a) == t_POL) {
	if (degree(a) > 0)
	  pari_err(e_MISC, "eisenstein_series_q_expansion_basis: series not defined over the prime field?!");
	gcoeff(M, j, i) = polcoeff0(a, 0, -1);
      }
      else {
	/* not a polynomial */
	gcoeff(M, j, i) = a;
      }
    }
  }

  if (gsigne(p) > 0) {
    M = matrixqz0(M, p);
    M = gmul(M, gmodulsg(1, p));
  }
  return gerepileupto(av, M);
}

/*
  Return  [E, [<1>, ..., <n-1>], [T_1, ..., T_{prec-1}]],
  where  E  is a basis of q-expansions of Eisenstein series up to
  O(q^prec)  and the  T_n  and  <d>  are the Hecke operators
  with respect to this basis.

  TODO: this crashes while computing  B^(-1)  when called as
    eisenstein_series_as_hecke_module(Gamma_1(11),2,101)
  or
    eisenstein_series_as_hecke_module(Gamma_1(35),2,101)
  This is probably because the cyclotomic polynomial used is
  reducible modulo  p.
*/
GEN
eisenstein_series_as_hecke_module(GEN group, unsigned long weight, GEN p, unsigned long prec)
{
  GEN B, Binv, E, Eq, M, T, diam, T_i, result, gen, exponents, factors, a, d, v;
  long bound, dim, level, ngen, e, i, j, v_i;
  forvec_t iter;
  pari_sp av = avma;

  level = modular_group_level(group);
  gen = znstar(stoi(level));
  ngen = lg(gel(gen, 2)) - 1;
  exponents = cgetg(ngen + 1, t_VEC);
  for (i = 1; i <= ngen; ++i)
    gel(exponents, i) = mkvec2(gen_0, gsubgs(gmael(gen, 2, i), 1));

  printf("eisenstein_series_as_hecke_module: computing Eisenstein series\n");
  E = eisenstein_series(group, weight, prec);
  printf("eisenstein_series_as_hecke_module: computing q-expansions\n");
  dim = glength(E);
  bound = itos(modular_group_modular_forms_degree(group, weight)) + 1;
  if (prec < bound)
    prec = bound;

  Eq = cgetg(dim + 1, t_VEC);
  for (i = 1; i <= dim; ++i)
    gel(Eq, i) = eisenstein_series_q_expansion(gel(E, i), prec);

  /*
    TODO: the following will fail if  p  divides the denominator
    of the constant term of one of the q-expansions.
  */
  if (gsigne(p) > 0)
    Eq = gmul(Eq, gmodulsg(1, p));
  printf("eisenstein_series_as_hecke_module: computing echelon form\n");

  M = cgetg(dim + 1, t_MAT);
  for (i = 1; i <= dim; ++i) {
    gel(M, i) = cgetg(prec + 1, t_COL);
    for (j = 1; j <= prec; ++j)
      gcoeff(M, j, i) = polcoeff0(gel(Eq, i), j - 1, -1);
  }

  /*
    Computing the reduced column echelon form will give us
    a basis over the prime field.
  */
  result = reduced_column_echelon_form(M, matid(dim), 0);
  /* result[1] is the rank */
  if (itos(gel(result, 1)) != dim)
    pari_err(e_MISC, "eisenstein_series_as_hecke_module: wrong rank");

  M = cgetg(dim + 1, t_MAT);
  for (i = 1; i <= dim; ++i) {
    gel(M, i) = cgetg(prec + 1, t_COL);
    for (j = 1; j <= prec; ++j) {
      GEN p11 = gen_0;
      a = gcoeff(gel(result, 2), j, i);
      if (typ(a) == t_POLMOD) {
	a = lift(a);
	if (degree(a) > 0)
	  pari_err(e_MISC, "eisenstein_series_as_hecke_module: series not defined over the prime field?!");
	p11 = polcoeff0(a, 0, -1);
      }
      else
	/* not a polynomial */
	p11 = a;
      gcoeff(M, j, i) = p11;
    }
  }

  /*
    B  is the base change matrix from the basis of eigenforms to
    the echelon basis (the transpose is because we have placed
    the  q-expansions in columns and not in rows).
  */
  B = gtrans(gel(result, 3));
  Binv = ginv(B);

  printf("eisenstein_series_as_hecke_module: computing diamond operators:");
  diam = cgetg(level + 1, t_VEC);
  for (i = 1; i <= level; i++)
    gel(diam, i) = zeromat(dim, dim);

  for (i = 1; i <= ngen; i++) {
    i = itos(liftint(gmael(gen, 3, i)));
    printf(" %li", i);
    a = cgetg(dim + 1, t_VEC);
    for (j = 1; j <= dim; ++j)
      gel(a, j) = char_evaluate(eisenstein_series_character(gel(E, j)), i);
    gel(diam, i + 1) = lift(gmul(matmuldiagonal(B, a), Binv));
  }
  printf("\n");

  forvec_init(&iter, exponents, 0);
  while ((v = forvec_next(&iter)) != NULL) {
    d = gen_1;
    a = matid(dim);
    for (i = 1; i <= ngen; i++) {
      v_i = itos(gel(v, i));
      d = gmul(d, gpowgs(gmael(gen, 3, i), v_i));
      a = gmul(a, gpowgs(gel(diam, itos(liftint(gmael(gen, 3, i))) + 1), v_i));
    }
    gel(diam, itos(liftint(d)) + 1) = a;
  }

  printf("eisenstein_series_as_hecke_module: computing operators T_n up to n = %li:", prec - 1);
  T = zerovec(prec - 1);
  if (prec > 1)
    gel(T, 1) = matid(dim);

  for (i = 2; i < prec; i++) {
    if (uisprime(i)) {
      printf(" %li", i);
      T_i = cgetg(dim + 1, t_VEC);
      for (j = 1; j <= dim; ++j)
	gel(T_i, j) = polcoeff0(gel(Eq, j), i, -1);
      gel(T, i) = lift(gmul(matmuldiagonal(B, T_i), Binv));
    } else {
      /* i  is composite */
      factors = factorint(stoi(i), 0);
      p = gcoeff(factors, 1, 1);
      e = itos(gcoeff(factors, 1, 2));
      if (gequal1(gel(matsize(factors), 1))) {
	/* i  is a prime power */
	if (dvdsi(level, p))
	  gel(T, i) = gmul(gel(T, gtos(p)), gel(T, gtos(gpowgs(p, e - 1))));
	else {
	  /* p  does not divide the level */
	  gel(T, i) = gsub(gmul(gel(T, gtos(p)), gel(T, gtos(gpowgs(p, e - 1)))), gmul(gmul(gpowgs(p, weight - 1), gel(diam, smodis(p, level) + 1)), gel(T, gtos(gpowgs(p, e - 2)))));
	}
      } else {
	/* i  is not a prime power */
	gel(T, i) = gmul(gel(T, gtos(gpowgs(p, e))), gel(T, gtos(gdivsg(i, gpowgs(p, e)))));
      }
    }
  }
  printf("\n");
  return gerepilecopy(av, mkvec3(M, diam, T));
}

