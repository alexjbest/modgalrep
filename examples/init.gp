\r modular-groups
\r modular-forms
\r modular-symbols
\r cusp-forms

ramified_primes(form) =
{
   my(M, subspace, n, l);
   [M, subspace] = form;
   n = modular_group_level(modular_symbols_group(M));
   l = characteristic(subspace);
   setunion(factorint(n)[,1]~, [l]);
}

init_modforms(n, weight=2, prec=1000) =
{
   M = modular_forms_q_expansion_basis(Gamma_1(n), weight, 0, prec);
   MZ = matrixqz(M);
   MZ *= qflll(MZ);
   write(concat(["modforms_Gamma_1_", Str(n), "_weight_",
		 Str(weight), "_prec_", Str(prec), ".gp"]), MZ);
}

init_dim_1(n, k, l) =
{
   S = cusp_forms(Gamma_1(n), k, l);
   if(cusp_forms_dimension(S) != 1,
      error("space of cusp forms does not have dimension 1"));
   print(S[1]);
   print(S[2]);
}

init_level_1(k, l) = init_dim_1(1, k, l);

init_weight_2(n, l) = init_dim_1(n, 2, l);

init_eigenform(n, k, l, m, a_m) =
{
   my(S = cusp_forms(Gamma_1(n), k, l),
      K = matker(cusp_forms_hecke_operator(S, m) - a_m));
   if(matsize(K)[2] != 1,
      error("eigenspace does not have dimension 1"));
   print(S[1]);
   print(S[2] * K);
}
