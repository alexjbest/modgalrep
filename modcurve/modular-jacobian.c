/***

    Modular Jacobians

***/

#include <pari/pari.h>

#include "curve.h"
#include "divisor.h"
#include "jacobian.h"
#include "matsmall.h"
#include "modular-curve.h"
#include "modular-jacobian.h"


GEN
modular_curve_jacobian_medium(GEN group, unsigned long p) {
  return curve_jacobian (modular_curve (group, p, 8),
			 JACOBIAN_TYPE_MEDIUM);
}

GEN
modular_curve_jacobian_large(GEN group, unsigned long p) {
  return curve_jacobian (modular_curve (group, p, 9),
			 JACOBIAN_TYPE_LARGE);
}

/*
  Given an element  x in Pic^0 X  and a rational point  O
  of  X, return a "normalised" divisor determined by  x.
  This divisor is of degree  <= g  and is represented as a
  subspace of the space of global sections of the line
  bundle  L  of degree  >= 2*g + 1.

  This should be faster than jacobian_normalised_divisor().
*/
GEN
modular_jacobian_normalised_divisor(GEN X, GEN W_D, GEN W_rO) {
  unsigned long p = curve_base_field_characteristic(X);
  GEN T = curve_base_field_polynomial(X);
  long r;
  GEN R, s, W, W4_D_rO;
  pari_sp av = avma;

  switch (jacobian_type(X)) {
  case JACOBIAN_TYPE_MEDIUM:
    W_D = matsmall_echelon(W_D, &R, p, T);
    r = lg(R) - 1;
    s = gel(W_D, r);
    W = curve_multiply_section_subspace(X, s, curve_V(X, 3));
    W4_D_rO = curve_multiply_subspaces(X, W_D, gel(W_rO, R[r] - R[1]));
    return gerepileupto(av, curve_divide_subspaces(X, W, 5, W4_D_rO, 4));
  case JACOBIAN_TYPE_LARGE:
    pari_err(e_MISC, "not implemented");
  default:
    pari_err(e_MISC, "invalid type for the Jacobian");
  }
  return NULL;
}


/*
  TODO: compute Hecke operators.

  How many Hecke operators do we need to locate the right subspace
  of the l-torsion of the Jacobian?  In any case, we only need the
  T_p for p prime.
*/
