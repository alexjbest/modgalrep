#include <pari/pari.h>

#include "curve.h"
#include "divisor.h"
#include "finite-fields.h"
#include "jacobian.h"
#include "l-torsion.h"
#include "matsmall.h"
#include "modular-curve.h"
#include "modular-jacobian.h"
#include "pairings.h"
#include "torsion-subscheme.h"


#define GIVEUP 1  /* give up if computation is too hard */


/*
  Simple-minded function to compute the multiplicative order
  of  x  modulo  f.
*/
static unsigned long
order_mod(GEN x, GEN f) {
  unsigned long k;
  pari_sp av = avma;

  if(!gequal1(ggcd(x, f)))
    pari_err(e_MISC, "%Ps not invertible modulo %Ps", x, f);
  for(k = 1; !gequal1(gpowgs(gmodulo(x, f), k)); k++);
  avma = av;
  return k;
}

/*
  Factor the polynomial F  as

    F = f_primary * projector  in  F_l[x],

  where  f_primary  has the same irreducible factors as  f
  and  projector  is coprime to  f.
*/
static void
factor_coprime(GEN F, GEN f, GEN *f_primary, GEN *projector, long l) {
  GEN g;
  GEN factorisation = factormod0(F, stoi(l), 0);
  long i;

  *f_primary = *projector = gen_1;
  for(i = 1; i < lg(gel(factorisation, 1)); i++) {
    g = gpowgs(gcoeff(factorisation, i, 1),
	       itos(gcoeff(factorisation, i, 2)));
    if(gequal0(gmod(f, gcoeff(factorisation, i, 1))))
      *f_primary = gmul(*f_primary, g);
    else
      *projector = gmul(*projector, g);
  }
}

static GEN
find_extension(GEN group, unsigned long l, unsigned long p,
	       GEN f, GEN *projector, GEN *projector_dual,
	       unsigned long max_degree) {
  pari_sp av = avma;
  GEN charpoly_Frob = modular_curve_characteristic_polynomial_frobenius(group, p);
  GEN f_dual, f_primary, extension, x;
  long extension_degree;

  x = pol_x(gvar(f));
  extension_degree = order_mod(x, f);
  extension_degree = clcm(extension_degree, itos(order(gmodulss(p, l))));

  if (extension_degree > max_degree)
    pari_err(e_MISC, "extension degree too high (%li)", extension_degree);

  printf("using extension of F_%li of degree %li\n", p, extension_degree);
  extension = finite_field_extension(extension_degree, p, NULL);

  /* f = x^2 + a*x + b  ==>  g = x^2 + (p*a/b)*x + p^2/b */
  f_dual = shallowcopy(f);
  gel(f_dual, 3) = gmulsg(p, gdiv(gel(f, 3), gel(f, 2)));
  gel(f_dual, 2) = gdiv(sqru(p), gel(f, 2));
  pari_printf("dual polynomial: %Ps\n", f_dual);

  factor_coprime(charpoly_Frob, f, &f_primary, projector, l);
  pari_printf("using factorisation %Ps = (%Ps) * (%Ps) mod %li\n",
	      charpoly_Frob, lift(f_primary), lift(*projector), l);

  /* We want  f  to cut out the representation.  */
  if(!gequal(f_primary, f))
    pari_err(e_MISC, "subspace not determined by Frobenius action");

  if(gequal(f_dual, f))
    *projector_dual = *projector;
  else {
    factor_coprime(charpoly_Frob, f_dual, &f_primary, projector_dual, l);
    /* We want  f_dual  to cut out the dual representation.  */
    if(!gequal(f_primary, f_dual))
      pari_err(e_MISC, "dual subspace not determined by Frobenius action");
  }

  gerepileall(av, 3, &extension, projector, projector_dual);
  return extension;
}

/* Naïve algorithm for finding a random  l-torsion point.  */
static GEN
random_torsion_point(GEN J, unsigned long l) {
  GEN n, m;
  long v;
  GEN P, Q;
  pari_sp av = avma;

  n = jacobian_count_points(J);
  v = gvaluation(n, stoi(l));
  if(v == 0)
    pari_err(e_MISC, "no point of order %li", l);
  m = gdiv(n, powuu(l, v));
  while(1) {
    puts("finding random point");
    P = jacobian_random_point(J);
    pari_printf("multiplying by %Ps\n", m);
    P = jacobian_multiply(J, P, m);
    if(!jacobian_is_zero(J, P)) {
      while(1) {
	Q = jacobian_multiply(J, P, stoi(l));
	if(jacobian_is_zero(J, Q))
	  break;
	P = Q;
      }
      break;
    }
  }
  return gerepileupto(av, P);
}

static GEN
random_representation_space_point(GEN J, unsigned long l, GEN projector, int tries) {
  GEN P;
  pari_sp av = avma;
  while (tries-- > 0) {
    avma = av;
    P = random_torsion_point(J, l);
    pari_printf("applying Frobenius polynomial %Ps\n", projector);
    P = jacobian_Frob_polynomial(J, P, centerlift(projector), 1);
    if(!jacobian_is_zero(J, P))
      return gerepileupto(av, P);
  }
  if (GIVEUP)
    pari_err(e_MISC, "too many tries to find a random point");
  return NULL;
}

/* Naïve algorithm for finding a basis of the  l-torsion.  */
static GEN
find_basis(GEN J, unsigned long l, GEN projector, int tries) {
  GEN P, Q, multiples_P;
  long i;
  pari_sp av = avma;
  int tries0 = tries;

  /* Find a point  P  in the representation space.  */
  P = random_representation_space_point(J, l, projector, tries0);
  if (P == NULL)
    return NULL;

  /* Create the vector [0, P, 2*P, ..., (l - 1)*P].  */
  multiples_P = cgetg(l + 1, t_VEC);
  gel(multiples_P, 1) = jacobian_zero(J);
  for(i = 1; i <= l - 1; i++)
    gel(multiples_P, i + 1) = jacobian_add(J, gel(multiples_P, i), P);

  puts("looking for a linearly independent point");
  Q = jacobian_Frob(J, P, 1);

  while(tries-- > 0) {
    int Q_multiple_of_P = 0;
    for(i = 1; i <= l; i++) {
      if(jacobian_equal(J, Q, gel(multiples_P, i))) {
	printf("Q = %li*P\n", i - 1);
	Q_multiple_of_P = 1;
	break;
      }
    }
    if(!Q_multiple_of_P)
      return gerepilecopy(av, mkvec2(P, Q));;
    puts("generating new torsion point");
    Q = random_representation_space_point(J, l, projector, tries0);
    if (Q == NULL)
      break;
  }
  if (GIVEUP)
    pari_err(e_MISC, "too many tries to find a linearly independent point");
  return NULL;
}

static GEN
Frob_matrix(GEN J, unsigned long l, GEN basis, unsigned long m, GEN f) {
  GEN P = gel(basis, 1), Q = gel(basis, 2), Frob;
  pari_sp av = avma;

  printf("computing Frobenius matrix\n");
  if(jacobian_equal(J, Q, jacobian_Frob(J, P, m))) {
    /*
      If we have taken  Q = Frob(P), then the matrix of  Frob
      with respect to (P, Q) is the companion matrix of the
      characteristic polynomial of  Frob, which is  f.
      We just do a consistency check.
    */
    GEN lincomb = jacobian_add(J,
			       jacobian_multiply(J, P, gneg(lift(polcoeff0(f, 0, -1)))),
			       jacobian_multiply(J, Q, gneg(lift(polcoeff0(f, 1, -1)))));
    if(!jacobian_equal(J, jacobian_Frob(J, Q, m), lincomb))
      pari_err(e_MISC, "inconsistent matrix of Frobenius");
    Frob = matcompanion(f);
  } else {
    /* Q != Frob(P) */
    Frob = jacobian_l_torsion_Frob_matrix(J, l, basis, m);
  }

  pari_printf("Frobenius matrix = %Ps\n", Frob);
  return gerepileupto(av, Frob);
}

static GEN
eval_function(GEN J, GEN D, GEN multiples_O, long *w) {
  unsigned long p = curve_base_field_characteristic (J);
  GEN T = curve_base_field_polynomial (J);
  GEN R, x;
  long a, i, r;
  pari_sp av = avma;

  D = modular_jacobian_normalised_divisor(J, D, multiples_O);
  D = matsmall_echelon(D, &R, p, T);
  r = lg(D) - 1;
  /* compute the "weight" */
  *w = 0;
  for (i = 1; i <= r; i++)
    *w += R[i];

  if (T == NULL) {
    a = coeff(D, R[r] + 1, r);
    x = gmodulss(a, p);
  } else {
    x = gcoeff(D, R[r] + 1, r);
    x = gmodulo(gmul(Flx_to_ZX(x), gmodulss(1, p)),
		gmul(Flx_to_ZX(T), gmodulss(1, p)));
  }
  return gerepileupto(av, x);
}

static GEN
values_from_basis(GEN J, GEN P, GEN Q, long l, GEN multiples_O) {
  pari_sp av = avma, av1;
  long i, j, w, W = 0;
  GEN multiples_P, multiples_Q, D, V;

  printf("computing multiples of P and Q\n");
  multiples_P = cgetg(l + 1, t_VEC);
  multiples_Q = cgetg(l + 1, t_VEC);
  gel(multiples_P, 1) = gel(multiples_Q, 1) = jacobian_zero(J);
  for(i = 1; i < l; i++) {
    gel(multiples_P, i + 1) = jacobian_add(J, gel(multiples_P, i), P);
    gel(multiples_Q, i + 1) = jacobian_add(J, gel(multiples_Q, i), Q);
  }

  printf("computing function values:");
  av1 = avma;
  V = zeromatcopy(l, l);
  for (i = 0; i < l; i++) {
    for (j = 0; j < l; j++) {
      printf(" (%li, %li)", i, j);
      fflush(stdout);
      D = jacobian_addflip(J, gel(multiples_P, i + 1), gel(multiples_Q, j + 1));
      gcoeff(V, i + 1, j + 1) = eval_function(J, D, multiples_O, &w);
      W += w;
      if (gc_needed(av1, 1))
	V = gerepilecopy(av1, V);
    }
  }
  putchar('\n');
  return gerepilecopy(av, mkvec2(V, stoi(W)));
}

/*
  Return a pair [V, W], where  V  is the matrix of all
  function values and  W  is the total weight.
*/
static GEN
all_function_values(GEN J, unsigned long l, GEN projector,
		    GEN projector_dual, int tries) {
  pari_sp av = avma;
  GEN basis, basis_dual, values, values_dual;
  GEN O, multiples_O, P, Q, P_dual, Q_dual, z, Z;

  printf("computing multiples of the distinguished point\n");
  O = modular_curve_distinguished_point(J);
  multiples_O = curve_point_multiples(J, O);

  printf("computing bases for the desired subspaces of the %li-torsion\n", l);
  basis = find_basis(J, l, projector, tries);
  if (!basis) {
    basis = jacobian_l_torsion_basis(J, l, multiples_O);
    /* TODO: project */
  }
  /* TODO: find the dual subspace at the same time */

  P = gel(basis, 1);
  Q = gel(basis, 2);
  values = values_from_basis(J, P, Q, l, multiples_O);

  if (gequal(projector, projector_dual)) {
    basis_dual = basis;
    printf("computing Weil pairing\n");
    z = jacobian_weil_pairing(J, P, Q, l);
    if(gequal1(z) || !gequal1(gpowgs(z, l)))
      pari_err(e_MISC, "Weil pairing not of order %li\n", l);
    return gerepilecopy(av, shallowconcat(values, mkvec(z)));
  }
  else {
    basis_dual = find_basis(J, l, projector_dual, tries);
    P_dual = gel(basis_dual, 1);
    Q_dual = gel(basis_dual, 2);
    values_dual = values_from_basis(J, P_dual, Q_dual, l, multiples_O);
    printf("computing Weil pairings\n");
    Z = mkmat2(mkcol2(jacobian_weil_pairing(J, P, P_dual, l),
		      jacobian_weil_pairing(J, Q, P_dual, l)),
	       mkcol2(jacobian_weil_pairing(J, P, Q_dual, l),
		      jacobian_weil_pairing(J, Q, Q_dual, l)));
    return gerepilecopy(av, shallowconcat1(mkvec3(values, values_dual,
						  mkvec(Z))));
  }
}

/*
  Return the torsion subscheme of J_Gamma over F_p realising the mod l
  representation attached to a cusp form such that the characteristic
  polynomial of Frob_p equals f.

  We check that this subscheme is two-dimensional over F_l.
*/
GEN
torsion_subscheme(GEN Gamma, unsigned long l, unsigned long p,
		  GEN f, unsigned long max_degree, unsigned long tries) {
  GEN projector, projector_dual;
  GEN extension, J, J_k, order_J, values;
  long v;
  pari_sp av = avma;

  /* Find the field of definition of the representation space.  */
  extension = find_extension(Gamma, l, p, f, &projector,
			     &projector_dual, max_degree);

  /*
    Compute the Jacobian and its base change to the right field.
  */
  J = modular_curve_jacobian_medium(Gamma, p);
  printf("working with curve of genus %li with projective"
	 " embedding of degree %li into P^%li\n",
	 curve_genus(J), curve_degree(J), lg(curve_V(J, 1)) - 2);

  J_k = jacobian_base_change(J, extension);

  order_J = jacobian_count_points(J_k);
  v = gvaluation(order_J, stoi(l));
  pari_printf("Jacobian has order %Pi = %li^%li * %Pi\n",
	      order_J, l, v, gdiv(order_J, powuu(l, v)));

  values = all_function_values(J_k, l, projector, projector_dual, tries);
  return gerepileupto(av, values);
}
