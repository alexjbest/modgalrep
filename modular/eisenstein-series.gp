eisenstein_series_level(E) = E[1];
eisenstein_series_weight(E) = E[2];
eisenstein_series_character(E) = E[3];
eisenstein_series_chi1(E) = E[4];
eisenstein_series_chi2(E) = E[5];
install(eisenstein_series_q_expansion, "GL", , "libmodular.so");
install(eisenstein_series_with_character, "GGLL", , "libmodular.so");
install(eisenstein_series, "GLD0,L,", , "libmodular.so");
install(eisenstein_series_q_expansion_basis, "GGD0,L,", , "libmodular.so");
install(eisenstein_series_as_hecke_module, "GLGD0,L,", , "libmodular.so");
