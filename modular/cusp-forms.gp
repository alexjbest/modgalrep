cusp_forms_modular_symbols(S) = S[1];
cusp_forms_group(S) = modular_symbols_group (S[1]);
cusp_forms_weight(S) = modular_symbols_weight (S[1]);
cusp_forms_to_modular_symbols(S) = S[2];
cusp_forms_dimension(S) = matsize (S[2]) [2];

install (cusp_forms, "GLDG", , "libmodular.so");
install (cusp_forms_diamond_operator, "GL", , "libmodular.so");
install (cusp_forms_hecke_operator, "GL", , "libmodular.so");
install (cusp_forms_atkin_lehner_operator, "GL", , "libmodular.so");
install (cusp_forms_hecke_algebra, "GD0,L,", , "libmodular.so");
install (cusp_forms_q_expansion_basis, "GD0,L,", , "libmodular.so");
install (hecke_algebra_q_expansion_basis, "GLG", , "libmodular.so");
install (cusp_forms_as_hecke_module, "GLD0,G,D0,L,", , "libmodular.so");
