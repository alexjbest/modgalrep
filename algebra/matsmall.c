#include <pari/pari.h>
#include <pari/paripriv.h>  /* ucoeff */

#include "finite-fields.h"
#include "matsmall.h"
#include "Flm_echelon.h"
#include "FlxqM_echelon.h"


/***
    Functions dealing with ``small matrices''.
***/

GEN
matsmall_id (long n, unsigned long p, GEN T) {
  if (T == NULL)
    return matid_Flm (n);
  else
    return matid (n);
}

GEN
matsmall_zero (long m, long n, unsigned long p, GEN T) {
  if (T == NULL)
    return zero_Flm_copy (m, n);
  else
    return zeromatcopy (m, n);
}

GEN
matsmall_transpose (GEN A, unsigned long p, GEN T) {
  if (T == NULL)
    return Flm_transpose (A);
  else
    return gtrans (A);
}

GEN
matsmall_mul (GEN A, GEN B, unsigned long p, GEN T) {
  if (lg(B) == 1)
    return cgetg(1, t_MAT);
  if (lg(A) != lgcols(B))
    pari_err(e_MISC, "incompatible dimensions");
  if (lg(A) == 1)
    return cgetg(lg(B), t_MAT);
  if (T == NULL)
    return Flm_mul (A, B, p);
  else
    return FlxqM_mul (A, B, T, p);
}

GEN
matsmall_col_mul (GEN x, GEN v, unsigned long p, GEN T) {
  if (T == NULL)
    return Flm_Flc_mul (x, v, p);
  else
    return FlxqM_FlxqC_mul (x, v, T, p);
}

GEN
matsmall_inv (GEN A, unsigned long p, GEN T) {
  if (T == NULL)
    return Flm_inv (A, p);
  else
    return FlxqM_inv (A, T, p);
}

GEN
matsmall_pow(GEN A, long n, unsigned long p, GEN T) {
  GEN B = matsmall_id(lg(A) - 1, p, T);

  if (n < 0) {
    A = matsmall_inv(A, p, T);
    n = -n;
  }
  while (n) {
    if (n % 2) {
      B = matsmall_mul(A, B, p, T);
      n--;
    }
    if (n)
      A = matsmall_mul(A, A, p, T);
    n >>= 1;
  }
  return B;
}

GEN
matsmall_ker (GEN A, unsigned long p, GEN T) {
  if (T == NULL)
    return Flm_ker(A, p);
  else
    return FlxqM_ker(A, T, p);
}

GEN
matsmall_random (unsigned long rows, unsigned long cols,
		 unsigned long p, GEN T) {
  unsigned long i, j;
  GEN A = cgetg (cols + 1, t_MAT);

  if (T == NULL) {
    for (i = 1; i <= cols; i++) {
      gel (A, i) = cgetg (rows + 1, t_VECSMALL);
      for (j = 1; j <= rows; j++)
	coeff (A, j, i) = random_Fl (p);
    }
  } else {
    long v = varn (T);
    for (i = 1; i <= cols; i++) {
      gel (A, i) = cgetg (rows + 1, t_COL);
      for (j = 1; j <= rows; j++)
	gcoeff (A, j, i) = random_Flx (degpol (T), evalvarn (v), p);
    }
  }
  return A;
}

/*
  Return an  h-dimensional subspace of the finite-dimensional
  vector space  V  (given as a basis) with uniform distribution.
*/
GEN
matsmall_random_subspace(GEN V, unsigned long h, int check,
			 unsigned long p, GEN T) {
  GEN A;
  int dim = lg(V) - 1;
  pari_sp av = avma;

  if (h < 0 || h > dim)
    pari_err (e_MISC, "invalid dimension");

  if (h == 0)
    return cgetg(1, t_MAT);
  else if (h == dim)
    return gcopy(V);

  do
    A = matsmall_random(dim, h, p, T);
  while (check && matsmall_rank(A, p, T) < h);

  return gerepileupto(av, matsmall_mul(V, A, p, T));
}

long
matsmall_rank (GEN A, unsigned long p, GEN T) {
  if (T == NULL)
    return Flm_rank(A, p);
  else
    return FlxqM_rank(A, T, p);
}

GEN
matsmall_indexrank(GEN A, unsigned long p, GEN T) {
  if (T == NULL)
    return Flm_indexrank(A, p);
  else
    return FlxqM_indexrank(A, T, p);
}

GEN
matsmall_image (GEN A, unsigned long p, GEN T) {
  if (T == NULL)
    return Flm_image(A, p);
  else
    return FlxqM_image(A, T, p);
}

GEN
matsmall_coker (GEN A, unsigned long p, GEN T) {
  pari_sp av = avma;
  if (T == NULL)
    return gerepileupto(av, Flm_transpose(Flm_ker(Flm_transpose(A), p)));
  else
    return gerepilecopy(av, shallowtrans(FlxqM_ker(shallowtrans(A), T, p)));
}

/*
  Quotient of two linear subspaces  W \subseteq V
  of some ambient vector space.
*/
GEN
matsmall_quotient(GEN V, GEN W, unsigned long p, GEN T) {
  pari_sp av = avma;
  GEN A = matsmall_solve(V, W, p, T);
  if (A == NULL)
    pari_err(e_MISC, "subspaces are not contained in each other");
  A = matsmall_coker(A, p, T);
  return gerepileupto(av, A);
}

/* column echelon form */
GEN
matsmall_echelon(GEN A, GEN *Rp, ulong p, GEN T) {
  pari_sp av = avma;
  long i, j, r;
  GEN R, C;
  if (T == NULL) {
    r = Flm_echelon(A, &R, &C, p);
    for (j = 1; j <= r; j++) {
      for (i = 1; i < R[j]; i++)
	ucoeff(C, i, j) = 0;
      ucoeff(C, R[j], j) = 1;
    }
  } else {
    long sv = get_Flx_var(T);
    GEN one = pol1_Flx(sv), zero = zero_Flx(sv);
    r = FlxqM_echelon(A, &R, &C, T, p);
    for (j = 1; j <= r; j++) {
      for (i = 1; i < R[j]; i++)
	gcoeff(C, i, j) = zero;
      gcoeff(C, R[j], j) = one;
    }
  }
  if (Rp) {
    gerepileall(av, 2, &R, &C);
    *Rp = R;
    return C;
  }
  return gerepilecopy(av, C);
}


/***
    Conversion functions
***/

static GEN
FlxqM_to_RgM(GEN A, GEN T, ulong p) {
  long rows, columns = lg(A) - 1, i, j;
  GEN result = cgetg(columns + 1, t_MAT);
  if (columns == 0)
    return result;
  rows = nbrows(A);
  for (j = 1; j <= columns; j++) {
    GEN c = cgetg(rows + 1, t_COL);
    for (i = 1; i <= rows; i++)
      gel(c, i) = Flxq_to_FF(gcoeff(A, i, j), p, T);
    gel(result, j) = c;
  }
  return result;
}

static GEN
RgM_to_FlxqM(GEN A, GEN T, ulong p) {
  long rows, columns = lg(A) - 1, i, j;
  GEN result = cgetg(columns + 1, t_MAT);
  if (columns == 0)
    return result;
  rows = nbrows(A);
  for (j = 1; j <= columns; j++) {
    GEN c = cgetg(rows + 1, t_COL);
    for (i = 1; i <= rows; i++)
      gel(c, i) = Rg_to_Flxq(gcoeff(A, i, j), T, p);
    gel(result, j) = c;
  }
  return result;
}

GEN
matsmall_to_RgM(GEN A, unsigned long p, GEN T) {
  if (T == NULL)
    return Flm_to_mod(A, p);
  else
    return FlxqM_to_RgM(A, T, p);
}

GEN
RgM_to_matsmall(GEN A, unsigned long p, GEN T) {
  if (T == NULL)
    return RgM_to_Flm(A, p);
  else
    return RgM_to_FlxqM(A, T, p);
}

/* Utility functions */

/*
  Return the characteristic polynomial of  A  as a t_POL
  with coefficients of type t_INTMOD (if T == NULL) or
  t_FFELT (if T != NULL).
*/
GEN
matsmall_charpoly (GEN A, long v, unsigned long p, GEN T) {
  pari_sp av = avma;
  GEN f = carhess(matsmall_to_RgM(A, p, T), v);
  return gerepileupto(av, f);
}

/*
  Return the determinant of  A  as a t_INTMOD (if T == NULL)
  or as a t_POLMOD in which the polynomials have t_INTMOD
  coefficients (if T != NULL).
*/
GEN
matsmall_det (GEN A, unsigned long p, GEN T) {
  if (T == NULL)
    return gmodulss (Flm_det (A, p), p);
  else {
    GEN pp = utoi (p);
    return gmodulo (gmodulo (Flx_to_ZX (FlxqM_det (A, T, p)), pp),
		    gmodulo (Flx_to_ZX (T), pp));
  }
}

/*
  Assuming  x  is a vector of  n  matrices of size  n \times n
  and  v  is a vector of length n, compute the linear combination
  of the elements of  x  with coefficients given by  v.
*/
GEN
matsmall_linear_combination (GEN x, GEN v, unsigned long p, GEN T) {
  pari_sp av = avma;
  int n = lg (x) - 1;
  GEN y = cgetg (n + 1, t_MAT);
  GEN tmp = cgetg (n + 1, t_MAT);
  int i, j;

  for (i = 1; i <= n; i++) {
    /* compute the i-th column of the result */
    for (j = 1; j <= n; j++)
      gel (tmp, j) = gmael (x, j, i);
    gel (y, i) = matsmall_col_mul (tmp, v, p, T);
  }
  return gerepilecopy (av, y);
}

/*
  Solve the system A*X = B.
*/
GEN
matsmall_solve (GEN A, GEN B, unsigned long p, GEN T) {
  if (T == NULL)
    return Flm_gauss(A, B, p);
  else
    return FlxqM_gauss(A, B, T, p);
}

/*
  Solve the system X*A = B.
*/
GEN
matsmall_solve_left (GEN A, GEN B, unsigned long p, GEN T) {
  pari_sp av = avma;
  if (T == NULL)
    return gerepileupto(av, Flm_transpose(Flm_gauss(Flm_transpose(A),
						    Flm_transpose(B), p)));
  else
    return gerepilecopy(av, shallowtrans(FlxqM_gauss(shallowtrans(A),
						     shallowtrans(B), T, p)));
}

/*
  Compute the total inverse image of  V  under  A  (in contrast
  to PARI's inverseimage function).
*/
GEN
matsmall_inverse_image (GEN A, GEN V, unsigned long p, GEN T) {
  pari_sp av = avma;
  GEN quot;

  if (lg (V) == 1)
    return matsmall_ker (A, p, T);
  quot = matsmall_coker (V, p, T);
  return gerepileupto (av, matsmall_ker (matsmall_mul (quot, A, p, T), p, T));
}

/*
  Given a decomposition of a vector space as a direct sum of
  subspaces, compute the projection maps onto all of the subspaces.
  TODO: this assumes the subspaces are non-trivial (because of the
  transpose).
*/
GEN
matsmall_direct_sum_projections (GEN spaces, unsigned long p, GEN T) {
  pari_sp av = avma;
  GEN A, A_inv_t, projections;
  int i, j, dim = 0, count;

  /* Compute the dimension of the total space.  */
  for (i = 1; i < lg (spaces); i++)
    dim += lg (gel (spaces, i)) - 1;
  /*
    Compute the change-of-bases matrix from the direct sum
    of the subspaces to the ambient space.
  */
  A = cgetg (dim + 1, t_MAT);
  for (count = 0, i = 1; i < lg (spaces); i++)
    for (j = 1; j < lg (gel (spaces, i)); j++)
      gel (A, ++count) = gmael (spaces, i, j);
  A_inv_t = matsmall_transpose (matsmall_inv (A, p, T), p, T);
  projections = cgetg (lg (spaces), t_VEC);
  for (count = 0, i = 1; i < lg (spaces); i++) {
    /*
      Select the correct range of rows from A^{-1}.  We have
      transposed that matrix because extracting columns is easier
      than extracting rows.
    */
    gel (projections, i) = cgetg (lg (gel (spaces, i)), t_MAT);
    for (j = 1; j < lg (gel (spaces, i)); j++)
      gmael (projections, i, j) = gel (A_inv_t, ++count);
    gel (projections, i) = matsmall_transpose (gel (projections, i), p, T);
  }
  return gerepilecopy (av, projections);
}
