modular_symbols_group(M) = M[1];
modular_symbols_weight(M) = M[2];
modular_symbols_manin_symbols(M) = M[3];
modular_symbols_basis(M) = M[4];
modular_symbols_dimension(M) = length(M[4]);
modular_symbols_manin_symbols_to_basis(M) = M[5];
modular_symbols_boundary_map(M) = M[6];
modular_symbols_cuspidal_subspace(M) = M[7];
modular_symbols_cuspidal_subspace_dimension(M) = matsize(M[7])[2];

install(modular_symbols, "GLDG", , "libmodular.so");
install(modular_symbols_diamond_operator, "GL", , "libmodular.so");
install(modular_symbols_hecke_operator, "GL", , "libmodular.so");
install(modular_symbols_star_operator, "G", , "libmodular.so");
install(modular_symbols_atkin_lehner_operator, "GL", , "libmodular.so");
install(modular_symbols_cuspidal_plus_subspace, "G", , "libmodular.so");
